<!-- Left panel : Navigation area -->
<aside id="left-panel" style="
    background: #FFF;
    border: solid 1px #DDD;
	">
	<nav>
		<ul>
			<li class="active">
				<a href="index.php" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i><span class="menu-item-parent">Dashboard</span></a>
			</li>

			<li>
				<a href="#" title="Project"><i class="fa fa-lg fa-fw fa-folder-open-o"></i> <span class="menu-item-parent">Project </span></a>

				<ul>
					<li>
						<a href="charter.php" title="Input"><i class="fa fa-folder-open-o"></i> <span class="menu-item-parent">Input</span></a>
					</li>
					<li>
						<a href="ongoing_pc.php" title="On Going"><i class="fa fa-folder-open-o"></i> <span class="menu-item-parent">On Going</span></a>
					</li>
					<li>
						<a href="closed_p.php" title="Closed"><i class="fa fa-folder-open-o"></i> <span class="menu-item-parent">Closed</span></a>
					</li>
				</ul>
			</li>

			<li>
				<a href="#" title="Manage Service"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Manage Service</span></a>
				<ul>
					<li>
						<a href="input_ms.php" title="Input"><i class="fa fa-lg fa-fw fa-folder-open-o"></i> <span class="menu-item-parent">Input</span></a>
					</li>
					<li>
						<a href="ongoing_ms.php" title="On Going"><i class="fa fa-folder-open-o"></i> <span class="menu-item-parent">On Going</span></a>
					</li>
					<li>
						<a href="closed_ms.php" title="Closed"><i class="fa fa-folder-open-o"></i> <span class="menu-item-parent">Closed</span></a>
					</li>
				</ul>
			</li>

			<li>
				<a href="manage_service.php" title="Daily Activity"><i class="fa fa-lg fa-fw fa-list-alt"></i> <span class="menu-item-parent">Daily Activity</span></a>
			</li>

			<li class="">
				<a href="#" title="Manage Profile"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Manage Profile</span></a>
				<ul>
					<li>
						<a href="ganti_password.php" title="Ganti Password"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Ganti Password</span></a>
					</li>

					<li class="">
						<a href="manage_user.php" title="Manage User"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Manage User</span></a>
					</li>
				</ul>
			</li>
		</ul>
	</nav>
	<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
</aside>
<!-- END NAVIGATION -->
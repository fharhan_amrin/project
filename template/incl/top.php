<!-- HEADER -->
<header id="header">
	<div id="logo-group">
		<!-- PLACE YOUR LOGO HERE -->
		<span> <img src="img/logo (2).png" alt="SmartAdmin"> </span>
		<!-- END LOGO PLACEHOLDER -->
	</div>
	<!-- pulled right: nav area -->
	<div class="pull-right">
		<!-- collapse menu button -->
		<div id="hide-menu" class="btn-header pull-right">
			<span> <a href="javascript:void(0);" title="Collapse Menu" data-action="toggleMenu"><i class="fa fa-reorder"></i></a>
			</span>
		</div>
		<!-- end collapse menu -->
		<!-- logout button -->
		<div id="logout" class="btn-header transparent pull-right">
			<span> <a href="login.html" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i
				class="fa fa-sign-out"></i> Logout</a> </span>
			</div>
			<!-- end logout button -->
		</div>
		<!-- end pulled right: nav area -->
	</header>
	<!-- END HEADER -->
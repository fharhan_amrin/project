<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Project</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/css/vendor.bundle.base.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/css/vendor.bundle.addons.css'); ?>">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">

    <link href="<?php echo base_url('assets/plugins/sweet-alert2/sweetalert2.min.css'); ?>" rel="stylesheet" media="all" type="text/css">
    <!-- endinject -->
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/logosmall.png'); ?>" />
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
            <div class="content-wrapper d-flex align-items-center auth  theme-one">
                <div class="row w-100">
                    <div class="col-lg-4 mx-auto">
                        <div class="auto-form-wrapper">
                            <center>
                                <p style="margin-bottom: 30px;">
                                    <img src="<?php echo base_url('assets/images/telkomcel.png'); ?>" height="50px">
                                </p>
                            </center>
                            <form action="javascript:void(0)" id="frmLogin" method="POST">
                                <div class="form-group">
                                    <label class="label">Username</label>
                                    <div class="input-group">
                                        <input type="text" name="username" id="username" class="form-control" placeholder="Username">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="mdi mdi-check-circle-outline"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="label">Password</label>
                                    <div class="input-group">
                                        <input type="password" name="password" id="password" class="form-control" placeholder="*********">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="mdi mdi-check-circle-outline"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary submit-btn btn-block" id="submit" type="submit">Login</button>
                                </div>
                                <div class="form-group d-flex justify-content-between">
                                    <div class="form-check form-check-flat mt-0">
                                        <!-- <label class="form-check-label"> -->
                                        <!-- <input type="checkbox" class="form-check-input" checked> Keep me signed in -->
                                        <!-- </label> -->
                                    </div>
                                    <!-- <a href="#" class="text-small forgot-password text-black">Forgot Password</a> -->
                                </div>
                                <div id='err_show' class="w3-text-red">
                                    <div id='msg'> </div>
                            </form>
                        </div>
                        <p class="footer-text text-center" style="color:#000;">Copyright © 2018 Telkomcel. All rights
                            reserved.</p>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<?php echo base_url('assets/jquery-3.4.0.js'); ?>"></script>
    <script src="<?= base_url('template/sweetalert2/') ?>sweetalert2.min.js"></script>

    <script type="text/javascript">
        // Ajax post
        $(document).ready(function() {
            $("#submit").click(function() {
                var username = $("#username").val();
                var password = $("#password").val();
                // Returns error message when submitted without req fields.
                if (username == '' || password == '') {
                    jQuery("div#err_msg").show();
                    jQuery("div#msg").html("All fields are required");
                } else {
                    // AJAX Code To Submit Form.
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url('index.php/login/check_login'); ?>",
                        data: {
                            username: username,
                            password: password
                        },
                        dataType: 'json',
                        cache: false,
                        success: function(result) {
                            if (result.status) {
                                localStorage.setItem('id', result.session.id);
                                setTimeout(() => {
                                    window.location.replace(result.url);
                                }, 200);
                            } else {
                                Swal.fire({
                                    position: 'center',
                                    type: 'error',
                                    // title: result.msg,
                                    html: '<div style="font-size:16px;">' + result.msg +
                                        '</div>',
                                    showConfirmButton: false,
                                    timer: 2000
                                });
                            }
                            //     if (result != 0) {
                            //         // On success redirect.
                            //         // window.location.replace(result);
                            //         Swal.fire(
                            //             'Sukses!',
                            //             result,
                            //             'success'
                            //         );
                            //     }else{

                            //     // Swal({
                            //     //     type: 'error',
                            //     //     title: 'Oops...',
                            //     //     text: 'Something went wrong!',
                            //     //     footer: '<a href>Why do I have this issue?</a>'
                            //     // });
                            //     Swal.fire(
                            //             'Gagal!',
                            //             'Gagal Login, harap perikasa kembali username dan password anda',
                            //             'error'
                            //         );
                            // }
                        }
                    });
                }
            });
        });
    </script>

    <!-- endinject -->
</body>

</html>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Project list</title>
<!-- Basic Styles -->
<link rel="stylesheet" type="text/css" media="screen" href="http://localhost/project_list/template/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="http://localhost/project_list/template/css/font-awesome.min.css">

</head>
<body>
    <table id="contoh" border="1" width="100%">
    <thead>
    <tr>
    <th>No</th>
    <th>Task</th>
    <th>PIC STS</th>
    <th>PIC TCEL</th>
    <th>Status</th>
    <th>Note</th>
    <th>Review</th>
    </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($project->se($status)->result() as $row) {
            ?>
            <tr>
            <td>
            <?=$no++;?> 
            </td>
            <td><a href="<?=site_url('Project_List/charter?id_pc=' . $row->id)?>"
            target='_blank'><?php echo $row->task ?></a></td>
            <td><?php echo $row->pic_sts; ?></td>
            <td><?php echo $row->pic_tcel; ?></td>
            <td><?php echo $project->cekStatus($row->status); ?></td>
            <td><?php echo n2lbr($row->note); ?></td>
            <td><?php echo n2lbr($row->review); ?></td>
            </tr>
            <?php }?>
    </tbody>
    </table>
</body>
</html>
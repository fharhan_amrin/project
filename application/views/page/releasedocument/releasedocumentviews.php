<br><br>
<div id="main" role="main">



  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="card">

          <div class="card-body" style="background-color: white; padding: 10px;">
            <div class="card-title">
              <b>
                <h1> Form Input Release Document</h1>
              </b>
            </div>
            <form method="POST" action="<?php echo
                                          base_url('ReleaseDocumentController/insertrelease')
                                        ?>" enctype="multipart/form-data">

              <div class="form-group">
                <label for="">Nodin</label>
                <input type='text' name='nodin' id="nodin" class="form-control" placeholder="nodin">
              </div>
              <div class="form-group">
                <label for="">Status</label>
                <select name="status" id="status" class="form-control">
                  <option value="">Not Selected</option>
                  <option value="0">Before Release</option>
                  <option value="1">Post Release</option>
                  <option value="2">Terminasi</option>
                </select>
              </div>
              <div class="form-group">
                <input type='file' name='files' class="form-control">
              </div>
              <input type='submit' class="btn btn-primary" value='Submit' name='upload' />

            </form>
          </div>
        </div>
      </div>
    </div>
    <br>

    <div class="row">
      <div class="col-md-10">
        <div class="card">
          <div class="card-body" style="background-color: white; padding: 10px;">
            <div class="table-responsive" style="background-color: white; padding: 10px;">
              <table class="table">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Nodin</th>
                    <th>Status</th>
                    <th>File</th>
                    <th>Created_At</th>
                    <th>Updated_At</th>
                  </tr>
                </thead>
              </table>
            </div>

          </div>

        </div>

      </div>

    </div>
  </div>
</div>
<script src="<?php echo base_url('assets/jquery.min.js'); ?>"></script>
<script>
  // $(document).ready(function() {
  //   alert("Hello World ")
  // });
</script>
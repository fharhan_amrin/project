<div id="main" role="main">

    <?php
    $tanggal = $this->input->get('tanggal');
    if ($tanggal == '') {
        $tanggal = date('Y-m-d');
    }

    $this->load->view('template/breadcumb');
    ?>

    <style type="text/css">
        .inp {
            display: none;
        }
    </style>

    <div id="content">
        <!-- widget grid -->
        <section id="widget-grid">


            <div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2><?= $smallTitle . ' (' . $tanggal . ')' ?> </h2>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div class="widget-body no-padding">

                        <div class="row" style="margin-top: 10px; margin-left: 1px;margin-bottom: 10px;">
                            <div class="col-md-12">
                                <?php
                                if ($this->session->userdata('status') == '1') {
                                ?>
                                    <a id="edit" class="btn btn-labeled btn-default"><span class="btn-label"><i class="fa fa-fw fa-pencil-square-o"></i></span><span>Edit</span></a>
                                <?php } ?>

                                <a id="save" style="display: none;" class="btn btn-labeled btn-default"><span class="btn-label"><i class="fa fa-fw fa-save"></i></span><span>Save</span></a>
                            </div>
                        </div>

                        <form method="post" action="<?php echo site_url('/query/delete') ?>" id="form-delete">
                            <div class="tampil">
                                <table id="contoh" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th>device</th>
                                            <th>Status</th>
                                            <th>Note</th>
                                            <!-- <th>created</th> -->
                                        </tr>
                                    </thead>

                                </table>
                            </div>

                    </div>
                </div>
                </form>
            </div>
        </section>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Edit Log Daily Activity #<span id="id"></span>
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form id="upForm" method="post" action="javascript:void(0);" class="smart-form" style="padding: 10px;">
                    <div id="tampil">
                        <table id="contoh" class="table table-bordered">
                            <tr>
                                <td>device</td>
                                <td>
                                    <input type="hidden" class="form-control" readonly name="id" placeholder="device">
                                    <input type="hidden" class="form-control" readonly name="device" value="<?= $device; ?>" placeholder="device">
                                    <input type="text" class="form-control" readonly name="device" placeholder="device">
                                </td>
                            </tr>

                            <tr>
                                <td>Status</td>
                                <td>
                                    <div class="inline-group">
                                        <label class="radio">
                                            <input type="radio" name="status" id="no_issue" value="0">
                                            <i></i>
                                            No Issue
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="status" id="issue" value="1">
                                            <i></i>
                                            Issue
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>note</td>
                                <td><input type="text" class="form-control" readonly name="note" placeholder="note">
                                </td>
                            </tr>
                            <tr>
                                <td>created_date</td>
                                <td><input type="text" class="form-control" readonly name="created_date" placeholder="note"></td>
                            </tr>
                        </table>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="btn_simpan">
                    Save
                </button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    var datas = new Array;
    $(document).ready(function() {
        showTable();
        update();

        $("#btn-delete").click(function() { // Ketika user mengklik tombol delete
            var confirm = window.confirm(
                "Apakah Anda yakin ingin menghapus data-data ini?"); // Buat sebuah alert konfirmasi

            if (confirm) // Jika user mengklik tombol "Ok"
                $("#form-delete").submit(); // Submit form
        });
        $('#edit').click(function(event) {
            event.preventDefault();
            $('.inp*').show();
            $('#save').show();
            $(this).hide();
            $('.txt*').hide();
        });

        $('#save').click(function(event) {
            event.preventDefault();
            $('.inp*').hide();
            $('#edit').show();
            $(this).hide();
            $('.txt*').show();
            showTable();
        });

    });

    function test(id) {
        var ID = [];
        $.each($("input[name='id[]']:checked"), function() {
            ID.push($(this).val());
        });

        var id = ID.join(",");
        $('#edit').attr('data-edit', id);
        // edit(id);
        hapus(id);
    }

    function edit(id) {
        $("#edit").attr("href", "<?php echo site_url(); ?>/project_list/viewUpPc?id=" + id);
    }

    function hapus(id) {
        $("#btn-delete").attr("href", "<?php echo site_url(); ?>/action/deletePc?id=" + id);
    }

    function prosesInCDR() {
        $('#inForm').submit(function(event) {
            event.preventDefault();
            $.ajax({
                    url: '<?= site_url("/ManageService/inServices"); ?>',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                })
                .done(function(data) {
                    Swal.fire(
                        'Sukses!',
                        data.msg,
                        'success'
                    )
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
        });
    }

    function getInfo(id = '') {
        $('#tampil').html('');
        $.ajax({
                url: '<?= site_url("/ManageService/getData?in="); ?>' + id + "&tanggal=" + $('input[name=tanggal]')
                    .val(),
                type: 'GET',
                dataType: 'JSON'
            })
            .done(function(data) {
                var no = 1;
                $.each(data, function(key, v) {
                    $('#tampil').append(
                        '<table class="table table-bordered" style="margin-bottom:10px;"><tr> <td>Device</td> <td> <input type="hidden" class="form-control" readonly name="id" placeholder="id" value="' +
                        v.id +
                        '"> <input type="hidden" class="form-control" readonly name="tanggal" value="" placeholder="Device"> <input type="text" class="form-control" readonly name="deskripsi" placeholder="Deskripsi" value="' +
                        v.device +
                        '"> </td> <tr> <td>Status</td> <td> <div class="inline-group"> <label class="radio"> <input type="radio" name="status' +
                        v.id + '" id="no_issue' + v.id +
                        '" value="0"> <i></i> No Issue </label> <label class="radio"> <input type="radio" name="status' +
                        v.id + '" id="issue' + v.id +
                        '" value="1"> <i></i> Issue </label> </div> </td> </tr> <td>note</td> <td><input type="text" class="form-control"  name="note" placeholder="note" value="' +
                        v.note +
                        '"></td></tr><tr> <td>created_date</td> <td><input type="text" class="form-control" readonly name="created_date" placeholder="created_at" value="' +
                        v.created_date +
                        '"></td></tr> </table>');

                    // created_date
                    if (v.status == '1') {
                        $('#issue' + v.id).prop('checked', true);
                    } else if (v.status == '0') {
                        $('#no_issue' + v.id).prop('checked', true);
                    }

                    console.log('');

                });


            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
    }


    function update(s) {
        $('#upForm').submit(function(event) {
            event.preventDefault();
            $.ajax({
                    url: '<?= site_url("/ManageService/upLDDOne"); ?>',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                    // data : {id:s,status : $('#inp'+s).val()}
                })
                .done(function(data) {
                    Swal.fire(
                        'Sukses!',
                        data.msg,
                        'success'
                    )
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
        });

    }

    function ubahINP(field, id, value) {
        $.ajax({
                url: '<?= site_url("/ManageService/upLDDOne"); ?>',
                type: 'POST',
                dataType: 'JSON',
                // data: $(this).serialize()
                data: {
                    id: id,
                    field: field,
                    val: value
                }
            })
            .done(function(data) {
                console.log('Sukses');
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
    }

    function kumpulinData(v = '') {

        if (v != '') {
            if (datas.length > 0) {
                for (var i = 0; i < datas.length; i++) {
                    if (datas[i].id != v) {
                        // datas.push({
                        //     id : v
                        // });
                        console.log(v);
                    }
                }
            } else {

                datas.push({
                    id: v
                });

            }
        }
        return datas;
    }

    function showTable() {
        // body...
        $('#contoh').DataTable({
            // Processing indicator
            "destroy": true,
            "searching": true,
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            "scrollX": true,
            // Initial no order.
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?= site_url("/ManageService/dtDD"); ?>",
                "type": "POST"
            },
            //Set column definition initialisation properties
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });
    }
</script>
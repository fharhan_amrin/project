<div id="main" role="main">

    <?php
    $tanggal = $this->input->get('tanggal');
    if ($tanggal == '') {
        $tanggal = date('Y-m-d');
    }

    $this->load->view('template/breadcumb');
    ?>

    <style type="text/css">
        .inp {
            display: none;
        }
    </style>

    <div id="content">
        <!-- widget grid -->
        <section id="widget-grid">
            <!-- NEW WIDGET START -->
            <!-- Widget ID (each widget will need unique ID)-->
            <!-- Widget ID (each widget will need unique ID)-->

            <div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2><?= $smallTitle . ' (' . $tanggal . ')' ?> </h2>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div class="widget-body no-padding">

                        <div class="row" style="margin-top: 10px; margin-left: 1px;margin-bottom: 10px;">
                            <div class="col-md-12">
                                <?php
                                if ($this->session->userdata('status') == '1') {
                                ?>

                                    <a id="edit" class="btn btn-labeled btn-default"><span class="btn-label"><i class="fa fa-fw fa-pencil-square-o"></i></span><span>Edit</span></a>
                                <?php } ?>

                                <a id="save" style="display: none;" class="btn btn-labeled btn-default"><span class="btn-label"><i class="fa fa-fw fa-save"></i></span><span>Save</span></a>
                            </div>
                        </div>

                        <form method="post" action="<?php echo site_url('query/delete') ?>" id="form-delete">
                            <div class="tampil">
                                <table id="contoh" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th>pic</th>
                                            <th>Status</th>
                                            <th>Note</th>
                                            <th>Review</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>

                    </div>
                </div>
                </form>
            </div>
        </section>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    Edit Log Daily Activity #<span id="id"></span>
                </h4>
            </div>
            <div class="modal-body no-padding">
                <form id="upForm" method="post" action="javascript:void(0);" class="smart-form" style="padding: 10px;">
                    <div id="tampil">
                        <table class="table table-bordered">
                            <tr>
                                <td>Deskripsi</td>
                                <td>
                                    <input type="hidden" class="form-control" readonly name="id" placeholder="Deskripsi">
                                    <input type="hidden" class="form-control" readonly name="tanggal" value="<?= $tanggal; ?>" placeholder="Deskripsi">
                                    <input type="text" class="form-control" readonly name="deskripsi" placeholder="Deskripsi">
                                </td>
                            </tr>
                            <tr>
                                <td>PIC</td>
                                <td><input type="text" class="form-control" readonly name="pic" placeholder="PIC"></td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>
                                    <div class="inline-group">
                                        <label class="radio">
                                            <input type="radio" name="status" id="no_issue" value="0">
                                            <i></i>
                                            No Issue
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="status" id="issue" value="1">
                                            <i></i>
                                            Issue
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Save
                </button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    var datas = new Array;

    $(document).ready(function() {

        showTable();
        $("#btn-delete").click(function() { // Ketika user mengklik tombol delete
            var confirm = window.confirm(
                "Apakah Anda yakin ingin menghapus data-data ini?"); // Buat sebuah alert konfirmasi

            if (confirm) // Jika user mengklik tombol "Ok"
                $("#form-delete").submit(); // Submit form
        });

        $('#contoh tbody:first').append('<tr><td>das</td></tr>')

        $('#edit').click(function(event) {
            event.preventDefault();
            $('.inp*').show();
            $('#save').show();
            $(this).hide();
            $('.txt*').hide();
            // $('#myModal').modal('show');
            // getInfo($('#edit').attr('data-edit'));
        });

        $('#save').click(function(event) {
            event.preventDefault();
            $('.inp*').hide();
            $('#edit').show();
            $(this).hide();
            $('.txt*').show();
            showTable();
            // $('#myModal').modal('show');
            // getInfo($('#edit').attr('data-edit'));
        });
    });

    function test(id) {
        var ID = [];
        $.each($("input[name='id[]']:checked"), function() {
            ID.push($(this).val());
        });

        var id = ID.join(",");
        $('#edit').attr('data-edit', id);
        // edit(id);
        hapus(id);
    }

    function edit(id) {
        $("#edit").attr("href", "<?php echo site_url(); ?>project_list/viewUpPc?id=" + id);
    }

    function hapus(id) {
        $("#btn-delete").attr("href", "<?php echo site_url(); ?>action/deletePc?id=" + id);
    }

    function prosesInCDR() {
        $('#inForm').submit(function(event) {
            event.preventDefault();
            $.ajax({
                    url: '<?= site_url("ManageService/inServices"); ?>',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                })
                .done(function(data) {
                    Swal.fire(
                        'Sukses!',
                        data.msg,
                        'success'
                    )
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
        });
    }

    function getInfo(id = '') {
        $('#tampil').html('');
        $.ajax({
                url: '<?= site_url("ManageService/getLDAID?in="); ?>' + id + "&tanggal=" + $('input[name=tanggal]')
                    .val(),
                type: 'GET',
                dataType: 'JSON'
            })
            .done(function(data) {
                var no = 1;
                $.each(data, function(key, v) {
                    $('#tampil').append(
                        '<table class="table table-bordered" style="margin-bottom:10px;"><tr> <td>Deskripsi</td> <td> <input type="hidden" class="form-control" readonly name="id[]" placeholder="id" value="' +
                        v.id +
                        '"> <input type="hidden" class="form-control" readonly name="tanggal" value="" placeholder="Deskripsi"> <input type="text" class="form-control" readonly name="deskripsi" placeholder="Deskripsi" value="' +
                        v.description +
                        '"> </td> </tr> <tr> <td>PIC</td> <td><input type="text" class="form-control" readonly name="pic" placeholder="PIC" value="' +
                        v.pic +
                        '"></td> </tr> <tr> <td>Status</td> <td> <div class="inline-group"> <label class="radio"> <input type="radio" name="status' +
                        v.id + '" id="no_issue' + v.id +
                        '" value="0"> <i></i> No Issue </label> <label class="radio"> <input type="radio" name="status' +
                        v.id + '" id="issue' + v.id +
                        '" value="1"> <i></i> Issue </label> </div> </td> </tr> </table>');


                    if (v.status == '1') {
                        $('#issue' + v.id).prop('checked', true);
                    } else if (v.status == '0') {
                        $('#no_issue' + v.id).prop('checked', true);
                    }

                    console.log('');

                });
                // $('input[name=id]').val(data.id);
                // $('input[name=deskripsi]').val(data.description);
                // $('input[name=pic]').val(data.pic);

            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
    }


    function update(s) {
        $('#upForm').submit(function(event) {
            event.preventDefault();
            $.ajax({
                    url: '<?= site_url("ManageService/upLDAOne"); ?>',
                    type: 'POST',
                    dataType: 'JSON',
                    data: $(this).serialize()
                    // data : {id:s,status : $('#inp'+s).val()}
                })
                .done(function(data) {
                    Swal.fire(
                        'Sukses!',
                        data.msg,
                        'success'
                    )
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
        });

    }


    function showTable() {
        // body...
        $('#contoh').DataTable({
            // Processing indicator
            "destroy": true,
            "searching": true,
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            "scrollX": true,
            // Initial no order.
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?= site_url("ManageService/dtDA"); ?>",
                "type": "POST"
            },
            //Set column definition initialisation properties
            "columnDefs": [{
                "targets": [0],
                "orderable": false
            }]
        });

    }



    function ubahINP(field, id, value) {
        $.ajax({
                url: '<?= site_url("ManageService/upLDAOne"); ?>',
                type: 'POST',
                dataType: 'JSON',
                // data: $(this).serialize()
                data: {
                    id: id,
                    field: field,
                    val: value
                }
            })
            .done(function(data) {
                console.log('Sukses');
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
    }

    function kumpulinData(v = '') {

        if (v != '') {
            if (datas.length > 0) {
                for (var i = 0; i < datas.length; i++) {
                    if (datas[i].id != v) {
                        // datas.push({
                        //     id : v
                        // });
                        console.log(v);
                    }
                }
            } else {

                datas.push({
                    id: v
                });

            }
        }
        return datas;
    }
    // function update(s) {
    //     $('#upForm').submit(function(event) {
    //         event.preventDefault();
    //         $.ajax({
    //                 url: '<?= site_url("ManageService/upLDAOne"); ?>',
    //                 type: 'POST',
    //                 dataType: 'JSON',
    //                 data: $(this).serialize()
    //                 // data : {id:s,status : $('#inp'+s).val()}
    //             })
    //             .done(function(data) {
    //                 Swal.fire(
    //                     'Sukses!',
    //                     data.msg,
    //                     'success'
    //                 )
    //             })
    //             .fail(function() {
    //                 console.log("error");
    //             })
    //             .always(function() {
    //                 console.log("complete");
    //             });
    //     });
    // }
</script>
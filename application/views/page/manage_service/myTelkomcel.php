<div id="main" role="main">

    <?php $this->load->view('template/breadcumb'); ?>

    <style>
        .dataTables_filter {
            display: block;
        }

        .dataTables_length {
            display: block;
        }

        .jarviswidget .widget-body {
            min-height: 45px !important;
            position: relative;
            padding-bottom: 13px;
        }
    </style>

    <div id="content">
        <!-- widget grid -->
        <section id="widget-grid">
            <!-- NEW WIDGET START -->
            <!-- Widget ID (each widget will need unique ID)-->
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-custombutton="false">

                <header type="button" data-toggle="collapse" href="#bisa" role="button" aria-expanded="false" aria-controls="bisa">
                    <span class=" widget-icon"> <i class="fa fa-edit"></i> </span>
                    <span id="txtAction" style="
                            font-size: 14px;
                            font-weight: bold;
                            position: relative;
                            top: -5px;
                            left: 5px;
                            ">Add </span>
                </header>



                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="collapse" id="bisa">


                        <div class="widget-body no-padding" id="ok">

                            <form id="inForm" method="POST" action="javascript:void(0);" class="smart-form" novalidate="novalidate">

                                <fieldset>
                                    <?php
                                    if ($this->session->userdata('status') == '1') {
                                        ?>

                                        <div class="row">
                                            <section class="col col-6">
                                                <span id="cloneID"></span>
                                                <label>Request Date</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-calendar"></i>
                                                    <input type="date" name="request_date" id="startdate" placeholder="Request Date">
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label>Costumer Request</label>
                                                <label class="input"> <i class="icon-append fa fa-pencil-square-o"></i>
                                                    <input type="text" name="costumer_request" placeholder="Costumer Request">
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label>Whom</label>
                                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                                    <input type="text" name="whom" placeholder="Whom">
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label>Request Done</label>
                                                <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                                    <input type="date" name="request_done" placeholder="Date">
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label>Request By</label>
                                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                                    <input type="text" name="request_by" placeholder="Request By">
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label>Review</label>
                                                <label class="input"> <i class="icon-append fa fa-search"></i>
                                                    <input type="text" name="review" placeholder="Review">
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label>Note</label>
                                                <label class="textarea">
                                                    <textarea rows="3" name="note" id="txtArea" placeholder="Note"></textarea>
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label>Status: *</label>
                                                <div id="checkout-form" class="smart-form" novalidate="novalidate">
                                                    <label class="select">
                                                        <select name="status" id="status">
                                                            <option value="" disabled="">-- Pilihan --</option>
                                                            <option value="1" selected=""> DRAFT </option>
                                                            <option value="2"> ON PROGRESS </option>
                                                            <option value="3"> DONE </option>
                                                            <option value="4"> PENDING </option>
                                                        </select> <i></i>
                                                    </label>
                                                </div>
                                            </section>

                                        </div>
                                </fieldset>

                                <footer>
                                    <button type="submit" class="btn btn-primary" id="txtAction" onclick="proses()">
                                        Add
                                    </button>
                                    <button type="reset" class="btn btn-default" onclick="ubahText('','add')">
                                        Cancel
                                    </button>
                                </footer>
                            <?php } else {
                                ?>
                                <div class="row">
                                    <section class="col col-6">
                                        <span id="cloneID"></span>
                                        <label>Request Date</label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input type="text" name="request_date" id="startdate" placeholder="Request Date" readonly>
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label>Costumer Request</label>
                                        <label class="input"> <i class="icon-append fa fa-pencil-square-o"></i>
                                            <input type="text" name="costumer_request" placeholder="Costumer Request" readonly>
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label>Whom</label>
                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="whom" placeholder="Whom" readonly>
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label>Request Done</label>
                                        <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                            <input type="date" name="request_done" placeholder="Date" readonly>
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label>Request By</label>
                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                            <input type="text" name="request_by" placeholder="Request By" readonly>
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label>Review</label>
                                        <label class="input"> <i class="icon-append fa fa-search"></i>
                                            <input type="text" name="review" class="review" id="review" placeholder="Review" readonly> </label> </section>
                                    <section class="col col-6">
                                        <label>Note</label>
                                        <label class="textarea">
                                            <textarea rows="3" name="note" placeholder="Note" disabled></textarea>
                                        </label>
                                    </section>
                                    <section class="col col-6" disabled>
                                        <label>Status: *</label>
                                        <div id="checkout-form" class="smart-form" novalidate="novalidate">
                                            <label class="select">
                                                <select name="status" id="status">
                                                    <option value="" disabled="">-- Pilihan --</option>
                                                    <option value="1" selected=""> DRAFT </option>
                                                    <option value="2" disabled> ON PROGRESS </option>
                                                    <option value="3" disabled> DONE </option>
                                                    <option value="4" disabled> PENDING </option>
                                                </select> <i></i>
                                            </label>
                                        </div>
                                    </section>

                                </div>

                                </fieldset>

                                <footer>
                                    <button type="submit" class="btn btn-primary" class="txtAction" id="txtAction" onclick="proses()" disabled>
                                        Add
                                    </button>
                                    <button type="reset" class="btn btn-default" onclick="ubahText('','add')" disabled>
                                        Cancel
                                    </button>
                                </footer>
                            <?php
                            }
                            ?>
                            </form>


                        </div>
                        <!-- end widget content -->
                    </div>
                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

            <div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
                <header>

                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2><?= $smallTitle; ?></h2>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="row" style="margin-top: 10px; margin-left: 1px;">
                            <div class="col-md-12">
                                <a href="<?php echo site_url('/MyTelkomcelController/cetak_pdf'); ?>" target="_blank" class="btn btn-primary">Cetak Pdf</a>
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">
                                    Import Excel
                                </button>
                                <!-- filter data pdf -->
                                <a class="btn btn-danger" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" style="float: right;margin-right: 10px;">
                                    Filter
                                </a>
                                <div class="collapse" id="collapseExample" style="margin-top:10px;border-top:solid 1px #DDD;    border-top: solid 1px #DDD; padding-top: 4px;">
                                    <div class="card card-body">

                                        <form action="<?php echo site_url('/MyTelkomcelController/cetak_pdf') ?>" method="post">




                                            <div class="row">

                                                <div class="col-md-3">
                                                    <section>
                                                        <label>Start Date </label>
                                                        <input style="width:100%;" type="date" name="startdate" class="input-sm">
                                                    </section>
                                                </div>

                                                <div class="col-md-3">
                                                    <section>
                                                        <label>End Date </label>
                                                        <input style="width:100%;" type="date" name="enddate" class="input-sm">
                                                    </section>
                                                </div>

                                                <!-- <div class="col-md-3">
                                                <section>
                                                    <label>Status </label>
                                                    <select name="status" id="status" class="form-control">
                                                        <option value="0">Draft</option>
                                                        <option value="1">On Progress</option>
                                                        <option value="3">Done</option>
                                                        <option value="4">Panding</option>
                                                    </select>
                                                </section>
                                            </div> -->

                                                <div class="col-md-3">
                                                    <button type="submit" class="btn btn-labeled btn-default" style="height:30px; position: relative;top: 24px;">Filter PDF</button>
                                                    <button type="button" class="btn btn-labeled btn-default" id="btn-cari" style="height:30px; position: relative;top: 24px;">Submit Filter</button>
                                                </div>

                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- akhir filter pdf -->
                        </div>
                    </div>
                    <form method="post" action="<?php echo site_url('/query/delete') ?>" id="form-delete">
                        <table id="contoh" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th>Request Date</th>
                                    <th>Costumer Request</th>
                                    <th>Whom</th>
                                    <th>Status & Date</th>
                                    <th>Request By</th>
                                    <th>Note</th>
                                    <th>Review</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($model->getMyTelkomcel(date('Y-m-d'))->result() as $row) {
                                    ?>
                                    <tr>
                                        <td><a href="#ok" onclick="edit(<?= $row->id; ?>)"><?php echo $row->request_date; ?></a>
                                        </td>
                                        <td><?php echo $row->costumer_request; ?></td>
                                        <td><?php echo $row->whom; ?></td>
                                        <td><?php echo $model->cekStatus($row->status); ?><br><?= $row->request_done; ?>
                                        </td>
                                        <td><?php echo $row->request_by; ?></td>
                                        <td><?php echo $row->note; ?></td>
                                        <td><?php echo $row->review; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                </div>
            </div>
            </form>
    </div>
    </section>
    <!-- modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Import Excel</h4>
                </div>
                <form class="form-horizontal" id="kirim" enctype=" multipart/form-data">
                    <div class="modal-body">
                        <div class="well">
                            <i class="fa-info-circle fa"></i> Information
                            <p>
                                Please download template below before uploading<br>
                                <a href="<?php echo site_url('/excel/mytelkomcel.xlsx'); ?>" id="btnDownloadTemplate" class="btn btn-info">Download
                                    Template Excel</a>
                            </p>
                        </div>
                        <input type="file" name="file" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- akhir modal -->
</div>
</div>
<script>
    $(document).ready(function() {
        $('#startdate').val('<?php echo $startdate; ?>');
        $('#enddate').val('<?php echo $enddate; ?>');

    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        btnCari();
        showTable();


    });

    function proses() {
        var id = $('input[name=id]').val();
        if (id != undefined) {
            prosesupMyTelkomcel(id);
        } else {
            prosesinMyTelkomcel();
        }
    }

    function prosesinMyTelkomcel() {
        event.preventDefault();
        $.ajax({
                url: '<?= site_url("/MyTelkomcelController/inMyTelkomcel"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: $('form').serialize()
            })
            .done(function(data) {

                Swal.fire(
                    'Sukses!',
                    data.msg,
                    'success'
                );
                showTable();
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
    }

    function prosesupMyTelkomcel() {
        event.preventDefault();
        $.ajax({
                url: '<?= site_url("/MyTelkomcelController/upMyTelkomcel"); ?>',
                type: 'POST',
                dataType: 'JSON',
                data: $('form').serialize()
            })
            .done(function(data) {

                Swal.fire(
                    'Sukses!',
                    data.msg,
                    'success'
                );
                showTable();

            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
    }

    function edit(id = '') {
        ubahText(id, 'edit');
        $('#bisa').removeClass("collapse");
        if (id != '') {

            $.ajax({
                    url: '<?= site_url("/MyTelkomcelController/getMT?id="); ?>' + id,
                    type: 'GET',
                    dataType: 'JSON'
                })
                .done(function(data) {
                    $('input[name=request_date]').val(data.request_date);
                    $('input[name=costumer_request]').val(data.costumer_request);
                    $('input[name=whom]').val(data.whom);
                    $('input[name=request_done]').val(data.request_done);
                    $('input[name=request_by]').val(data.request_by);
                    $('input[name=review]').val(data.review);
                    $('textarea[name=note]').text(data.note);
                    $('select[name=status]').val(data.status);

                    $('input[name=review]').removeAttr('readonly');
                    $('#txtAction').removeAttribute('disabled');
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

        }
    }

    function ubahText(id = '', val = '') {

        var r;
        if (val == 'add') {
            r = 'Add';
            $('#txtArea').html(" ");
            $('#txtAction*').text(r);
            $('#upForm').attr('id', '');
            $('#cloneID').html('');
            $('.review').input("disabled", "disabled");
            $(this).attr('disabled', 'disabled');
        } else if (val == 'edit') {
            r = 'Edit';
            $('#txtAction*').text(r);
            $('#txtAction*').removeAttr('disabled');

            $('#inForm').attr('id', '');

            $('#cloneID').html("<input type='hidden' name='id' value='" + id + "'>");
        }

        return r;
    }


    function showTable() {
        // status = $('select[name=status]').val();
        startdate = $('input[name=startdate]').val();
        enddate = $('input[name=enddate]').val();
        // body...
        $('#contoh').DataTable({
            // Processing indicator
            "destroy": true,
            // "searching": true,
            "processing": true,
            // DataTables server-side processing mode
            "serverSide": true,
            "scrollX": true,
            // Initial no order.
            "order": [],
            // Load data from an Ajax source
            "ajax": {
                "url": "<?= site_url("/MyTelkomcelController/dtMT"); ?>?status=" + status + "&startdate=" + startdate + "&enddate=" + enddate,
                "type": "POST"
            },
            //Set column definition initialisation properties
            "columnDefs": [{
                "targets": [2],
                "visible": true,
                "searching": true
            }, {
                "targets": [3],
                "visible": true
            }]
        });


    }


    //import excel
    $(document).ready(function() {

        $('#kirim').submit(function(e) {
            e.preventDefault();
            $.ajax({
                url: "<?php echo site_url('/MyTelkomcelController/importBack'); ?>",
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                dataType: "JSON",
                success: function(data) {
                    if (data.success) {

                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: data.title,
                            customClass: {
                                popup: 'animated tada'
                            },
                            animation: false,
                            html: '<div style="font-size:16px;">' + data.msg +
                                '</div>',
                            showConfirmButton: false,
                            timer: 4000
                        });
                    } else {
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: data.title,
                            customClass: {
                                popup: 'animated tada'
                            },
                            animation: false,
                            html: '<div style="font-size:16px;">' + data.msg +
                                '</div>',
                            showConfirmButton: false,
                            timer: 4000
                        })
                    }
                    $('input[name=file]').val('');
                    $('#myModal').modal('hide');
                    showTable();
                }
            });
        });


    });

    function clearInput() {
        var r;
        $('input[name="request_date"]').val('');
        $('input[name="customer_request"]').val('');
        $('input[name="whom"]').val('');
        $('input[name="request_done"]').val('');
        $('input[name="request_by"]').val('');
        $('input[name="review"]').val('');
        $('input[name="note"]').val('');
        $('select[name="status"]').val('');



    }


    function btnCari() {
        $(document).ready(function() {
            $('#btn-cari').click(function(e) {
                e.preventDefault();

                showTable();

            });
        });
    }
</script>
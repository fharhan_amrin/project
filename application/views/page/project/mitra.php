<div id="main" role="main">
		<?php $this->load->view('template/breadcumb'); ?> 
	<div id="content">
		<!-- widget grid -->
		<section id="widget-grid" class="">

			<!-- row -->
			<?php $this->load->view('template/menu_add'); ?>
			<!-- NEW WIDGET START -->
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>List Mitra Of Project</h2>

				</header>

				<!-- widget div-->
				<div>
					<!-- widget content -->
					<div class="widget-body no-padding">
						<div class="row" style="margin-top: 10px; margin-left: 1px;">
							<div class="col-md-12">
								<a href="javascript:void(0);"  data-toggle="modal" data-target="#add" class="btn btn-labeled btn-default"><span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span><span>Add</span></a>

								<a href="javascript:void(0);" id="linkEdit" class="btn btn-labeled btn-default"><span class="btn-label"><i class="glyphicon glyphicon-minus-sign"></i></span><span>Edit</span></a>

								<a href="javascript:void(0);" onclick="return confirm('Anda yakin ingin mengahpus mitra ini ?')" id="linkDelete" class="btn btn-labeled btn-default"><span class="btn-label"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i></span><span>Delete</span></a>
							</div>
						</div>

						<form method="post" action="<?=site_url('action/inMitraList')?>">
							<input type="hidden" name="id_pc" value="<?=$this->input->get('id_pc');?>">
							<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
								<thead>			                
									<tr>
										<th style="width: 30px;">
										</th>
										<th>Mitra</th>
										<th>PM</th>
										<th>Mobile Number</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($mitra->getMitra()->result() as $v){ ?>
												<tr>
													<td>
														<div class="smart-form">
															<label class="checkbox">
																<input id="cek" <?= $mitra->cekStatus($v->id,$this->input->get('id_pc')) == true ? 'checked' : '' ?> name="id_mitra[]" type="checkbox" value="<?php echo $v->id ?>"><i></i>
															</label>
														</div>
													</td>
													<td><?=$v->mitra;?></td>
													<td><?=$v->pm;?></td>
													<td><?=$v->phone;?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
									<button style="margin: 10px;" type="submit" name="btn" class="btn btn-labeled btn-primary">Lanjut</button>
								</form>

							</div>
						</div>
					</div>
				</section>
			</div>
		</div>

		<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							×
						</button>
						<h4 class="modal-title" id="myModalLabel">Add</h4>
					</div>
					
					<form method="post" action="<?=site_url('action/inMitra');?>" id="formAdd">
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="tags">Mitra</label>
										<input type="text" name="mitra" class="form-control" id="tags" placeholder="Mitra">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="tags">PM</label>
										<input type="text" name="pm" class="form-control" id="tags" placeholder="PM">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="tags">Mobile Number</label>
										<input type="number" name="phone" class="form-control" id="tags" placeholder="Mobile Number">
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">
								Cancel
							</button>
							<button type="submit" class="btn btn-primary">
								Save
							</button>
						</div>
					</form>

				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>


		<script type="text/javascript">
			$('#cek').change(function(event) {
				event.preventDefault();

				var ID = [];
				$.each($("input[name='id[]']:checked"), function(){            
					ID.push($(this).val());
				});

				var id = ID.join(",");
				edit(id);
				hapus(id);
			});	


			function edit(id) {
				$("#linkEdit").attr("href", "<?php echo base_url('index.php/'); ?>Project_list/editMitra?id="+id+"&inp="+<?=$this->input->get('id_pc')?>);
			}

			function hapus(id) {
				$("#linkDelete").attr("href", "<?php echo base_url('index.php/'); ?>action/deMitra?id="+id);
			}

		</script>
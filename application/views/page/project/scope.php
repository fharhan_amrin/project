<div id="main" role="main">
	
	<?php $this->load->view('template/breadcumb'); ?>
	<div id="content">
		<!-- widget grid -->
		<section id="widget-grid" class="">

			<!-- row -->
			<?php $this->load->view('template/menu_add'); ?>
			<!-- NEW WIDGET START -->
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2><?=$smallTitle;?></h2>

				</header>

				<!-- widget div-->
				<div>
					<!-- widget content -->
					<div class="widget-body no-padding">
						<form method="get" action="<?=site_url('Project_List/upScope')?>">
							<input type="hidden" name="id_pc" value="<?=$this->input->get('id_pc');?>">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group" style=" margin:10px;">
										<label>Scope of Work :</label>
										<textarea name="scope" class="form-control" id="note" placeholder="Scope" style="min-height: 200px"><?php  
												$pl = $project->getProjectListID($this->input->get('id_pc'));
												if ($pl->num_rows() > 0) {
													echo $pl->row()->scope;
												}
											?></textarea>
									<input type="submit" name="btn" class="btn btn-labeled btn-primary" value="Save" style="margin-top: 10px;">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

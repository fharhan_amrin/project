<style type="text/css">
    .btn-file {
        position: relative;
        overflow: hidden;
    }

    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        cursor: inherit;
        display: block;
    }
</style>

<?php
function cekDoc($t = "")
{
    $path = '';

    if ($t == 1) {
        $path = "pra_project";
    } else if ($t == 2) {
        $path = "on_project";
    } else if ($t == 3) {
        $path = "Clossing";
    }

    return $path;
}
?>

<div id="main" role="main">
    <!-- breadcrumb -->
    <?php $this->load->view('template/breadcumb'); ?>

    <div id="content">
        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- Menu Add -->
            <?php $this->load->view('template/menu_add'); ?>

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
                <div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-folder-open"></i> </span>
                        <h2>Document</h2>
                    </header>
                    <div style="border-bottom-width: 2px; padding-bottom: 13px;">
                        <div class="widget-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- <a href="javascript:void(0);" data-toggle="modal" data-target="#add"
                                        class="btn btn-labeled btn-default"><span class="btn-label"><i
                                                class="glyphicon glyphicon-plus"></i></span><span>Add</span></a> -->


                                    <!-- <a href="javascript:void(0);" onclick="return confirm('Anda yakin ingin menghapus document ini ?')" id="linkDelete" class="btn btn-labeled btn-default"><span class="btn-label"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i></span><span>Delete</span></a> -->

                                </div>
                            </div><br>

                            <form id="ok" action="<?= base_url('index.php/Project_List/uploadDoc'); ?>" method="post" enctype="multipart/form-data">


                                <div class="col-md-12 col-lg-12 text-left" style="margin-bottom: 18px;">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-12 col-xs-12" style="padding-right: 0px; padding-left: 0px; width: 150px;">
                                            <span class="btn btn-default btn-file" style=" padding-right: 23px; padding-left: 23px; padding-top: 8px; border-bottom-width: 1px; padding-bottom: 8px;">
                                                <i class="fa fa-3x fa-folder-open-o"><br>
                                                    <h5>Pra Project</h5><input type="file" name="pp">
                                                </i>
                                            </span>
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12" style="padding-right: 0px; padding-left: 0px; width: 146px;">
                                            <span class="btn btn-default btn-file" style=" padding-right: 23px; padding-left: 23px; padding-top: 8px; border-bottom-width: 1px; padding-bottom: 8px;">
                                                <i class="fa fa-3x fa-folder-open-o"><br>
                                                    <input type="hidden" name="id_pc" value="<?= $this->input->get('id_pc'); ?>">
                                                    <h5>On Project</h5><input type="file" name="op">
                                                </i>
                                            </span>
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12" style="padding-right: 0px; padding-left: 0px;">
                                            <span class="btn btn-default btn-file" style=" padding-right: 30px; padding-left: 30px; padding-top: 8px; border-bottom-width: 1px; padding-bottom: 8px;">
                                                <i class="fa fa-3x fa-folder-open-o"><br>
                                                    <h5>Clossing</h5><input type="file" name="cl">
                                                </i>
                                            </span>
                                        </div>

                                        <button type="submit" class="btn btn-primary" style="position: relative; right: 34px;">
                                            <div class="col-md-2 col-sm-12 col-xs-12" style="padding-right: 26px; padding-left: 26px; padding-top: -2px;
                                                border-bottom-width: 1px; padding-bottom: 3px;">
                                                <!-- <span class="btn btn-primary btn-file" -->
                                                <!-- style=" padding-right: 30px; padding-left: 30px; padding-top: 8px;
                                                border-bottom-width: 1px; padding-bottom: 8px;"> -->
                                                <i class="fa fa-3x fa-save"><br>
                                                    <h5>Save</h5>
                                                    <!-- <input type="file" name="cl"> -->
                                                </i>
                                                <!-- </span> -->
                                            </div>
                                        </button>

                                        <a href="<?= site_url('Project_List/project_list') ?>">
                                            <button type="button" class="btn btn-danger" style="position: relative; right: 34px;">
                                                <div class="col-md-2 col-sm-12 col-xs-12" style="padding-right: 26px; padding-left: 26px; padding-top: -2px;
                                                border-bottom-width: 1px; padding-bottom: 3px;">
                                                    <!-- <span class="btn btn-primary btn-file" -->
                                                    <!-- style=" padding-right: 30px; padding-left: 30px; padding-top: 8px;
                                                border-bottom-width: 1px; padding-bottom: 8px;"> -->
                                                    <i class="fa fa-3x fa-arrow-right"><br>
                                                        <h5>Back to Project List</h5>
                                                        <!-- <input type="file" name="cl"> -->
                                                    </i>
                                                    <!-- </span> -->
                                                </div>
                                            </button>
                                        </a>

                                    </div>
                                </div>

                                <!-- <input type="submit" name="btn"> -->

                            </form>

                            <div class="tabel">
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th>File</th>
                                            <th>Document</th>
                                            <th>Created Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($document->getDocumentPC()->result() as $v) { ?>
                                            <tr>
                                                <td><a target="_blank" href="<?= site_url('uploads/' . cekDoc($v->id) . '/' . $v->file); ?>"><?= $v->file; ?></a></td>
                                                <td><?= $v->document; ?></td>
                                                <td><?= $v->created_date; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
    </div>
</div>

<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h4 class="modal-title" id="myModalLabel">Add</h4>
            </div>

            <form method="post" action="<?= site_url('action/inStatement'); ?>" id="formAdd">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tags">Document</label>
                                <input type="text" name="document" class="form-control" id="tags" placeholder="Document">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-primary">
                        Save
                    </button>
                </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    function edit(id) {
        $("#linkEdit").attr("href", "<?php echo base_url('index.php/'); ?>Project_list/editStatement?id=" + id + "&inp=" +
            <?= $this->input->get('id_pc') ?>);
    }

    function hapus(id) {
        $("#linkDelete").attr("href", "<?php echo base_url('index.php/'); ?>action/deStatement?id=" + id);
    }
</script>
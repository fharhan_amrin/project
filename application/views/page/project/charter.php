<div id="main" role="main">

	<?php $this->load->view('template/breadcumb'); ?>

	<div id="content">
		<!-- widget grid -->
		<section id="widget-grid" class="">

			<!-- row -->
			<?php $this->load->view('template/menu_add'); ?>
			<!-- NEW WIDGET START -->
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-folder-open"></i> </span>
					<h2>Project Charter</h2>
				</header>

				<!-- widget div-->
				<div>
					<form action="<?php echo base_url('index.php/') . $action ?>" method="<?= $method; ?>" onSubmit="validasi()">
						<?php
						$date = date_default_timezone_set("Asia/Jakarta");
						$date = date("Y-m-d H-m-s");
						?>

						<div class="form-group" style=" margin-bottom: 0;">
							<label>Task : *</label>
							<input type="hidden" name="id" value="<?= $pl['id']; ?>">
							<input type="text" required name="task" class="form-control" id="task" placeholder="Task" value="<?= $pl['task']; ?>">
						</div>

						<div class="form-group" style="margin-top:20px;">
							<label>Detail Task : </label>
							<textarea name="detail_task" id="detail_task" class="form-control" placeholder="Detail Task "><?= $pl['detail_task']; ?></textarea>
						</div>

						<div class="form-group" style="margin-top:20px;">
							<label><b>PIC : </b></label>
							<div class="row">
								<div class="col-md-12">
									<table class="table table-bordered" border="1" style="width: 100%">
										<tr>
											<td>STS</td>
											<td>TCEL</td>
										</tr>
										<tr>
											<td>
												<input type="text" class="form-control" name="pic_sts" id="pic_sts" style="width: 100%;" placeholder="PIC STS" value="<?= $pl['pic_sts']; ?>">
											</td>
											<td>
												<input type="text" class="form-control" name="pic_tcel" id="pic_tcel" style="width: 100%;" placeholder="PIC TCEL" value="<?= $pl['pic_tcel']; ?>">
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label><b>STATUS : </b></label>
							<div class="row">
								<div class="col-md-12">
									<table class="table table-bordered" border="1" style="width: 100%">
										<tr>
											<td>STS</td>
										</tr>
										<tr>
											<td>
												<select class="form-control" name="status" id="status" style="width: 100%;">
													<option value="" disabled=""></option>
													<option <?= $pl['status'] == '1' ? 'selected' : ''; ?> value="1"> DRAFT </option>
													<option value="2" <?= $pl['status'] == '2' ? 'selected' : ''; ?>> ON PROGRESS </option>
													<option value="3" <?= $pl['status'] == '3' ? 'selected' : ''; ?>> DONE </option>
													<option value="4" <?= $pl['status'] == '4' ? 'selected' : ''; ?>> PENDING </option>
												</select>
											</td>

										</tr>
									</table>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Confirm By: *</label>
										<input type="text" required name="confirm" id="confrim" class="form-control" placeholder="Confrim By " value="<?= $pl['confirm']; ?>">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label>Request Via : *</label>
										<input type="text" required name="via" id="requestVa" class="form-control" placeholder="Request Via " value="<?= $pl['via']; ?>">
									</div>
								</div>

								<!-- // -->
								<div class="col-md-6">
									<div class="form-group">
										<label>Start Date :</label>
										<input type="date" name="startdate" id="startdate" class="form-control" placeholder="Confrim By " value="<?= $pl['startdate'] == '' ?  '' : date("Y-m-d", strtotime($pl['startdate'])); ?>">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label>End Date :</label>
										<input type="date" name="enddate" id="enddate" class="form-control" placeholder="Request Via " value="<?= $pl['enddate'] == '' ? '' : date("Y-m-d", strtotime($pl['enddate'])); ?>">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label>Priority :</label>
										<!-- <input type="text"  name="priority" id="priority" class="form-control" placeholder="priority  " value="<?= $pl['priority']; ?>"> -->
										<select name="priority" id="priority" class="form-control">
											<!-- <input type="text"  name="priority" id="priority" class="form-control" placeholder="priority  " value="<?= $pl['priority']; ?>"> -->
											<option value="3" <?= $pl['priority'] == '3' ? 'selected' : '' ?>>Normal</option>
											<option value="2" <?= $pl['priority'] == '2' ? 'selected' : '' ?>>Medium</option>
											<option value="1" <?= $pl['priority'] == '1' ? 'selected' : '' ?>>Immediately</option>
										</select>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label>Unit Tcel :</label>
										<!-- <input type="date" required name="enddate" id="enddate" class="form-control" placeholder="Request Via " value="<?= $pl['enddate']; ?>"> -->
										<select name="subunit_id" id="subunit_id" class="form-control">
											<?php foreach ($subunit as $v) { ?>
												<option <?= $pl['subunit_id'] == $v->id ? "selected" : "" ?> value="<?= $v->id; ?>"><?= $v->sub_unit ?></option>
											<?php } ?>
										</select>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label>Note STS : </label>
										<textarea name="note" <?= $this->session->userdata('level') != 1 ? 'readonly' : ''; ?> id="note" class="form-control" placeholder="Note "><?= $pl['note']; ?></textarea>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label>Review : </label>
										<textarea name="review" id="review" class="form-control" placeholder="Review "><?= $pl['review']; ?></textarea>
									</div>
								</div>
							</div>


							<div class="row" style="margin-bottom: 30px;
						margin-left: 2px;
						">

								<a href="<?php echo base_url('index.php/'); ?>Project_List/project_list" class="btn btn-labeled btn-default"> <span class="btn-label"><i class="glyphicon glyphicon-remove-circle"></i></span>Cancel </a>

								<button class="btn btn-labeled btn-primary"><span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Save</button>

							</div>
					</form>
					<script data-pace-options=' {
				"restartOnRequestAfter": true;
			}		
			' src="js/plugin/pace/pace.min.js"></script>
				</div>
			</div>
		</section>
	</div>
</div>
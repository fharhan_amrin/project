<div id="main" role="main">
	<ol class="breadcrumb">
		<li>Home</li>
		<li>Project</li>
		<li>Input</li>
		<li>Project Charter</li>
	</ol>
	<div id="content">
		<!-- widget grid -->
		<section id="widget-grid" class="">

			<!-- row -->

		<?php $this->load->view('template/menu_add');?>
			<!-- NEW WIDGET START -->
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-folder-open"></i> </span>
					<h2>Project Charter</h2>
				</header>

				<!-- widget div-->
				<div>
					<form action="<?php echo base_url('index.php/'). 'action/updatePc' ?>" method="get">
					<?php foreach ($data as $row){ ?>
						<p>Nama Project : *</p>
						<section>
							<label class="textarea">
								<input type="hidden" name="id[]" value="<?php echo $row->id ?>">
								<textarea rows="3" name="name_project[]" placeholder="" style="resize: none;
								width: 650px;
								height: 80px;
								"><?php echo $row->nama_project; ?></textarea>
							</label>
						</section>

						<div class="row">
							<div class="col-md-6">
								<br>
								<div class="form-group">
									<label>Nomor Dinas : *</label>
									<input type="number" value="<?php echo $row->no; ?>" name="nodin[]" class="form-control" placeholder="Nomor Dinas" >
								</div>

								<div class="form-group">
									<label>Tanggal Nomor Dinas : *</label>
									<div class="smart-form">
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
										<input type="text" value="<?php echo $row->tanggal_nodin; ?>" name="tgl_nodin[]" placeholder="Tanggal No Dinas" class="datepicker" data-dateformat='yy/mm/dd'>
									</label>

								</div>
								</div>

								<div class="form-group">
									<label>PIC TELKOMCEL: *</label>
									<input type="text" value="<?php echo $row->pic_telkomcel; ?>" name="pic_telkomcel[]" class="form-control" placeholder="Person In Charge Telkomcel">
								</div>

								<div class="form-group">
									<label>PIC STS: *</label>
									<input type="text" value="<?php echo $row->pic_sts; ?>" name="pic_sts[]" class="form-control" placeholder="Person In Charge STS">
								</div>

								<label>Segmen : *</label>
								<div id="checkout-form" class="smart-form" novalidate="novalidate">
									<label class="select">  
										<select name="segmen[]">
											<option value=""  disabled="">-- Pilihan --</option>
											<?php foreach ($segmen as $key): ?>
											<option value="<?=$key->id;?>" <?= $key->id == $row->id_segmen ? 'selected' : ''; ?> > <?=$key->segmen;?> </option>
											<?php endforeach ?>

										</select> <i></i> 
									</label>
								</div><br>
							</div>

							<div class="col-md-6">
								<br>


								<label for="nama"> Start Date : *</label>
								<div class="smart-form">
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
										<input type="text" value="<?php echo $row->start_date; ?>" name="start_date[]" placeholder="Start Date" class="datepicker" data-dateformat='yy/mm/dd'>
									</label>

								</div><br>

								<label for="nama"> End Date : *</label>
								<div class="smart-form">
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
										<input type="text" value="<?php echo $row->end_date; ?>" name="end_date[]" placeholder="End Date" class="datepicker" data-dateformat='yy/mm/dd'>
									</label>

								</div><br>

								<label for="nama"> Tanggal Testing : *</label>
								<div class="smart-form">
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
										<input type="text" value="<?php echo $row->tanggal_testing; ?>" name="tgl_testing[]" placeholder="Tanggal Testing" class="datepicker" data-dateformat='yy/mm/dd'>
									</label>

								</div><br>


								<label for="nama">Tanggal Rilis: *</label>
								<div class="smart-form">
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
										<input type="text" value="<?php echo $row->tanggal_rilis; ?>" name="tgl_rilis[]" placeholder="Tanggal Rilis" class="datepicker" data-dateformat='yy/mm/dd'>
									</label>

								</div><br>
								
								<div class="form-group">
									<label>Status: *</label>
									<div id="checkout-form" class="smart-form" novalidate="novalidate">
										<label class="select">  
											<select name="status" id="status">
												<option value="" disabled="">-- Pilihan --</option>
												<option value="1" <?=$row->status == 1 ? "selected" : "";?> > LEAD </option>
												<option value="2" <?=$row->status == 2 ? "selected" : "";?>> DELAY </option>
												<option value="3" <?=$row->status == 3 ? "selected" : "";?>> LAG </option>
												<option value="4" <?=$row->status == 4 ? "selected" : "";?>> DRAFT </option>
												<option value="5" <?=$row->status == 5 ? "selected" : "";?>> CLOSED </option>
											</select> <i></i> 
										</label>
									</div>
								</div>

							</div>
						</div>

						<?php } ?>
						<div class="row" style="margin-bottom: 30px;
						margin-left: 2px;
						">

						<a onclick="return window.history.back();" class="btn btn-labeled btn-default"> <span class="btn-label"><i class="glyphicon glyphicon-remove-circle"></i></span>Cancel </a>

						<button name="submit" class="btn btn-labeled btn-primary"><span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Save</button>

					</div>
				</form>
				<script data-pace-options=' {
				"restartOnRequestAfter": true;
			}		
			' src="js/plugin/pace/pace.min.js"></script>
		</div>
	</div>
</section>
</div>
</div>
<div id="main" role="main">
	<ol class="breadcrumb">
		<li>Home</li>
		<li>Project</li>
		<li>Input</li>
		<li>Project Charter</li>
	</ol>
	<div id="content">
		<!-- widget grid -->
		<section id="widget-grid" class="">

			<!-- NEW WIDGET START -->
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-folder-open"></i> </span>
					<h2>Edit Mitra</h2>
				</header>

				<!-- widget div-->
				<div>
					<form action="<?php echo site_url() . '/action/upMitra' ?>" method="post" onSubmit="validasi()">
						<?php
						$date = date_default_timezone_set("Asia/Jakarta");
						$date = date("Y-m-d H-m-s");
						?>
						<?php $no = 1;
						foreach ($mitra->getMitraAll()->result() as $v) : ?>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="tags">Mitra</label>
										<input type="hidden" name="id[]" value="<?php echo $v->id ?>">
										<input type="hidden" name="inp" value="<?php echo $this->input->get('inp'); ?>">
										<input type="text" name="mitra[]" value="<?= $v->mitra; ?>" class="form-control" id="tags" placeholder="Mitra">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="tags">PM</label>
										<input type="text" name="pm[]" value="<?= $v->pm; ?>" class="form-control" id="tags" placeholder="PM">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="tags">Mobile Number</label>
										_
										<input type="number" name="phone[]" value="<?= $v->phone; ?>" class="form-control" id="tags" placeholder="Mobile Number">
									</div>
								</div>
							</div>
							<hr>
						<?php endforeach ?>


						<div class="row" style="margin-bottom: 30px;
						margin-left: 2px;
						">

							<a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-labeled btn-default"> <span class="btn-label"><i class="glyphicon glyphicon-remove-circle"></i></span>Cancel </a>

							<button class="btn btn-labeled btn-primary"><span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Save</button>

						</div>
					</form>
					<script data-pace-options=' {
				"restartOnRequestAfter": true;
			}		
			' src="js/plugin/pace/pace.min.js"></script>
				</div>
			</div>
		</section>
	</div>
</div>
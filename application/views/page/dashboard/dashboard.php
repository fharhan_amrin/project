<?php $status = array(); ?>
<style type="text/css">
.box {
    padding: 10px;
}

.total {
    font-size: 20px;
    color: #FFF;
}

.jumlah {
    font-size: 18px;
    color: #FFF;
}

/* .highcharts-plot-background {
    fill: #FCFFC5 !important;
}
.highcharts-plot-border {
    stroke-width: 2px !important;
    stroke: #7cb5ec !important;

} */

circle {
    cx: 202.5 !important;
    cy: 176.5 !important;
    r: 120.5 !important;
    fill: none !important;
    stroke: rgb(0, 0, 0) !important;
    stroke-width: 10!important;
}
</style>
<div id="main" role="main">
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Dashboard</li>
    </ol>
    <!-- MAIN CONTENT -->

    <div id="content">


        <div class="row" style="margin-bottom: 10px;">
            <div class="col-md-12">
                <div class="box" style="background: #555;">
                    <div class="row">
                        <div class="col-md-12 text-center">

                            <div class="total">Development Project 
                                <select name="tahun" onchange="chartTahun(this.value)"
                                    style="color: #00BCD4; background: transparent; border: 0;">
                                    <option value="2020">2020</option>
                                </select>
                                </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        

        <div class="row">
          <div class="col-md-6">
            <div id="chartdiv" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
          </div>
           <div class="col-md-6">
             <div id="vas" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
          </div>
        </div>
         <div class="row">
          <div class="col-md-6">
          <div id="mytelkomcel" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
            
          </div>
           <div class="col-md-6">
          <div id="crm" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
            
          </div>
        </div>
    </div>

    <!-- end row -->
    <!-- row -->
    
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    chart();
    vas();
    mytelkomcel();
    crm();
});

function chart(t = '') {
    Highcharts.setOptions({
        colors: ['#00d607', '#f20000', '#0300ed', '#ed8e00', '#000000']
    });

    $.ajax({
        type: "GET",
        url: "<?= site_url('dashboard_new/dataDashboard?tahun='); ?>" + t,
        dataType: "JSON",
        success: function(r) {
            var data = r.data;
            $('#total_dp').text('(' + r.total_dp + ')');

            Highcharts.chart('chartdiv', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    },
                    backgroundColor: 'rgba(255, 255, 255, 0.0)'
                },
                   
                exporting: {
                    enabled: false
                },
                title: {
                    text: 'Development Project <b>('+r.total_dp+')</b>',
                },
                tooltip: {
                    pointFormat: '<b style="font-size:20px;"><{series.name}/b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        innerSize: 200,
                        depth: 45,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}',
                            style: {
                                fontFamily: '\'Lato\', sans-serif',
                                lineHeight: '16px',
                                fontSize: '16px'
                            }
                        }

                    }
                },
                series: [{
                    type: 'pie',
                    data: data,
                }]
            });
        }
    });
}


function vas(t = '') {
    Highcharts.setOptions({
        colors: ['#00d607', '#f20000', '#0300ed', '#ed8e00', '#000000']
    });

    $.ajax({
        type: "GET",
        url: "<?= site_url('dashboard_new/vas?tahun='); ?>" + t,
        dataType: "JSON",
        success: function(r) {
            var data = r.data;
            $('#total_vas').text('(' + r.total_vas + ')');

            Highcharts.chart('vas', {
                
                title: {
                    text: 'Vas <b>('+r.total_vas+')</b>',
                 },
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    },
                    backgroundColor: 'rgba(255, 255, 255, 0.0)'
                },

                exporting: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: '<b style="font-size:20px;"><{series.name}/b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        innerSize: 200,
                        depth: 45,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}',
                            style: {
                                fontFamily: '\'Lato\', sans-serif',
                                lineHeight: '16px',
                                fontSize: '16px'
                            }
                        }

                    }
                },
                series: [{
                    type: 'pie',
                    data: data,
                }]
            });
        }
    });
}

function mytelkomcel(t = '') {
    Highcharts.setOptions({
        colors: ['#00d607', '#f20000', '#0300ed', '#ed8e00', '#000000']
    });

    $.ajax({
        type: "GET",
        url: "<?= site_url('dashboard_new/mytelkomcel?tahun='); ?>" + t,
        dataType: "JSON",
        success: function(r) {
            var data = r.data;
            $('#total_mytelkomcel').text('(' + r.total_mytelkomcel + ')');

            Highcharts.chart('mytelkomcel', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    },
                    backgroundColor: 'rgba(255, 255, 255, 0.0)'
                },
                   
                exporting: {
                    enabled: false
                },
                title: {
                    text: 'Mytelkomcel <b>('+r.total_mytelkomcel+')</b>',

                },
                tooltip: {
                    pointFormat: '<b style="font-size:20px;"><{series.name}/b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        innerSize: 200,
                        depth: 45,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}',
                            style: {
                                fontFamily: '\'Lato\', sans-serif',
                                lineHeight: '16px',
                                fontSize: '16px'
                            }
                        }

                    }
                },
                series: [{
                    type: 'pie',
                    data: data,
                }]
            });
        }
    });
}
function crm(t = '') {
    Highcharts.setOptions({
        colors: ['#00d607', '#f20000', '#0300ed', '#ed8e00', '#000000']
    });

    $.ajax({
        type: "GET",
        url: "<?= site_url('dashboard_new/crm?tahun='); ?>" + t,
        dataType: "JSON",
        success: function(r) {
            var data = r.data;
            $('#total_crm').text('(' + r.total_crm + ')');

            Highcharts.chart('crm', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    },
                    backgroundColor: 'rgba(255, 255, 255, 0.0)'
                },
                   
                exporting: {
                    enabled: false
                },
                title: {
                    text: 'CRM <b>('+r.total_crm+')</b>',
                    
                },
                tooltip: {
                    pointFormat: '<b style="font-size:20px;"><{series.name}/b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        innerSize: 200,
                        depth: 45,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}',
                            style: {
                                fontFamily: '\'Lato\', sans-serif',
                                lineHeight: '16px',
                                fontSize: '16px'
                            }
                        }

                    }
                },
                series: [{
                    type: 'pie',
                    data: data,
                }]
            });
        }
    });
}

function chartTahun(v) {
    chart(v);
    vas(v);
    mytelkomcel(v);
    crm(v);
}
</script>
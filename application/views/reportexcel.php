<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=reportprojectlist.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>






<table border="1" width="100%">
    <thead>
        <tr>

            <th>No</th>
            <th>Request Date</th>
            <th>Task</th>
            <th>Pic Sts</th>
            <th>Pic Tcel</th>
            <th>Status</th>
            <th>Note</th>
            <th>Review</th>
            <th>Confirm</th>
            <th>Via</th>
            <th style="background-color: #F6B914 ">Database</th>

        </tr>
    </thead>
    <tbody id="tblBody">

        <?php
        $no = 1;

        function status($value)
        {
            switch ($value) {
                case '1':
                    return "DRAF";
                    break;
                case '2':
                    return "ON PROGRESS";
                    break;
                case '3':
                    return "DONE";
                    break;
                case '4':
                    return "PENDING";
                    break;

                default:
                    # code...
                    break;
            }
        }

        function statusaktif($value)
        {
            switch ($value) {
                case '0':
                    return "Deleted";
                    break;


                default:
                    # code...
                    break;
            }
        }

        foreach ($data as $row) {
        ?>
            <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $row->tanggal; ?></td>
                <td><?php echo $row->task; ?></td>
                <td><?php echo $row->pic_sts; ?></td>
                <td><?php echo $row->pic_tcel; ?></td>
                <td><?php echo status($row->status); ?></td>
                <td><?php echo $row->note; ?></td>
                <td><?php echo $row->review; ?></td>
                <td><?php echo $row->confirm; ?></td>
                <td><?php echo $row->via; ?></td>
                <td style="background-color: #F6B914"><?php echo statusaktif($row->aktif); ?></td>



            </tr>

        <?php } ?>



    </tbody>

    <!-- </tbody> -->
</table>
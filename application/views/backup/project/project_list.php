<div id="main" role="main">

  <?php $this->load->view('template/breadcumb');?>

  <div id="content">
    <!-- widget grid -->
    <section id="widget-grid">
      <!-- NEW WIDGET START -->
      <!-- Widget ID (each widget will need unique ID)-->
      <div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
        <header>
          <span class="widget-icon"> <i class="fa fa-table"></i> </span>
          <h2><?=$smallTitle;?></h2>
        </header>

        <!-- widget div-->
        <div>
          <!-- widget content -->
          <div class="widget-body no-padding">
            <div class="row" style="margin-top: 10px; margin-left: 1px;">
              <div class="col-md-12">
                <a href="<?php echo base_url(); ?>Project_list/charter" class="btn btn-labeled btn-default"><span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span><span>Add</span></a>

                <a id="edit" class="btn btn-labeled btn-default"><span class="btn-label"><i class="fa fa-fw fa-pencil-square-o"></i></span><span>Edit</span></a>

                <a class="btn btn-labeled btn-default" id="btn-delete" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ? ')"><span class="btn-label"><i class="glyphicon glyphicon-minus-sign"></i></span><span>Delete</span></a>
              </div>
            </div>
            <form method="post" action="<?php echo base_url('query/delete') ?>" id="form-delete">
              <table id="dt_basic contoh" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Task</th>
                    <th colspan="2">PIC</th>
                    <th colspan="2">Status</th>
                    <th>Note</th>
                    <th>Review</th>
                  </tr>
                  <tr>
                    <td colspan="2"></td>
                    <td>STS</td>
                    <td>TCEL</td>
                    <td>STS</td>
                    <td>TCEL</td>
                    <td colspan="2"></td>
                  </tr>
                </thead>
                <tbody>
                  <?php
foreach ($project->se()->result() as $row) {
    ?>
                    <tr>
                      <td>
                        <input onchange="test(this)" type="checkbox" name="id[]" value="<?php echo $row->id ?>">
                      </td>
                      <td><?php echo $row->task ?></td>
                      <td><?php echo $row->pic_sts; ?></td>
                      <td><?php echo $row->pic_tcel; ?></td>
                      <td><?php echo $row->s_sts; ?><br><?php echo $row->ket_s_sts; ?></td>
                      <td><?php echo $row->s_tcel; ?><br><?php echo $row->ket_s_tcel; ?></td>
                      <td><?php echo $project->cekStatus($row->status); ?></td>
                      <td><?php echo $row->note; ?></td>
                      <td><?php echo $row->review; ?></td>
                    </tr>
                  <?php }?>
                </tbody>
              </table>
          </div>
        </div>
        </form>
      </div>
    </section>
  </div>
</div>
<script src="<?php echo base_url('assets/jquery.min.js'); ?>"></script>
<script type="text/javascript">
  $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
    showTable();


    $("#btn-delete").click(function() { // Ketika user mengklik tombol delete
      var confirm = window.confirm("Apakah Anda yakin ingin menghapus data-data ini?"); // Buat sebuah alert konfirmasi

      if (confirm) // Jika user mengklik tombol "Ok"
        $("#form-delete").submit(); // Submit form
    });

  });

  function test(id) {
    var ID = [];
    $.each($("input[name='id[]']:checked"), function() {
      ID.push($(this).val());
    });

    var id = ID.join(",");
    edit(id);
    hapus(id);
  }

  function edit(id) {
    $("#edit").attr("href", "<?php echo base_url(); ?>project_list/viewUpPc?id=" + id);
  }

  function hapus(id) {
    $("#btn-delete").attr("href", "<?php echo base_url(); ?>action/deletePc?id=" + id);
  }

  
</script>
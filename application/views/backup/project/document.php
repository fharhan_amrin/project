<style type="text/css">
.btn-file {
	position: relative;
	overflow: hidden;
}
.btn-file input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	min-width: 100%;
	min-height: 100%;
	font-size: 100px;
	text-align: right;
	filter: alpha(opacity=0);
	opacity: 0;
	outline: none;   
	cursor: inherit;
	display: block;
}
</style>
<div id="main" role="main">
	<!-- breadcrumb -->
	<ol class="breadcrumb">
		<li>Home</li>
		<li>Project</li>
		<li>Input</li>
		<li>Document</li>
	</ol>
	<div id="content">
		<!-- widget grid -->
		<section id="widget-grid" class="">

			<!-- row -->
			<div class="row">
				<div class="col-md-12">&nbsp;&nbsp;
					<a href="charter" class="btn btn-default">Project Charter</a>
					<a href="mitra" class="btn btn-default">Mitra Project</a>
					<a href="statement" class="btn btn-default">Statement Of Work</a>
					<a href="document" class="btn btn-danger">Document</a>
				</div>
			</div><br>
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-folder-open"></i> </span>
						<h2>Document</h2>
					</header>
					<div style="border-bottom-width: 2px; padding-bottom: 13px;">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-12">
<!-- 									<a href="javascript:void(0);"  data-toggle="modal" data-target="#add" class="btn btn-labeled btn-default"><span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span><span>Add</span></a> -->


								<!-- <a href="javascript:void(0);" onclick="return confirm('Anda yakin ingin menghapus document ini ?')" id="linkDelete" class="btn btn-labeled btn-default"><span class="btn-label"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i></span><span>Delete</span></a> -->

								</div>
							</div><br>
							
							<form id="ok" action="<?= base_url('action/uploadDoc'); ?>" method="post" enctype="multipart/form-data">

							<div class="col-md-12 col-lg-12 text-left" style="margin-bottom: 18px;">
								<div class="row">
									<div class="col-md-2 col-sm-12 col-xs-12" style="padding-right: 0px; padding-left: 0px; width: 150px;">
										<span class="btn btn-default btn-file" style=" padding-right: 23px; padding-left: 23px; padding-top: 8px; border-bottom-width: 1px; padding-bottom: 8px;">
											<i class="fa fa-3x fa-folder-open-o"><br>
												<h5>Pra Project</h5><input type="file" name="pp">
											</i>
										</span>
									</div>
									<div class="col-md-2 col-sm-12 col-xs-12" style="padding-right: 0px; padding-left: 0px; width: 146px;">
										<span class="btn btn-default btn-file" style=" padding-right: 23px; padding-left: 23px; padding-top: 8px; border-bottom-width: 1px; padding-bottom: 8px;">
											<i class="fa fa-3x fa-folder-open-o"><br>
												<input type="hidden" name="id_pc" value="<?=$this->input->get('id_pc');?>">
												<h5>On Project</h5><input type="file" name="op">
											</i>
										</span>
									</div>
									<div class="col-md-2 col-sm-12 col-xs-12" style="padding-right: 0px; padding-left: 0px;">
										<span class="btn btn-default btn-file" style=" padding-right: 30px; padding-left: 30px; padding-top: 8px; border-bottom-width: 1px; padding-bottom: 8px;">
											<i class="fa fa-3x fa-folder-open-o"><br>
												<h5>Clossing</h5><input type="file" name="cl">
											</i>
										</span>
									</div>
								</div>
							</div>

							<input type="submit" name="btn">

							</form>

							<div class="tabel" >
								<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
										<thead>			                
											<tr>
												<th>File</th>
												<th>Document</th>
												<th>Created Date</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($document->getDocumentPC()->result() as $v){ ?>
											<tr>
												<td><?=$v->file;?></td>
												<td><?=$v->document;?></td>
												<td><?=$v->created_date;?></td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
							</div>
						</div>
					</div>
				</div>
			</article>
		</div>
	</div>

	<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							×
						</button>
						<h4 class="modal-title" id="myModalLabel">Add</h4>
					</div>
					
					<form method="post" action="<?=site_url('action/inStatement');?>" id="formAdd">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="tags">Document</label>
									<input type="text" name="document"  class="form-control" id="tags" placeholder="Document">
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">
							Cancel
						</button>
						<button type="submit" class="btn btn-primary">
							Save
						</button>
					</div>
					</form>

				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">



 function edit(id) {
   $("#linkEdit").attr("href", "<?php echo base_url(); ?>Project_list/editStatement?id="+id+"&inp="+<?=$this->input->get('id_pc')?>);
 }

 function hapus(id) {
   $("#linkDelete").attr("href", "<?php echo base_url(); ?>action/deStatement?id="+id);
 }

</script>
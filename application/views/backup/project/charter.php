<div id="main" role="main">
	
	<?php $this->load->view('template/breadcumb'); ?> 

	<div id="content">
		<!-- widget grid -->
		<section id="widget-grid" class="">

			<!-- row -->
			<?php $this->load->view('template/menu_add'); ?>
			<!-- NEW WIDGET START -->
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-default" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-folder-open"></i> </span>
					<h2>Project Charter</h2>
				</header>

				<!-- widget div-->
				<div>
					<form action="<?php echo base_url().$action ?>" method="<?=$method;?>" onSubmit="validasi()">
						<?php 
						$date = date_default_timezone_set("Asia/Jakarta");
						$date = date("Y-m-d H-m-s");
						?>

						<div class="form-group" style=" margin-bottom: 0;">
							<label>Nama Project : *</label>
							<input type="hidden" name="id[]" value="<?=$project['id'];?>">
							<input type="text" name="name_project" class="form-control" id="name_project" placeholder="Nama Project" value="<?=$project['nama_project'];?>">
						</div>

						<div class="form-group" style=" margin-top: 10px;">
							<label>Note : *</label>
							<textarea name="note" class="form-control" id="note" placeholder="Note" style="min-height: 200px"><?=$project['note'];?></textarea>
						</div>

						<div class="row">
							<div class="col-md-6">
								<br>
								<div class="form-group">
									<label>Nomor Dinas : *</label>
									<input type="number" name="nodin" id="nodin" class="form-control" placeholder="Nomor Dinas" value="<?=$project['nota_dinas'];?>" >
								</div>

								<div class="form-group">
									<label>Tanggal Nomor Dinas : *</label>
									<div class="smart-form">
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
										<input type="text" autocomplete="off" name="tgl_nodin" id="tgl_nodin" placeholder="Tanggal No Dinas" class="datepicker" value="<?=$project['tanggal_nodin'];?>" data-dateformat='yy/mm/dd'>
									</label>

								</div>
								</div>

								<div class="form-group">
									<label>PIC TELKOMCEL: *</label>
									<input type="text" name="pic_telkomcel" id="pic_telkomcel" class="form-control" placeholder="Person In Charge Telkomcel" value="<?=$project['pic_telkomcel'];?>">
								</div>

								<div class="form-group">
									<label>PIC STS: *</label>
									<input type="text" name="pic_sts" id="pic_sts" class="form-control" placeholder="Person In Charge STS" value="<?=$project['pic_sts'];?>">
								</div>

								<label>Segmen : *</label>
								<div id="checkout-form" class="smart-form" novalidate="novalidate">
									<label class="select">  
										<select name="segmen" id="segmen">
											<option value="" selected="" disabled="">-- Pilihan --</option>
											<option value="1"> BSS </option>
											<option value="2"> VAS </option>
											<option value="3"> CORE </option>
										</select> <i></i> 
									</label>
								</div><br>

								<div class="form-group">
									<label>Confirm By: *</label>
									<input type="text" name="confirm" value="<?=$project['confirm'];?>" id="confrim" class="form-control" placeholder="Confrim By ">
								</div>

							</div>

							<div class="col-md-6">
								
								<br>
								<label for="nama"> Request Date : *</label>
								<div class="smart-form">
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
										<input type="text" autocomplete="off" name="start_date" id="start_date" placeholder="Start Date" value="<?=$project['start_date'];?>" class="datepicker" data-dateformat='yy/mm/dd'>
									</label>

								</div><br>

								<label for="nama"> End Date : *</label>
								<div class="smart-form">
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
										<input type="text" autocomplete="off" name="end_date" id="end_date" placeholder="End Date" class="datepicker" value="<?=$project['end_date'];?>" data-dateformat='yy/mm/dd'>
									</label>

								</div><br>

								<label for="nama">Testing Date : *</label>
								<div class="smart-form">
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
										<input type="text" autocomplete="off" name="tgl_testing" id="tgl_testing" placeholder="Testing" class="datepicker" value="<?=$project['tanggal_testing'];?>"  data-dateformat='yy/mm/dd'>
									</label>
								</div><br>


								<label for="nama">Release Date : *</label>
								<div class="smart-form">
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
										<input type="text" autocomplete="off" name="tgl_rilis" id="tgl_rilis" placeholder="Release Date " value="<?=$project['tanggal_rilis'];?>" class="datepicker" data-dateformat='yy/mm/dd'>
									</label>
								</div><br>
								
								<div class="form-group">
									<label>Status: *</label>
									<div id="checkout-form" class="smart-form" novalidate="novalidate">
										<label class="select">  
											<select name="status" id="status">
												<option value="" disabled="">-- Pilihan --</option>
												<option value="1" selected=""> DRAFT </option>
												<option value="2"> ON PROGRESS </option>
												<option value="3"> DONE </option>
												<option value="4"> PANDING </option>
											</select> <i></i> 
										</label>
									</div>
								</div>

								<div class="form-group">
									<label>Request Via : *</label>
									<input type="text" name="via" id="requestVa" value="<?=$project['via'];?>" class="form-control" placeholder="Request Via ">
								</div>

							</div>
						</div>

						<div class="row" style="margin-bottom: 30px;
						margin-left: 2px;
						">

						<a href="<?php echo base_url(); ?>Project_list/project_list" class="btn btn-labeled btn-default"> <span class="btn-label"><i class="glyphicon glyphicon-remove-circle"></i></span>Cancel </a>

						<button class="btn btn-labeled btn-primary"><span class="btn-label"><i class="glyphicon glyphicon-download-alt"></i></span>Save</button>

					</div>
				</form>
				<script data-pace-options=' {
				"restartOnRequestAfter": true;
			}		
			' src="js/plugin/pace/pace.min.js"></script>
		</div>
	</div>
</section>
</div>
</div>

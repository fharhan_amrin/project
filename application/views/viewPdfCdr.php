<style>
    #customers {
        font-family: "Times New Roman", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td,
    #customers th {
        border: 1px solid #ddd;
        padding: 8px;
        text-align: center;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: center;
        background-color: rgb(115, 184, 255);
        color: black;
    }

    #customers tbody,
    thead {
        font-size: 14px;
    }

    .page_break {
        page-break-before: always;
    }
</style>
<img src="assets/sts.jpeg" alt="">
<img src="assets/telcomcel.jpeg" style="float:right">
<h2 style="text-align: center"><?php echo $smallTitle; ?></h2>
<!-- <?php echo $startdate; ?> -->

<table id="customers" width="100%">
    <thead>
        <tr>
            <th>No</th>
            <th>Request <br>Date</th>
            <th>Costumer Request</th>
            <th>Whom</th>
            <th>Date <br>& <br>Status</th>
            <!-- <th>status</th> -->
            <th>Request <br>by</th>
            <!-- <th>note</th> -->
            <th>review</th>
            <!-- <th>created_date</th> -->
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($project->se($status)->result() as $row) {
            ?>
            <tr>
                <td>
                    <?= $no++; ?>
                </td>

                <td><?php echo $row->request_date; ?></td>
                <td><?php echo $row->costumer_request; ?></td>
                <td><?php echo $row->whom; ?></td>
                <td><?php echo $project->cekStatus($row->status); ?> <br>
                    <?php echo $row->request_done; ?></td>
                <td><?php echo nl2br($row->request_by); ?></td>
                <!-- <td><?php echo nl2br($row->note); ?></td> -->
                <td><?php echo nl2br($row->review); ?></td>
                <!-- <td><?php echo nl2br($row->created_date); ?></td> -->

            </tr>
        <?php } ?>
    </tbody>

</table>
<div class="page_break"></div>
<div style="margin-top:30px;"></div>
<img src="assets/sts.jpeg" alt="">
<img src="assets/telcomcel.jpeg" style="float:right">
<h2 style="text-align: center"><?php echo $smallTitle; ?></h2>
<h2>Manage Service Sign-off :</h2>
<h3>Menyetujui :</h3>
<table id="customers" width="100%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Company Name</th>
            <th>Signature</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Mohamad Reza <br>
                <span style="color: grey;">(Project Manager)</span></td>
            <td>PT. SHIBLY TEKNOLOGI SOLUSI</td>
            <td rowspan="2"></td>
        </tr>
        <tr>
            <td>Bayu Setiadi Laksana <br>
                <span style="color: grey;">(Manager Core and Data Comm)</span></td>
            <td>TELKOMCEL</td>
        </tr>

    </tbody>
</table>
<br>
<h3>Mengetahui :</h3>
<table id="customers" width="100%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Company Name</th>
            <th>Signature</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Sudin <br>
                <span style="color: grey;">(VP NITS)</span></td>
            <td>TELKOMCEL</td>
            <td rowspan="1"></td>
        </tr>

    </tbody>
</table>
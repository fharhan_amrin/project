<header id="header" style="background:#000;">
    <div id="logo-group">
        <!-- PLACE YOUR LOGO HERE -->
        <span> <img src="<?php echo base_url() ?>template/img/telkomcel.png" alt="SmartAdmin"> </span>
        <!-- END LOGO PLACEHOLDER -->
    </div>
    <!-- pulled right: nav area -->
    <div class="pull-right">
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" title="Collapse Menu" data-action="toggleMenu" style="
    background: #FFF;
    border: 0;
    border-radius: 14px;
"><i class="fa fa-reorder" style="
    line-height: 1.7;
    color: #000;
"></i></a>
            </span>
        </div>
        <!-- end collapse menu -->
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="<?php echo site_url('/login/signOut'); ?>" title="Sign Out" data-action="userLogout" data-logout-msg="Apakah anda yakin ingin logout ? " style="border: 0;
    background: #d22f31;
    color: #FFF;
    font-size: 14px;
    padding: 3px 10px;"><i class="fa fa-sign-out"></i> Logout</a>
            </span>
        </div>
        <!-- end logout button -->
    </div>
    <!-- end pulled right: nav area -->
</header>

<aside id="left-panel" style="    background: #FFF;
    border-right: solid 1px #DDD;">
    <nav>
        <ul>
            <li <?= $this->uri->segment(1) == 'dashboard' ? 'class="active"' : ''; ?>>
                <a href="#" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i><span class="menu-item-parent">Dashboard</span></a>
                <ul style="display:block">
                    <li <?= $this->uri->segment(2) == 'dashboard' ? 'class="active" ' : ''; ?>>
                        <a href="<?php echo base_url('index.php/'); ?>dashboard" title="Dashboard"><span class="menu-item-parent">2019</span></a>
                    </li>
                    <li <?= $this->uri->segment(2) == 'dashboard' ? 'class="active" ' : ''; ?>>
                        <a href="<?php echo base_url('index.php/'); ?>dashboard_new" title="Dashboard"><span class="menu-item-parent">2020</span></a>
                    </li>
                </ul>
            </li>

            <li <?= $this->uri->segment(1) == 'project_list' ? 'class="open active"' : ''; ?>>
                <a href="#" title="Project"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Development Project </span></a>

                <ul <?= $this->uri->segment(1) == 'project_list' ? 'style="display: block"' : ''; ?>>
                    <li <?= $this->uri->segment(2) == 'project_list' ? 'class="active" ' : ''; ?>>
                        <a href="<?php echo base_url('index.php/'); ?>Project_List/project_list" title="Project List"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Project List</span></a>
                    </li>
                    <ul style="display: block;">
                        <li <?= $this->input->get('status') == '1' ? 'class="active" ' : ''; ?>>
                            <a href="<?php echo base_url('index.php/'); ?>Project_List/project_list?status=1" title="Draft List"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Draft List</span></a>
                        </li>
                        <li <?= $this->input->get('status') == '2' ? 'class="active" ' : ''; ?>>
                            <a href="<?php echo base_url('index.php/'); ?>Project_List/project_list?status=2" title="On Progress List"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">On Progress List</span></a>
                        </li>
                        <li <?= $this->input->get('status') == '3' ? 'class="active" ' : ''; ?>>
                            <a href="<?php echo base_url('index.php/'); ?>Project_List/project_list?status=3" title="Done List"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Done List</span></a>
                        </li>
                        <li <?= $this->input->get('status') == '4' ? 'class="active" ' : ''; ?>>
                            <a href="<?php echo base_url('index.php/'); ?>Project_List/project_list?status=4" title="Panding List"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Pending List</span></a>
                        </li>
                    </ul>
                </ul>
            </li>

            <li <?= $this->uri->segment(1) == 'ManageService' ? 'class="open active"' : ''; ?>>
                <a href="#" title="Manage Service"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Manage Service</span></a>
                <ul <?= $this->uri->segment(1) == 'ManageService' ? 'style="display: block"' : ''; ?>>
                    <li <?= $this->uri->segment(2) == 'VAS' ? 'class="active" ' : ''; ?>>
                        <a href="<?= base_url('index.php/'); ?>ManageService/customerDailyRequest" title="Input"><span class="menu-item-parent">VAS</span></a>
                    </li>
                    <ul style="display:block">
                        <li <?= $this->uri->segment(2) == 'customerDailyRequest' ? 'class="active" ' : ''; ?>>
                            <a href="<?php echo base_url('index.php/'); ?>ManageService/customerDailyRequest" title="Input"><span class="menu-item-parent">Costumer Daily Request</span></a>
                        </li>
                        <!-- <li <?= $this->uri->segment(2) == 'services' ? 'class="active" ' : ''; ?>>
                        <a href="<?php echo base_url('index.php/'); ?>ManageService/services" title="On Going"> <span class="menu-item-parent">List Services STS</span></a>
                    </li> -->
                        <li <?= $this->uri->segment(2) == 'logDailyActivity' ? 'class="active" ' : ''; ?>>
                            <a href="<?php echo base_url('index.php/'); ?>ManageService/logDailyActivity" title="On Going"> <span class="menu-item-parent">Log Daily Activity</span></a>
                        </li>
                        <!-- <li <?= $this->uri->segment(2) == 'dailyMonitor' ? 'class="active" ' : ''; ?>>
							<a href="<?php echo base_url('index.php/'); ?>ManageService/dailyMonitor" title="Closed"> <span class="menu-item-parent">Daily Server Statistic & Disk usage Monitoring</span></a>
						</li> -->
                        <li <?= $this->uri->segment(2) == 'dailyDevice' ? 'class="active" ' : ''; ?>>
                            <a href="<?php echo base_url('index.php/'); ?>ManageService/dailyDevice" title="Closed"> <span class="menu-item-parent">Log Daily Device</span></a>
                        </li>
                    </ul>
                    <li <?= $this->uri->segment(2) == 'myTelkomcel' ? 'class="active" ' : ''; ?>>
                        <a href="<?php echo base_url('index.php/'); ?>myTelkomcelController/myTelkomcel" title="Closed"> <span class="menu-item-parent">My Telkomcel</span></a>
                    </li>
                    <li <?= $this->uri->segment(2) == 'CRM' ? 'class="active" ' : ''; ?>>
                        <a href="<?php echo base_url('index.php/'); ?>CRMController/CRM" title="Closed"> <span class="menu-item-parent">CRM</span></a>
                    </li>
                </ul>
            </li>





            <li <?= $this->uri->segment(5) == 'project_list_new' ? 'class="open active"' : ''; ?>>
                <a href="#" title="Project"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Document Release </span></a>

                <ul <?= $this->uri->segment(5) == 'project_list_new' ? 'style="display: block"' : ''; ?>>
                    <li <?= $this->uri->segment(6) == 'project_list_new' ? 'class="active" ' : ''; ?>>
                        <a href="<?php echo base_url('index.php/'); ?>Project_List_Release_Document/project_list" title="Document Release"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent"> Document </span></a>
                    </li>
                    <ul style="display: block;">
                        <li <?= $this->input->get('status_release') == '0' ? 'class="active" ' : ''; ?>>
                            <a href="<?php echo base_url('index.php/'); ?>Project_List_Release_Document/project_list?status_release=0" title="Draft List"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Before Release</span></a>
                        </li>
                        <li <?= $this->input->get('status_release') == '1' ? 'class="active" ' : ''; ?>>
                            <a href="<?php echo base_url('index.php/'); ?>Project_List_Release_Document/project_list?status_release=1" title="On Progress List"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Post Release</span></a>
                        </li>
                        <li <?= $this->input->get('status_release') == '2' ? 'class="active" ' : ''; ?>>
                            <a href="<?php echo base_url('index.php/'); ?>Project_List_Release_Document/project_list?status_release=2" title="Done List"><i class="fa fa-lg fa-fw fa-gear"></i> <span class="menu-item-parent">Terminasi</span></a>
                        </li>

                    </ul>
                </ul>
            </li>
        </ul>
    </nav>
    <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
</aside>
<!-- END NAVIGATION -->
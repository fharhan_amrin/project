<style>
    #customers {
        font-family: "Times New Roman", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td,
    #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }

    #customers tbody,
    thead {
        font-size: 14px;
    }
</style>

<h2 style="text-align: center"><?php echo $smallTitle; ?></h2>
<table id="customers" width="100%">
    <thead>
        <tr>
            <th>No</th>
            <th>request_date</th>
            <th>costumer_request</th>
            <th>whom</th>
            <th>request_done</th>
            <th>status</th>
            <th>request_by</th>
            <th>note</th>
            <th>review</th>
            <th>created_date</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        foreach ($project->se($status)->result() as $row) {
            ?>
            <tr>
                <td>
                    <?= $no++; ?>
                </td>

                <td><?php echo $row->request_date; ?></td>
                <td><?php echo $row->services; ?></td>
                <td><?php echo $row->whom; ?></td>
                <td><?php echo $row->request_done; ?></td>
                <td><?php echo $project->cekStatus($row->status); ?></td>
                <td><?php echo nl2br($row->request_by); ?></td>
                <td><?php echo nl2br($row->note); ?></td>
                <td><?php echo nl2br($row->review); ?></td>
                <td><?php echo nl2br($row->created_date); ?></td>

            </tr>
        <?php } ?>
    </tbody>
</table>
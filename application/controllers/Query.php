<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Query extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('M_pc');
        if (!$this->session->userdata('id')) {
            redirect('/');
        }
    }

    public function addPc()
    {

        $date = date_default_timezone_set("Asia/Jakarta");
        $date = date("Y-m-d H-m-s");

        $name_project = $this->input->post('name_project');
        $nodin = $this->input->post('nodin');
        $tgl_nodin = $this->input->post('tgl_nodin');
        $pic_telkomcel = $this->input->post('pic_telkomcel');
        $pic_sts = $this->input->post('pic_sts');
        $segmen = $this->input->post('segmen');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $tgl_testing = $this->input->post('tgl_testing');
        $tgl_rilis = $this->input->post('tgl_rilis');
        $status = $this->input->post('status');
        $tgl_update = $date;

        if ($name_project && $nodin && $tgl_nodin && $pic_sts && $pic_telkomcel && $segmen && $start_date && $end_date && $tgl_testing && $tgl_rilis && $status) {
            $data = array(
                'nama_project' => $name_project,
                'no' => $nodin,
                'tanggal_nodin' => $tgl_nodin,
                'pic_telkomcel' => $pic_telkomcel,
                'pic_sts' => $pic_sts,
                'id_segmen' => $segmen,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'tanggal_testing' => $tgl_testing,
                'tanggal_rilis' => $tgl_rilis,
                'status' => $status,
            );
            $this->M_pc->insert($data, 'project_list');
            redirect('query/tampilPc');
        } else {
            echo "<script>alert('Anda harus mengisi data dengan lengkap !');window.history.back();</script>";
        }
    }

    public function tampilPc()
    {
        $data['project'] = $this->M_pc;
        redirect('page/project_list', $data);
    }

    public function update()
    {
        $arr = array();
        $id = $this->input->get('id');
        for ($i = 0; $i < count($id); $i++) {
            $name_project = $this->input->get('name_project')[$i];
            $nodin = $this->input->get('nodin')[$i];
            $tgl_nodin = $this->input->get('tgl_nodin')[$i];
            $pic_telkomcel = $this->input->get('pic_telkomcel')[$i];
            $pic_sts = $this->input->get('pic_sts')[$i];
            $segmen = $this->input->get('segmen')[$i];
            $start_date = $this->input->get('start_date')[$i];
            $end_date = $this->input->get('end_date')[$i];
            $tgl_testing = $this->input->get('tgl_testing')[$i];
            $tgl_rilis = $this->input->get('tgl_rilis')[$i];
            $status = $this->input->get('status')[$i];

            $ok = array(
                'id' => $id[$i],
                'nama_project' => $name_project,
                'no' => $nodin,
                'tanggal_nodin' => $tgl_nodin,
                'pic_telkomcel' => $pic_telkomcel,
                'pic_sts' => $pic_sts,
                'id_segmen' => $segmen,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'tanggal_testing' => $tgl_testing,
                'tanggal_rilis' => $tgl_rilis,
                'status' => $status,
            );

            array_push($arr, $ok);

        }

        $this->M_pc->update($arr);
        redirect('Project_List/project_list');
    }

    public function delete()
    {
        $id = $_POST['id'];
        if ($id != '') {
            $this->M_pc->de($id);
        }
        redirect('Project_List/project_list');
    }

}

/* End of file ProjectCharter.php */
/* Location: ./application/controllers/ProjectCharter.php */
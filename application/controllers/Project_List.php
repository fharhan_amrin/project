<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Project_List extends MY_Controller
{
    private $filename = "data"; // Kita tentukan nama filenya
    private $id = 0;
    private $result = [];

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('M_pc');
        $this->load->model('ProjectListModel', 'plm');
        $this->load->model('MitraModel', 'mm');
        $this->load->model('StatementModel', 'sm');
        $this->load->model('DocumentModel', 'dm');

        //  OPEN :: LOG MODEL
        $this->load->model('LogModel', 'lm');

        // $this->load->library('Cetak');

        // Daftarin id
        $this->id = $this->session->userdata('id');
        if (!$this->session->userdata('id')) {
            redirect('/');
        }
    }

    public function index()
    {
        $data = [
            'report' => $this->M_pc->get_data_project(),
        ];
        // echo json_encode($data['report']);
        // echo $this->session->userdata('id');
        // $this->render_page('page/dashboard/index', $data);
    }

    public function cetakpdf()
    {
        // require_once APPPATH . 'third_party/wordwrap/wordwrap.php';
        $pdf = new PDF('l', 'mm', 'A4');
        // membuat halaman baru
        $pdf->AddPage();
        $pdf->SetFont('Arial', '', 12);
        // $text = str_repeat('this is a word wrap test ', 20);
        // $nb = $pdf->WordWrap($text, 120);
        // $pdf->Write(5, "This paragraph has $nb lines:\n\n");
        // $pdf->Write(5, $text);
        // $pdf->Output();
        // $pdf->SetFont('Arial', 'B', 16);
        // // mencetak string
        // $pdf->Cell(260, 7, 'Data Project List', 0, 1, 'C');
        // $pdf->SetFont('Arial', 'B', 22);
        // $pdf->Cell(190, 7, 'DAFTAR SISWA KELAS IX JURUSAN REKAYASA PERANGKAT LUNAK', 0, 1, 'C');
        // // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(20, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(40, 6, 'Task', 1, 0);
        $pdf->Cell(46, 6, 'Pic STS', 1, 0);
        $pdf->Cell(46, 6, 'Pic Tcel', 1, 0);
        $pdf->Cell(46, 6, 'Status', 1, 0);
        $pdf->Cell(46, 6, 'Note', 1, 0);
        $pdf->Cell(46, 6, 'Review', 1, 1);
        $pdf->SetFont('Arial', '', 10);
        $mahasiswa = $this->db->get('pl')->result();
        foreach ($mahasiswa as $row) {
            $nb = $pdf->WordWrap($row->task, 120);
            $pdf->Cell(40, 6, $nb, 1, 0);
            $pdf->Cell(46, 6, $row->pic_sts, 1, 0);
            $pdf->Cell(46, 6, $row->pic_tcel, 1, 0);

            $pdf->Cell(46, 6, $this->plm->cekStatus($row->status), 1, 0);
            $pdf->Cell(46, 6, $row->note, 1, 0);
            $pdf->Cell(46, 6, $row->review, 1, 1);
        }
        $pdf->Output();
    }

    public function import()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Upload
        $this->filename = $this->upload();

        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('uploads/project_list/' . $this->filename); // Load file yang telah diupload ke folder excel
        $getSheet = $loadexcel->getSheetNames();

        foreach ($getSheet as $rows) {
            $sheet = $loadexcel->getSheetByName($rows)->toArray(null, true, true, true);
            // $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
            // var_dump($sheet);
            // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
            $data = [];

            $numrow = 1;
            foreach ($sheet as $row) {
                // Cek $numrow apakah lebih dari 1
                // Artinya karena baris pertama adalah nama-nama kolom
                // Jadi dilewat saja, tidak usah diimport
                if ($numrow > 1) {
                    // Kita push (add) array data ke variabel data
                    array_push($data, array(
                        'No' => $row['A'], // Insert data nis dari kolom A di excel
                        'Naran_Estudante' => $row['B'], // Insert data nis dari kolom A di excel
                        'Nre' => $row['C'], // Insert data nis dari kolom A di excel
                        'Faculty' => $row['D'], // Insert data nis dari kolom A di excel
                        'Department' => $row['E'], // Insert data nis dari kolom A di excel
                        // 'via' => $row['F'], // Insert data nis dari kolom A di excel
                        // 'note' => $row['G'], // Insert data nis dari kolom A di excel
                        // 'review' => $row['H'], // Insert data nis dari kolom A di excel
                        // 'tanggal' => date('Y-m-d H:i:s'), // Insert data nis dari kolom A di excel
                        // 'scope' => $row['I'], // Insert data nis dari kolom A di excel

                    ));
                }

                $numrow++; // Tambah 1 setiap kali looping
            }

            // echo json_encode($data);
            // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
            $this->plm->insert_multiple($data);
        }
        $log = [
            'status' => true,
            'msg' => "Berhasil Import",
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Import Data";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        echo json_encode($log);
        // redirect("Project_List/project_list"); // Redirect ke halaman awal (ke controller siswa fungsi index)
    }

    public function upload()
    {
        $config['upload_path'] = "./uploads/project_list";
        $config['allowed_types'] = 'xlsx|csv';
        $config['encrypt_name'] = false;

        $this->load->library('upload', $config);
        if ($this->upload->do_upload("file")) {
            $data = array('upload_data' => $this->upload->data());

            $image = $data['upload_data']['file_name'];
            return $image;
        }
    }

    public function cetak_pdf()
    {

        $data = [
            'title' => 'Project List :: Project List All',
            'smallTitle' => 'Project List',
            'project' => $this->plm,
            'status' => $this->input->get('status'),
            'startdate' => $this->input->post('startdate'),
        ];

        $this->load->view('welcome_message', $data);

        // Get output html
        $html = $this->output->get_output();

        // Load pdf library
        $this->load->library('pdfs');

        // Load HTML content
        $this->dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $this->dompdf->render();

        // Output the generated PDF (1 = download and 0 = preview)
        $this->dompdf->stream("project_list" . date('Ymd') . ".pdf", array("Attachment" => 0));

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> cetak_pdf";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);
    }

    public function charter()
    {
        $this->load->model('SubunitModel', 'subm');

        // Definisi
        $action = 'Project_List/inPL';
        $method = 'post';

        $edit = [
            'id' => '',
            'task' => '',
            'detail_task' => '',
            // 's_sts' => '',
            // 's_tcel' => '',
            // 'ket_s_sts' => '',
            // 'ket_s_tcel' => '',
            'status' => '',
            'pic_sts' => '',
            'pic_tcel' => '',
            'note' => '',
            'review' => '',
            'confirm' => '',
            'via' => '',
            'startdate' => '',
            'enddate' => '',
            'priority' => '',
            'subunit_id' => '',
            'tanggal' => '',
        ];

        $id_pc = $this->input->get('id_pc');
        if ($id_pc != '') {

            $action = 'Project_List/upPL';
            $method = 'get';

            $pc = $this->M_pc->getProjectListID_($id_pc)->row();

            $edit = [
                'id' => $pc->id,
                'task' => $pc->task,
                'detail_task' => $pc->detail_task,
                // 's_sts' => $pc->s_sts,
                // 's_tcel' => $pc->s_tcel,
                // 'ket_s_sts' => $pc->ket_s_sts,
                // 'ket_s_tcel' => $pc->ket_s_tcel,
                'status' => $pc->status,
                'pic_sts' => $pc->pic_sts,
                'pic_tcel' => $pc->pic_tcel,
                'note' => $pc->note,
                'review' => $pc->review,
                'confirm' => $pc->confirm,
                'via' => $pc->via,
                'startdate' => $pc->startdate,
                'enddate' => $pc->enddate,
                'priority' => $pc->priority,
                'subunit_id' => $pc->subunit_id,
                'tanggal' => $pc->tanggal,
            ];
        }

        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/project_list', 'Project List'), anchor('/Project_List/charter?id_pc=' . $this->input->get('id_pc'), 'Project Charter')],
            'smallTitle' => 'Project Charter',
            'title' => 'Project Charter',
            'pl' => $edit,
            'action' => $action,
            'method' => $method,
            'subunit' => $this->subm->getSubunit('', '1')->result()
        ];

        $this->render_page('page/project/charter', $data);

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman Charter";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);
    }

    public function mitra()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/project_list', 'Project List'), anchor('/Project_List/mitra?id_pc=' . $this->input->get('id_pc'), 'Mitra')],
            'mitra' => $this->mm,
            'title' => 'Input Mitra',

        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman Mitra";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        $this->render_page('page/project/mitra', $data);
    }

    public function statement()
    {
        $data = [
            'statement' => $this->sm,
            'mitra' => $this->mm,

        ];
        $this->render_page('page/project/statement', $data);
    }

    public function document()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/project_list', 'Project List'), anchor('/Project_List/document?id_pc=' . $this->input->get('id_pc'), 'Document')],
            'smallTitle' => 'Document',
            'title' => 'Document List',
            'document' => $this->dm,
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman Document";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        $this->render_page('page/project/document', $data);
    }

    public function uploadDoc()
    {
        $this->load->model('MSupport', 'support');
        $type = 'jpg|png|xlsx|pdf|pptx';

        $pp = (array) json_decode($this->support->upload('pp', './uploads/pra_project/', $type));
        $op = (array) json_decode($this->support->upload('op', './uploads/on_project/', $type));
        $cl = (array) json_decode($this->support->upload('cl', './uploads/clossing/', $type));

        if ($pp['error'] != 1) {

            $object = [
                'id_project_list' => $this->input->post('id_pc'),
                'id_document' => 1,
                'file' => $pp['success']->file_name,
                'created_date' => date('Y-m-d H:i:s'),
            ];

            $this->dm->inDocumentList($object);

            // Insert Log Actvitiy
            $msgLog = "User : " . $this->session->userdata('username') . " -> Upload Document (1)";
            $this->lm->id_user = $this->session->userdata('id');
            $this->lm->inLogActivity($msgLog, json_encode($object));

            array_push($this->result, $object);
        }

        if ($op['error'] != 1) {

            $object = [
                'id_project_list' => $this->input->post('id_pc'),
                'id_document' => 2,
                'file' => $op['success']->file_name,
                'created_date' => date('Y-m-d H:i:s'),
            ];

            $this->dm->inDocumentList($object);

            // Insert Log Actvitiy
            $msgLog = "User : " . $this->session->userdata('username') . " -> Upload Document (2)";
            $this->lm->id_user = $this->session->userdata('id');
            $this->lm->inLogActivity($msgLog, json_encode($object));

            array_push($this->result, $object);
        }

        if ($cl['error'] != 1) {

            $object = [
                'id_project_list' => $this->input->post('id_pc'),
                'id_document' => 3,
                'file' => $cl['success']->file_name,
                'created_date' => date('Y-m-d H:i:s'),
            ];

            $this->dm->inDocumentList($object);

            // Insert Log Actvitiy
            $msgLog = "User : " . $this->session->userdata('username') . " -> Upload Document (3)";
            $this->lm->id_user = $this->session->userdata('id');
            $this->lm->inLogActivity($msgLog, json_encode($object));

            array_push($this->result, $object);
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    // PROJECT CHARTER

    public function viewUpPc()
    {

        $id = $this->input->get('id');

        if ($id != '') {

            $data['segmen'] = $this->M_pc->segmen()->result();
            $data['data'] = $this->M_pc->get_by_id($id);
            $this->render_page('page/project/charter', $data);
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function viewUpPd()
    {

        $id = $this->input->get('id');

        if ($id != '') {

            $data['segmen'] = $this->M_pc->segmen()->result();
            $data['data'] = $this->M_pc->get_by_id($id);
            $this->render_page('page/project/charter', $data);
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    // Mitra

    public function editMitra()
    {

        $id = $this->input->get('id');

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman Edit Mitra";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        if ($id != '') {
            $data = [
                'mitra' => $this->mm,
            ];
            $this->render_page('page/mitra/formEdit', $data);
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    // Statement

    public function editStatement()
    {

        $id = $this->input->get('id');

        if ($id != '') {
            $data = [
                'statement' => $this->sm,
                'mitra' => $this->mm,
            ];
            $this->render_page('page/statement/formEdit', $data);
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    ########################################## NEW REVOLUTION #########################################

    # ~SCOPE

    public function scope()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/project_list', 'Project List'), anchor('/Project_List/scope?id_pc=' . $this->input->get('id_pc'), 'Scope')],
            'smallTitle' => 'Scope Of Work',
            'title' => 'Scope Of Work',
            'project' => $this->M_pc,
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman Scope";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        $this->render_page('page/project/scope', $data);
    }

    public function upScope()
    {
        $this->M_pc->upScope();

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman Edit Scope";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        redirect('Project_List/project_list');
    }

    # ~PROJECT LIST
    public function project_list()
    {

        $data = [
            'title' => 'Project List :: Project List All',
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/project_list', 'Project List')],
            'smallTitle' => 'Project List',
            'project' => $this->plm,
            'status' => '',
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman Project List";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        $this->render_page('page/project/project_list', $data);
    }

    #Update Project List
    public function inPL()
    {
        $this->plm->inPL();
    }

    #Update Project List
    public function upPL()
    {
        $this->plm->upPL();
    }

    #Delete Project List
    public function dePL()
    {
        $id = $_POST['id'];

        if ($id != '') {
            $this->plm->dePL($id);
        } else {
            redirect("Project_List/project_list");
        }
    }

    // Cetak PDF
    public function cetak()
    {
        $data = [
            'title' => 'Project List :: Project List All',
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/project_list', 'Project List')],
            'smallTitle' => 'Project List',
            'project' => $this->plm,
            'status' => '',
        ];

        $this->load->view('test', $data);
    }

    //DATA TABLE PROJECT LIST
    public function getdtprojectlist()
    {
        echo $this->plm->dtProjectList();
    }

    # ~DRAFT_LIST

    public function draft_list()
    {

        $data = [
            'title' => 'Project List :: Draft List',
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/draft_list', 'Draft List')],
            'smallTitle' => 'Draft',
            'project' => $this->plm,
            'status' => 1,
        ];

        $this->render_page('page/project/project_list', $data);
    }

    # ~ON PROGRESS

    public function on_progress_list()
    {
        $data = [
            'title' => 'Project List :: On Progress List',
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/on_progress_list', 'On Progress List')],
            'smallTitle' => 'On Progress List',
            'project' => $this->plm,
            'status' => 2,
        ];

        $this->render_page('page/project/project_list', $data);
    }

    # ~DONE

    public function done_list()
    {
        $data = [
            'title' => 'Project List :: Done List',
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/done_list', 'Done List')],
            'smallTitle' => 'Done List',
            'project' => $this->plm,
            'status' => 3,
        ];

        $this->render_page('page/project/project_list', $data);
    }

    # ~PANDING

    public function panding_list()
    {
        $data = [
            'title' => 'Project List :: Panding List',
            'bread' => [anchor('/dashboard', 'Home'), anchor('/Project_List/panding_list', 'Panding List')],
            'smallTitle' => 'Panding List',
            'project' => $this->plm,
            'status' => 4,
        ];

        $this->render_page('page/project/project_list', $data);
    }

    public function exportExcelProjectlist()
    {

        $query['data']  = $this->db->query("SELECT * FROM pl")->result();


        $this->load->view('reportexcel.php', $query);
    }
}

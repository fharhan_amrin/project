<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ManageService extends MY_Controller
{

    private $filename = "data";
    private $bread = [];

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('CDRModel', 'cdr');
        $this->load->model('ServiceModel', 's');
        $this->load->model('LogDailyActivityModel', 'lda');
        $this->load->model('LogDailyDeviceModel', 'ldd');

        //  OPEN :: LOG MODEL
        $this->load->model('LogModel', 'lm');

        if (!$this->session->userdata('id')) {
            redirect('/');
        }
    }

    public function index()
    {
        $data = [
            'title' => 'Manage Service',
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman Manage Service";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        $this->render_page('page/dashboard/index', $data);
    }

    // CDR
    public function customerDailyRequest()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/ManageService/customerDailyRequest', 'Customer Daily Request List')],
            'title' => 'Manage Service :: Costumer Daily Request',
            'smallTitle' => 'Costumer Daily Request',
            'model' => $this->cdr,
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman " . $data['title'];
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        $this->render_page('page/manage_service/customerDailyRequest', $data);
    }

    public function importBack()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Upload
        $this->filename = $this->upload();

        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('uploads/project_list/' . $this->filename); // Load file yang telah diupload ke folder excel
        $getSheet = $loadexcel->getSheetNames();

        foreach ($getSheet as $rows) {
            $sheet = $loadexcel->getSheetByName($rows)->toArray(null, true, true, true);
            // $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
            // var_dump($sheet);
            // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
            $data = [];

            $numrow = 1;
            foreach ($sheet as $row) {
                // Cek $numrow apakah lebih dari 1
                // Artinya karena baris pertama adalah nama-nama kolom
                // Jadi dilewat saja, tidak usah diimport
                if ($numrow > 1) {
                    // Kita push (add) array data ke variabel data
                    $obj = array(
                        'request_date' => $row['A'],
                        'costumer_request' =>  $row['B'],
                        'whom' => $row['C'],
                        'request_done' => $row['D'],
                        'status' => $row['E'],
                        'request_by' => $row['F'],
                        'note' => $row['G'],
                        'review' => $row['H'],
                        'created_date' => date('Y-m-d H:i:s'),

                    );
                    array_push($data, $obj);
                }

                $numrow++; // Tambah 1 setiap kali looping
            }

            // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
            try {
                $q = $this->db->insert_batch('cdr', $data);
                // $q = $this->dm->insert_multiple($data);
                if ($q) {
                    $log = array('success' => true, 'title' => 'Success', 'msg' => 'Berhasil import');
                    // redirect("http://150.242.111.2:5/googleplay/");
                } else {
                    $log = array('success' => false, 'title' => 'Failed', 'msg' => 'File Voucher already exists ');
                    // redirect("http://150.242.111.235/googleplay?err=1");
                }
            } catch (Exception $th) {
                // echo $th->getMessage();
                $log = array('success' => false, 'msg' => 'Failed claused by ' . $th->getMessage());
                // redirect('http://localhost/googleplay?err=1');
            }
        }

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman Import";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        // Redirect ke halaman awal (ke controller siswa fungsi index)
        echo json_encode($log);
    }
    public function import()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Upload
        $this->filename = $this->upload();

        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('uploads/project_list/' . $this->filename); // Load file yang telah diupload ke folder excel
        $getSheet = $loadexcel->getSheetNames();

        foreach ($getSheet as $rows) {
            $sheet = $loadexcel->getSheetByName($rows)->toArray(null, true, true, true);
            // $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
            // var_dump($sheet);
            // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
            $data = [];

            $numrow = 1;
            foreach ($sheet as $row) {
                // Cek $numrow apakah lebih dari 1
                // Artinya karena baris pertama adalah nama-nama kolom
                // Jadi dilewat saja, tidak usah diimport
                if ($numrow > 1) {
                    // Kita push (add) array data ke variabel data
                    $obj = array(
                        'request_date' => $row['A'],
                        'services' =>  $row['B'],
                        'whom' => $row['C'],
                        'request_done' => $row['D'],
                        'status' => $row['E'],
                        'request_by' => $row['F'],
                        'note' => $row['G'],
                        'review' => $row['H'],
                        'created_date' => date('Y-m-d H:i:s'),

                    );
                    array_push($data, $obj);
                }

                $numrow++; // Tambah 1 setiap kali looping
            }

            // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
            try {
                $q = $this->db->insert_batch('list_services', $data);
                // $q = $this->dm->insert_multiple($data);
                if ($q) {
                    $log = array('success' => true, 'title' => 'Success', 'msg' => 'Berhasil import');
                    // redirect("http://150.242.111.235/googleplay/");
                } else {
                    $log = array('success' => false, 'title' => 'Failed', 'msg' => 'File Voucher already exists ');
                    // redirect("http://150.242.111.235/googleplay?err=1");
                }
            } catch (Exception $th) {
                // echo $th->getMessage();
                $log = array('success' => false, 'msg' => 'Failed claused by ' . $th->getMessage());
                // redirect('http://localhost/googleplay?err=1');
            }
        }

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman Import";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        // Redirect ke halaman awal (ke controller siswa fungsi index)
        echo json_encode($log);
    }

    public function upload()
    {
        $config['upload_path'] = "./uploads/project_list";
        $config['allowed_types'] = '*';
        $config['encrypt_name'] = false;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload("file")) {
            $data = array('upload_data' => $this->upload->data());

            $image = $data['upload_data']['file_name'];
            return $image;
        }
    }
    public function cetak_pdf()
    {
        $this->cdr->startdate = $this->input->post('startdate');
        $this->cdr->enddate = $this->input->post('enddate');

        $data = [
            'title' => 'Manage Service :: Manage Service All',
            'smallTitle' => 'Manage Service',
            'project' => $this->cdr,
            // 'startdate' => $startdate,
            // 'enddate' => $enddate,
            'status' => '',
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Cetak pdf #" . $data['title'];
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        $this->load->view('viewPdfCdr', $data);

        // Get output html
        $html = $this->output->get_output();

        // Load pdf library
        $this->load->library('pdfs');

        // Load HTML content
        $this->dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $this->dompdf->render();

        // Output the generated PDF (1 = download and 0 = preview)
        $this->dompdf->stream("ManageService" . date('Ymd') . ".pdf", array("Attachment" => 0));
    }

    public function dtCdr()
    {
        echo $this->cdr->dtCdr();
    }

    public function inCDR()
    {
        $log = [];

        $in = $this->cdr->inCDR();
        if ($in) {
            $log = [
                'msg' => 'Berhasil Menambahkan Request',
            ];
        } else {
            $log = [
                'msg' => 'Gagal Menambahkan Request',
            ];
        }

        echo json_encode($log);
    }

    public function getCDRID()
    {
        echo json_encode($this->cdr->getCDRID()->row());
    }
    public function getservive()
    {
        echo json_encode($this->s->getservice()->row());
    }

    public function upCDR()
    {
        $id = $this->input->post('id');

        $obj = [
            'request_date' => $this->input->post('request_date'),
            'costumer_request' => $this->input->post('costumer_request'),
            'whom' => $this->input->post('whom'),
            'status' => $this->input->post('status'),
            'request_done' => $this->input->post('request_done'),
            'request_by' => $this->input->post('request_by'),
            'review' => $this->input->post('review'),
            'note' => $this->input->post('note'),
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Update CDR";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog, json_encode($obj));

        echo json_encode($this->cdr->upCDR($obj, $id));
    }

    // SERVICES
    public function services()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/ManageService/services', 'Services')],
            'title' => 'Manage Service :: Service',
            'smallTitle' => 'Service',
            'model' => $this->s,
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman " . $data['title'];
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        $this->render_page('page/manage_service/services', $data);
    }
    public function cetak_pdf_list_service()
    {

        $data = [
            'title' => 'Service :: Service List All',
            'smallTitle' => 'Service',
            'project' => $this->s,
            'status' => '',
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Cetak PDF " . $data['title'];
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        $this->load->view('cetak_pdf_list_service', $data);

        // Get output html
        $html = $this->output->get_output();

        // Load pdf library
        $this->load->library('pdfs');

        // Load HTML content
        $this->dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $this->dompdf->render();

        // Output the generated PDF (1 = download and 0 = preview)
        $this->dompdf->stream("ManageService" . date('Ymd') . ".pdf", array("Attachment" => 0));
    }

    public function dtservice()
    {
        echo $this->s->dtservice();
    }

    public function upservices()
    {
        $id = $this->input->post('id');

        $obj = [
            'request_date' => $this->input->post('request_date'),
            'services' => $this->input->post('services'),
            'whom' => $this->input->post('whom'),
            'status' => $this->input->post('status'),
            'request_done' => $this->input->post('request_done'),
            'request_by' => $this->input->post('request_by'),
            'review' => $this->input->post('review'),
            'note' => $this->input->post('note'),
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Update Services";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog, json_encode($obj));

        echo json_encode($this->s->upServices($obj, $id));
    }

    public function getServiceID()
    {
        echo json_encode($this->s->getServicesID()->row());
    }

    public function inServices()
    {
        $log = [];

        $in = $this->s->inServices();
        if ($in) {
            $log = [
                'msg' => 'Berhasil Menambahkan Services',
            ];
        } else {
            $log = [
                'msg' => 'Gagal Menambahkan Services',
            ];
        }

        echo json_encode($log);
    }

    // SERVICES
    public function logDailyActivity()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/ManageService/logDailyActivity', 'Log Daily Activity')],
            'title' => 'Manage Service :: Log Daily Activity',
            'smallTitle' => 'Log Daily Activity',
            'model' => $this->lda,
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman " . $data['title'];
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        $this->render_page('page/manage_service/logDailyActivity', $data);
    }

    public function dtDA()
    {
        echo $this->lda->dtDA();
    }

    public function getLDAID()
    {
        // $id = $this->input->get('id');
        $in = $this->input->get('in');
        $tanggal = $this->input->get('tanggal');

        // $da = $this->lda->getDA($id);
        // $da = $da->row();
        // $status = $da->status;

        // $lda = $this->lda->getLDA('','',['id_da' => $id, 'created_date' => $tanggal]);

        // if ($lda->num_rows() > 0) {
        //     $lda = $lda->row();
        //     $status = $lda->status;
        // }

        $logs = [];

        $query = "SELECT * FROM daily_activity da WHERE da.id in ($in)";
        $q = $this->lda->getDA('', $query)->result();
        foreach ($q as $da) {
            $log = [
                'id' => $da->id,
                'description' => $da->description,
                'pic' => $da->pic,
                'status' => $this->getStatusLDA($da->id, $da->status, $tanggal),
                'note' => $da->note,
                'review' => $da->review,
            ];

            array_push($logs, $log);
        }

        echo json_encode($logs);
    }

    public function getStatusLDA($id = '', $s = '', $tanggal = '')
    {
        $lda = $this->lda->getLDA('', '', ['id_da' => $id, 'created_date' => $tanggal]);
        if ($lda->num_rows() > 0) {
            $lda = $lda->row();
            $status = $lda->status;
        } else {
            $status = $s;
        }

        return $status;
    }

    public function upLDAOne()
    {
        $log = [];
        $idPost = $this->input->post('id');
        $tanggal = $this->input->post('tanggal');

        $status = $this->input->post('field') == 'status' ? $this->input->post('val') : '';

        $obj = [
            $this->input->post('field') => $this->input->post('val'),
        ];

        $id = ['id' => $idPost];

        $in = $this->lda->upLDAOne($obj, $id);
        if ($in) {
            $log = [
                'msg' => 'Berhasil Ubah Device',
            ];

            $OLDA = [
                'id_da' => $idPost,
                'status' => $status,
                'created_date' => $tanggal,
            ];

            $deLog = $this->lda->deLDA('', ['id_da' => $idPost, 'created_date' => $tanggal]);
            if ($deLog) {
                $inLog = $this->lda->inLDA($OLDA);
            }
        } else {
            $log = [
                'msg' => 'Gagal Ubah Device',
            ];
        }

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Update LDA One";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog, json_encode($OLDA));

        echo json_encode($log);
    }
    public function upLDDOne()
    {
        $log = [];
        $idPost = $this->input->post('id');
        $tanggal = $this->input->post('tanggal');

        $status = $this->input->post('field') == 'status' ? $this->input->post('val') : '';

        $obj = [
            $this->input->post('field') => $this->input->post('val'),
        ];

        $id = ['id' => $idPost];

        $in = $this->ldd->upLDDOne($obj, $id);
        if ($in) {
            $log = [
                'msg' => 'Berhasil Ubah Device',
            ];

            $OLDA = [
                'id' => $idPost,
                'status' => $status,
                'created_date' => $tanggal,
            ];

            $deLog = $this->ldd->deLDD('', ['id' => $idPost, 'created_date' => $tanggal]);
            if ($deLog) {
                $inLog = $this->ldd->inLDD($OLDA);
            }
        } else {
            $log = [
                'msg' => 'Gagal Ubah Device',
            ];
        }

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Update LDD One";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog, json_encode($OLDA));

        echo json_encode($log);
    }

    // DMDD
    public function dailyDevice()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/ManageService/dailyDevice', 'Log Daily Device')],
            'title' => 'Manage Service :: Log Daily Device',
            'smallTitle' => 'Log Daily Device',
            'model' => $this->ldd,
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman " . $data['title'];
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        $this->render_page('page/manage_service/logDailyDevice', $data);
    }

    public function dtDD()
    {
        echo $this->ldd->dtDD();
    }
    // crud
    public function getData()
    {
        $in = $this->input->get('in');
        $tanggal = $this->input->get('tanggal');

        $logs = [];

        $query = "SELECT * FROM daily_device da WHERE da.id in ($in)";
        // $q = $this->ldd->getDA('', $query)->result();
        $q = $this->ldd->getDD('', $query)->result();
        foreach ($q as $dd) {
            $log = [
                'id' => $dd->id,
                'date' => $dd->date,
                'device' => $dd->device,
                'status' => $this->getStatusLDD($dd->id, $dd->status, $tanggal),
                'note' => $dd->note,
                'created_date' => $dd->created_date,
            ];

            array_push($logs, $log);
        }

        echo json_encode($logs);
    }

    public function getStatusLDD($id = '', $s = '', $tanggal = '')
    {
        $ldd = $this->ldd->getDD('', '', ['id' => $id, 'created_date' => $tanggal]);
        if ($ldd->num_rows() > 0) {
            $ldd = $ldd->row();
            $status = $ldd->status;
        } else {
            $status = $s;
        }

        return $status;
    }

    public function udpate()
    {
        $id = $this->input->post('id');
        $date = date('Y-m-d H:i:s');
        $device = $this->input->post('device');
        $status = $this->input->post('status');
        $note = $this->input->post('note');
        $created_date = date('Y-m-d H:i:s');
        $data = $this->MJ->updatemeja($id, $date, $device, $status, $note, $created_date);
        echo json_encode($data);
    }
}

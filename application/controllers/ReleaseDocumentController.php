<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ReleaseDocumentController extends MY_Controller
{

    public function index()
    {
        $data = [
            'title' => 'Release Document'
        ];
        $this->render_page('page/releasedocument/releasedocumentviews.php', $data);
    }





    public function do_upload()
    {
        $config['upload_path']          = './uploads/release/';
        $config['allowed_types']        = '*';
        $config['max_size']             = 0;
        $config['max_width']            = 0;
        $config['max_height']           = 0;

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if (!$this->upload->do_upload('files')) {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);

            // $this->load->view('welcome_message', $error);
        } else {
            $data = array('upload_data' => $this->upload->data());

            // var_dump($data);

            $nilai = $data['upload_data'];

            echo $nilai['file_name'];

            // $this->load->view('upload_success', $data);
        }
    }
    public function insertrelease()
    {
        //    Upload
        $this->filename = $this->do_upload();


        echo  $this->filename;
    }

    // public function uploadImage()
    // {

    //     $data = [];

    //     $count = count($_FILES['files']['name']);

    //     echo $count;

    //     for ($i = 0; $i < $count; $i++) {

    //         if (!empty($_FILES['files']['name'][$i])) {

    //             $_FILES['file']['name'] = $_FILES['files']['name'][$i];
    //             $_FILES['file']['type'] = $_FILES['files']['type'][$i];
    //             $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
    //             $_FILES['file']['error'] = $_FILES['files']['error'][$i];
    //             $_FILES['file']['size'] = $_FILES['files']['size'][$i];

    //             $config['upload_path'] = "./uploads/";
    //             $config['allowed_types'] = 'jpg|jpeg|png|gif|xlsx|csv';
    //             // $config['max_size'] = '5000';
    //             $config['file_name'] = $_FILES['files']['name'][$i];

    //             $this->load->library('upload', $config);

    //             if (!$this->upload->do_upload('file')) {
    //                 $uploadData = $this->upload->data();
    //                 $filename = $uploadData['file_name'];

    //                 $data['totalFiles'][] = $filename;
    //                 echo var_dump($data);
    //             } else {
    //                 echo "gak ada file";
    //             }
    //         }

    //         // $this->render_page('page/releasedocument/releasedocumentviews.php', $data);
    //     }
    // }
}
 
 /* End of file Controllername.php */

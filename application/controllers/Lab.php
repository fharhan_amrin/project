<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lab extends CI_Controller
{

    private $id_tujuan = "";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('LogDailyActivityModel', 'lda');
        $this->load->model('LogDailyDeviceModel', 'ldd');
        $this->load->model('TelegramModel', 'tm');
        $this->cekIdTelegram();
    }
    
    public function index()
    {
        // $this->load->view('welcome_message');
        var_dump(realpath('./uploads/'));
    }

    public function do_upload()
    {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = '*';
            $config['max_size']             = 100;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;

            $this->load->library('upload', $config);

	$this->upload->initialize($config);

            if ( ! $this->upload->do_upload('userfile'))
            {
                    $error = array('error' => $this->upload->display_errors());
                
                    $this->load->view('welcome_message', $error);
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());

                    $this->load->view('upload_success', $data);
            }
    }

    public function cekIdTelegram()
    {
        $this->load->model('SettingModel', 'sm');

        $s = $this->sm->getSetting('','telegram')->row();
        $t = (array) json_decode($s->data);

        foreach ($t['id_telegram'] as $v) {
            if ($v->aktif == "1") {
                $this->id_tujuan =  $v->id_tujuan;
            }
        }
    }
    
    public function inLDA()
    {
        $device = "1,Daily,Monitoring Server Performance,STS,No Issue,--,--|
        2,Daily,Checking CPU Usage,STS,No Issue,--,--|
        3,Daily,Checking MEM Usage,STS,No Issue,--,--|
        4,Daily,Checking Disk Usage,STS,No Issue,--,--|
        5,Daily,Checking Network Usage,STS,No Issue,--,--|
        6,Daily,Monitoring Aplication
        Performance,STS,No Issue,--,--|
        7,Daily,Logs Checking,STS,No Issue,--,--|
        8,Daily,CDR Checking,STS,No Issue,--,--|
        9,Daily,Checking java Usage,STS,No Issue,--,--|
        10,Daily,Checking Tomcat Usage,STS,No Issue,--,--|
        11,Daily,Healthcheck,STS,No Issue,--,--|
        12,Daily,Checking Log Rotate,STS,No Issue,--,--|
        13,Daily,Monitoring DB Performance,STS,No Issue,--,--|
        14,Daily,Checking DB Usage,STS,No Issue,--,--|
        15,Daily,Monitoring Transaction Log (Bonita),STS,No Issue,--,--";
        
        $d = explode('|', $device);
        for ($i = 0; $i < count($d); $i++) {
            $x = explode(',', $d[$i]);
            $id = $x[0];
            $date = $x[1];
            $desk = $x[2];
            $pic = $x[3];
            
            $obj = [
                'date' => date('Y-m-d'),
                'description' => $desk,
                'pic' => $pic,
                'status' => 1,
                'created_date' => date('Y-m-d H:i:s'),
            ];
            
            $this->lda->inDA($obj);
            
        }
    }
    
}

/* End of file Lab.php */
/* Location: ./application/controllers/Lab.php */
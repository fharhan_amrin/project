<?php


defined('BASEPATH') or exit('No direct script access allowed');

class CRMController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('CRMModel', 'crm');
    }
    public function CRM()
    {
        $data = [
            'bread' => [anchor('/dashboard', 'Home'), anchor('/CRMController/CRM/', 'CRM')],
            'title' => 'Manage Service :: CRM',
            'smallTitle' => 'CRM',
            'model' => $this->crm,
        ];

        // Insert Log Actvitiy
        // $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman " . $data['title'];
        // $this->lm->id_user = $this->session->userdata('id');
        // $this->lm->inLogActivity($msgLog);

        $this->render_page('page/manage_service/CRM_View', $data);
    }

    public function dtCRM()
    {
        echo $this->crm->dtCRM();
    }

    public function upCRM()
    {
        $id = $this->input->post('id');

        $obj = [
            'request_date' => $this->input->post('request_date'),
            'costumer_request' => $this->input->post('costumer_request'),
            'whom' => $this->input->post('whom'),
            'status' => $this->input->post('status'),
            'request_done' => $this->input->post('request_done'),
            'request_by' => $this->input->post('request_by'),
            'review' => $this->input->post('review'),
            'note' => $this->input->post('note'),
        ];

        // Insert Log Actvitiy
        // $msgLog = "User : " . $this->session->userdata('username') . " -> Update CRM";
        // $this->lm->id_user = $this->session->userdata('id');
        // $this->lm->inLogActivity($msgLog, json_encode($obj));

        echo json_encode($this->crm->upCRM($obj, $id));
    }

    public function getCRM()
    {
        echo json_encode($this->crm->getCRM()->row());
    }

    //proses insert data CRM
    public function inCRM()
    {
        $log = [];

        $in = $this->crm->inCRM();
        if ($in) {
            $log = [
                'msg' => 'Berhasil Menambahkan Request',
            ];
            //  OPEN :: Notifikasi Telegram
            // $TL = &get_instance();
            $this->load->model('TestingModel', 'tmm');

            // //  Get dari tabel User
            // $U = &get_instance();
            // $U->load->model('UsersModel', 'um');

            $this->tmm->nama_user = $this->session->userdata('username');
            $this->tmm->oke = $this->input->post('costumer_request');
            $this->tmm->review_crm = $this->input->post('review');




            $msg = $this->tmm->msgToTelegram('i');
            // $request_params = [
            //     'chat_id' => "-1001420679038",
            //     'text' => $msg,
            //     'parse_mode' => 'HTML'
            // ];

            // sts & tcel
            $request_params_ststcel = [
                'chat_id' => "-1001188812526",
                'text' => $msg,
                'parse_mode' => 'HTML'
            ];

            // callcenter

            $request_params_callcenter = [
                'chat_id' => "-1001420679038",
                'text' => $msg,
                'parse_mode' => 'HTML'
            ];

            $this->tmm->kirimPesan('', '', $request_params_ststcel);
            $this->tmm->kirimPesan('', '', $request_params_callcenter);
            // CLOSE :: Notifikasi Telegram


        } else {
            $log = [
                'msg' => 'Gagal Menambahkan Request',
            ];
        }

        echo json_encode($log);
    }

    public function cetak_pdf()
    {
        $this->crm->startdate = $this->input->post('startdate');
        $this->crm->enddate = $this->input->post('enddate');
        $startdate = $this->input->post('startdate');
        $enddate =   $this->input->post('enddate');
        // echo "dfkbjv,dfjk v";
        $data = [
            'title' => 'CRM :: CRM All',
            'smallTitle' => 'CRM',
            'project' => $this->crm,
            'startdate' => $startdate,
            'enddate' => $enddate,
            'status' => '',
        ];

        $this->load->view('viewpdfCRM', $data);

        // Get output html
        $html = $this->output->get_output();

        // Load pdf library
        $this->load->library('pdfs');

        // Load HTML content
        $this->dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $this->dompdf->render();

        // Output the generated PDF (1 = download and 0 = preview)
        $this->dompdf->stream("ManageService" . date('Ymd') . ".pdf", array("Attachment" => 0));
    }

    // import excel
    public function importBack()
    {
        // Load plugin PHPExcel nya
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

        // Upload
        $this->filename = $this->upload();

        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('uploads/project_list/' . $this->filename); // Load file yang telah diupload ke folder excel
        $getSheet = $loadexcel->getSheetNames();

        foreach ($getSheet as $rows) {
            $sheet = $loadexcel->getSheetByName($rows)->toArray(null, true, true, true);
            // $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);
            // var_dump($sheet);
            // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
            $data = [];

            $numrow = 1;
            foreach ($sheet as $row) {
                // Cek $numrow apakah lebih dari 1
                // Artinya karena baris pertama adalah nama-nama kolom
                // Jadi dilewat saja, tidak usah diimport
                if ($numrow > 1) {
                    // Kita push (add) array data ke variabel data
                    $obj = array(
                        'request_date' => $row['A'],
                        'costumer_request' =>  $row['B'],
                        'whom' => $row['C'],
                        'request_done' => $row['D'],
                        'status' => $row['E'],
                        'request_by' => $row['F'],
                        'note' => $row['G'],
                        'review' => $row['H'],
                        'created_date' => date('Y-m-d H:i:s'),

                    );
                    array_push($data, $obj);
                }

                $numrow++; // Tambah 1 setiap kali looping
            }

            // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
            try {
                $q = $this->db->insert_batch('crm', $data);
                // $q = $this->dm->insert_multiple($data);
                if ($q) {
                    $log = array('success' => true, 'title' => 'Success', 'msg' => 'Berhasil import');
                    // redirect("http://150.242.111.235/googleplay/");
                } else {
                    $log = array('success' => false, 'title' => 'Failed', 'msg' => 'File Voucher already exists ');
                    // redirect("http://150.242.111.235/googleplay?err=1");
                }
            } catch (Exception $th) {
                // echo $th->getMessage();
                $log = array('success' => false, 'msg' => 'Failed claused by ' . $th->getMessage());
                // redirect('http://localhost/googleplay?err=1');
            }
        }

        // Insert Log Actvitiy
        // $msgLog = "User : " . $this->session->userdata('username') . " -> Import Mytelkomcel";
        // $this->lm->id_user = $this->session->userdata('id');
        // $this->lm->inLogActivity($msgLog);

        // Redirect ke halaman awal (ke controller siswa fungsi index)
        echo json_encode($log);
    }

    public function upload()
    {
        $config['upload_path'] = "./uploads/project_list";
        $config['allowed_types'] = 'xlsx|csv';
        $config['encrypt_name'] = false;

        $this->load->library('upload', $config);
        if ($this->upload->do_upload("file")) {
            $data = array('upload_data' => $this->upload->data());

            $image = $data['upload_data']['file_name'];
            return $image;
        }
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    private $bread = [];

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('ProjectListModel', 'pl');

        //  OPEN :: LOG MODEL
        $this->load->model('LogModel', 'lm');

        if (!$this->session->userdata('id')) {
            redirect('/');
        }
    }

    public function index()
    {
        $data = [
            'title' => 'Dashboard'
        ];

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Halaman " . $data['title'];
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog);

        $this->render_page('page/dashboard/index', $data);
    }

    public function dataDashboard()
    {
        $tahun = $this->input->get('tahun');

        echo json_encode($this->pl->dashboard($tahun));
    }

    // use table cdr;
    public function vas()
    {
        $tahun = $this->input->get('tahun');

        echo json_encode($this->pl->vas($tahun));
    }

    // use table mytelkomcel;
    public function mytelkomcel()
    {
        $tahun = $this->input->get('tahun');

        echo json_encode($this->pl->mytelkomcel($tahun));
    }

    //  use table crm;
    public function crm()
    {
        $tahun = $this->input->get('tahun');

        echo json_encode($this->pl->crm($tahun));
    }
}

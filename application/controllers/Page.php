<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('M_pc');
        if (!$this->session->userdata('id')) {
            redirect('/');
        }
    }
    public function index()
    {
        $data['report'] = $this->M_pc->get_data_project();
        // echo json_encode($data['report']);
        $this->render_page('page/dashboard/index', $data);
    }

    public function charter()
    {
        $this->render_page('page/project/charter');
    }

    public function project_list()
    {
        $data['project'] = $this->M_pc;
        $this->render_page('page/project/project_list', $data);
    }

    public function mitra()
    {
        $this->render_page('page/project/mitra');
    }

    public function statement()
    {
        $this->render_page('page/project/statement');
    }

    public function document()
    {
        $this->render_page('page/project/document');
    }

    public function closed()
    {
        $this->render_page('page/project/closed');
    }

    public function inputMs()
    {
        $this->render_page('page/manage/input_ms');
    }

    public function onGoingMs()
    {
        $this->render_page('page/manage/onGoing_ms');
    }

    public function closedMs()
    {
        $this->render_page('page/manage/closed_ms');
    }

    // ACTION

    public function formUpdate()
    {

        $id = $this->input->get('id');

        if ($id != '') {

            $data['segmen'] = $this->M_pc->segmen()->result();
            $data['data'] = $this->M_pc->get_by_id($id);
            $this->render_page('page/project/formUpdate', $data);

        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

    public function formDelete()
    {
        $id = $this->input->get('id');
        if ($id != '') {
            $id = explode(',', $id);
            $this->M_pc->formDelete($id);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }
}
<?php
defined('BASEPATH') or exit('No direct script access allowed');


$active_group = 'default';
$query_builder = true;

$db['default'] = array(
    'dsn' => '',
    'hostname' => 'localhost',
    'username' => 'root',
    'password' => 'r00t@dm1n05',
    'database' => 'telkomcel',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => false,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => false,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => false,
    'compress' => false,
    'stricton' => false,
    'failover' => array(),
    'save_queries' => true,
);

// $db['default'] = array(
//     'dsn' => '',
//     'hostname' => 'localhost',
//     'username' => 'root',
//     'password' => '',
//     'database' => 'telkomcel',
//     'dbdriver' => 'mysqli',
//     'dbprefix' => '',
//     'pconnect' => false,
//     'db_debug' => (ENVIRONMENT !== 'production'),
//     'cache_on' => false,
//     'cachedir' => '',
//     'char_set' => 'utf8',
//     'dbcollat' => 'utf8_general_ci',
//     'swap_pre' => '',
//     'encrypt' => false,
//     'compress' => false,
//     'stricton' => false,
//     'failover' => array(),
//     'save_queries' => true,
// );

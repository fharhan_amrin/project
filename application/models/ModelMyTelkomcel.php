<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelMyTelkomcel extends CI_Model
{

    // public $dd = 'my_telkomcel';
    public $tablemy = 'my_telkomcel';
    public $log_tcel = 'log_tcel';
    public $kolom = "";
    public  $startdate = '';
    public   $enddate = '';

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('id')) {
            redirect('/');
        }
    }


    public function se($status = '')
    {
        // if ($status == '') {
        //     $status = $this->input->get('status');
        // }

        // if ($status != '') {
        //     $this->db->where('status', $status);
        // }
        $startdate = $this->startdate;
        $enddate = $this->enddate;

        if ($startdate != "" and $enddate != "") {
            $this->db->where('DATE(request_date) >=', date('Y-m-d', strtotime($startdate)));
            $this->db->where('DATE(request_date) <=', date('Y-m-d', strtotime($enddate)));
        }

        $this->db->order_by('id', 'ASC');
        $query = $this->db->get($this->tablemy);

        return $query;
    }

    public function getMT($id = '')
    {
        if ($id == '') {
            $id = $this->input->get('id');
        }

        $q = $this->db->get_where($this->tablemy, ['id' => $id]);
        return $q;
    }


    public function upMyTelkomcel($obj = '', $id = '', $based_on = '')
    {
        $log = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $q = $this->db->update($this->tablemy, $obj, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'msg' => 'Sukses ubah My Telkomcel ',
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }
    public function getDD($id = '', $q = '', $obj = '')
    {

        if ($id != '') {
            $obj = ['id' => $id];
        }

        if ($obj != '') {
            $q = $this->db->get_where($this->tablemy, $obj);
        } else if ($q != '') {
            $q = $this->db->query($q);
        } else {
            $q = $this->db->get_where($this->tablemy);
        }

        return $q;
    }

    public function inDD($obj = '')
    {
        $log = '';

        if ($obj != '') {
            $q = $this->db->insert($this->tablemy, $obj);
        }

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function upDD($obj = '', $id = '')
    {
        $log = '';
        $based_on = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $q = $this->db->update($this->tablemy, $obj, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'msg' => 'Sukses ubah Profile',
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function deDD($id = '', $q = "", $obj = "")
    {
        $log = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $this->db->delete($this->tablemy, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function upLDDOne($obj = '', $id = '')
    {
        $q = $this->db->update($this->tablemy, $obj, $id);
        if ($q) {
            return true;
        } else {
            return false;
        }
    }

    # ~logDailyActiviy

    public function getLDD($id = '', $q = '', $obj = '')
    {

        if ($id != '') {
            $obj = ['id' => $id];
        }

        if ($obj != '') {
            $q = $this->db->get_where($this->ldd, $obj);
        } else if ($q != '') {
            $q = $this->db->query($q);
        } else {
            $q = $this->db->get_where($this->ldd);
        }

        return $q;
    }

    public function inLDD($obj = '')
    {
        $log = '';

        if ($obj != '') {
            $q = $this->db->insert($this->log_tcel, $obj);
        }

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function upLDD($obj = '', $id = '')
    {
        $log = '';
        $based_on = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $q = $this->db->update($this->ldd, $obj, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'msg' => 'Sukses ubah Profile',
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function deLDD($id = '', $based_on = '')
    {
        $log = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $de = $this->db->delete($this->log_tcel, $based_on);

        if ($de) {
            return true;
        } else {
            return false;
        }
    }

    // OPTIONAL

    public function cekStatus($status = '')
    {
        if ($status == '') {
            $status = $this->input->get('status');
        }

        switch ($status) {
            case 1:
                $q = 'DRAFT';
                break;
            case 2:
                $q = 'ON PROGRESS';
                break;
            case 3:
                $q = 'DONE';
                break;
            case 4:
                $q = 'PANDING';
                break;
            default:
                $q = 'TIDAK DIKETEHUI';
                break;
        }

        return $q;
    }

    // public function dtMT()
    // {
    //     // Definisi
    //     $condition = '';
    //     $data = [];

    //     $CI = &get_instance();
    //     $CI->load->model('DataTable', 'dt');

    //     // Set table name
    //     $CI->dt->table = $this->tablemy;
    //     // Set orderable column fields
    //     $CI->dt->column_order = array(null, 'device', 'status', 'note', 'created_date');
    //     // Set searchable column fields
    //     $CI->dt->column_search = array('device', 'status', 'note', 'created_date');
    //     // Set select column fields
    //     $CI->dt->select = $this->tablemy . '.*';
    //     // Set default order
    //     $CI->dt->order = array($this->tablemy . '.id' => 'desc');

    //     // Fetch member's records
    //     $dataTabel = $this->dt->getRows($_POST, $condition);

    //     $i = $_POST['start'];

    //     // DEFINISI
    //     $request_date = "'request_date'";
    //     $costumer_request = "'costumer_request'";
    //     $whom = "'whom'";
    //     $request_done = "'request_done'";
    //     $status = "'status'";
    //     $request_by = "'request_by'";
    //     $note = "'note'";
    //     $review = "'review'";
    //     $created_date = "'created_date'";

    //     foreach ($dataTabel as $dt) {
    //         $i++;
    //         // $dt->status == 0 ? "checked" : ""
    //         $data[] = array(
    //             '<div id="txt_des' . $dt->id . '" class="txt">' . $dt->request_date . '</div> <div><input onchange="ubahINP(' . $request_date . ',' . $dt->id . ',this.value)" type="text" id="inp_request_date' . $dt->id . '"class="inp form-control" name="request_date" value="' . $dt->request_date . '"></div>',
    //             '<div id="txt_des' . $dt->id . '" class="txt">' . $dt->costumer_request . '</div> <div><input onchange="ubahINP(' . $costumer_request . ',' . $dt->id . ',this.value)" type="text" id="inp_costumer_request' . $dt->id . '"class="inp form-control" name="costumer_request" value="' . $dt->costumer_request . '"></div>',
    //             '<div id="txt_des' . $dt->id . '" class="txt">' . $dt->whom . '</div> <div><input onchange="ubahINP(' . $whom . ',' . $dt->id . ',this.value)" type="text" id="inp_whom' . $dt->id . '"class="inp form-control" name="whom" value="' . $dt->whom . '"></div>',
    //             '<div id="txt_des' . $dt->id . '" class="txt">' . $dt->request_done . '</div> <div><input onchange="ubahINP(' . $request_done . ',' . $dt->id . ',this.value)" type="text" id="inp_request_done' . $dt->id . '"class="inp form-control" name="request_done" value="' . $dt->request_done . '"></div>',
    //             '<span id="s' . $dt->id . '"> <div id="txt_pic' . $dt->id . '" class="txt">' . $this->cekStatus($dt->status) . '</div> <div class="inp" > <input type="radio" onchange="ubahINP(' . $status . ',' . $dt->id . ',this.value)" name="status' . $dt->id . '" ' . ($dt->status == 1 ? "checked" : "") . ' id="inp_status' . $dt->id . '" value="1"> Normal <input type="radio" onchange="ubahINP(' . $status . ',' . $dt->id . ',this.value)" name="status' . $dt->id . '" id="inp_status' . $dt->id . '" ' . ($dt->status == 2 ? "checked" : "") . '  value="2"> Checking <input type="radio" onchange="ubahINP(' . $status . ',' . $dt->id . ',this.value)" name="status' . $dt->id . '" id="inp_status' . $dt->id . '" ' . ($dt->status == 3 ? "checked" : "") . '  value="3"> Error </div>',
    //             '<div id="txt_pic' . $dt->id . '" class="txt">' . $dt->request_by . ' </div> <input type="text" onchange="ubahINP(' . $request_by . ',' . $dt->id . ',this.value)" id="inp_request_by' . $dt->id . '" class="inp form-control"name="request_by" value="' . $dt->request_by . '">',
    //             '<div id="txt_pic' . $dt->id . '" class="txt">' . $dt->note . ' </div> <input type="text" onchange="ubahINP(' . $note . ',' . $dt->id . ',this.value)" id="inp_note' . $dt->id . '" class="inp form-control"name="note" value="' . $dt->note . '">',
    //             '<div id="txt_pic' . $dt->id . '" class="txt">' . $dt->review . ' </div> <input type="text" onchange="ubahINP(' . $review . ',' . $dt->id . ',this.value)" id="inp_review' . $dt->id . '" class="inp form-control"name="review" value="' . $dt->review . '">',
    //             // ' <div id="txt_pic' . $dt->id . '" class="txt">' . $dt->created_date . ' </div> <input type="text" onchange="ubahINP(' . $created_date . ',' . $dt->id . ',this.value)"  id="inp_created_date' . $dt->id . '" class="inp form-control"name="created" value="' . $dt->created_date . '">',
    //         );
    //     }

    //     $output = array(
    //         "draw" => $_POST['draw'],
    //         "recordsTotal" => $this->dt->countAll($condition),
    //         "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
    //         "data" => $data,
    //     );

    //     // Output to JSON format
    //     return json_encode($output);
    // }

    public function dtMT()
    {
        // Definisi
        $kondisi = '';
        $data = [];
        $startdate = '';
        $enddate = '';


        if ($this->input->get('startdate') != "" && $this->input->get('enddate') != "") {
            $kondisi =  [
                // ['where', $this->tabel . '.aktif', '1'],
                // ['where', $this->tablemy . '.status', $this->input->get('status')],
                ['where', $this->tablemy . '.request_date >=', $this->input->get('startdate')],
                ['where', $this->tablemy . '.request_date <=', $this->input->get('enddate')]
            ];
        }

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tablemy;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'request_date', 'costumer_request', 'whom', 'status', 'request_done', 'request_by', 'note', 'review');
        // Set searchable column fields
        $CI->dt->column_search = array('request_date', 'costumer_request', 'whom', 'status', 'request_done', 'request_by', 'note', 'review');
        // Set select column fields
        $CI->dt->select = $this->tablemy . '.*';
        // Set default order
        $CI->dt->order = array($this->tablemy . '.id' => 'desc');

        $condition = $kondisi;

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(

                '<a href="#ok" onclick="edit(' . $dt->id . ')" >' . $dt->request_date . '</a>',
                $dt->costumer_request,
                $dt->whom,
                $this->cekStatus($dt->status) . '<br>' . $dt->request_done,
                $dt->request_by,
                $dt->note,
                $dt->review,
            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    public function inMyTelkomcel($object = '')
    {

        if ($object == '') {

            $object = [
                'request_date' => $this->input->post('request_date'),
                'costumer_request' => $this->input->post('costumer_request'),
                'whom' => $this->input->post('whom'),
                'status' => $this->input->post('status'),
                'request_done' => $this->input->post('request_done'),
                'request_by' => $this->input->post('request_by'),
                'review' => $this->input->post('review'),
                'note' => $this->input->post('note'),
                'created_date' => date('Y-m-d H:i:s'),
            ];
        }

        $q = $this->db->insert($this->tablemy, $object);

        // Insert Log Actvitiy
        $msgLog = "User : " . $this->session->userdata('username') . " -> Insert MyTelkomcel";
        $this->lm->id_user = $this->session->userdata('id');
        $this->lm->inLogActivity($msgLog, json_encode($object));

        if ($q) {
            return true;
        } else {
            return false;
        }
    }


    public function getMyTelkomcel($date = '')
    {
        $startDate = $this->input->get('startDate');
        $endDate = $this->input->get('endDate');

        // DATE KOSONG MASUK KE GET DATE
        if ($date == '') {
            $date = $this->input->get('date');
        }

        // KALAU DI GET DATE KOSONG MASUK KE DEFAULT DATE(Y-M-D)
        if ($date == '') {
            $arr = ['date(created_date)' => date('Y-m-d')];
        } else {
            $arr = ['date(created_date)' => $date];
        }

        // KALAU ADA INPUTAN STARTDATE DAN ENDDATE
        if ($startDate != '' && $endDate != '') {
            $q = $this->db->query("SELECT * FROM " . $this->tablemy . " WHERE (created_date BETWEEN '" . $startDate . "' AND '" . $endDate . "')");
        } else {
            $this->db->order_by('id', 'desc');
            $q = $this->db->get_where($this->tablemy, $arr);
        }

        return $q;
    }
}

/* End of file LogDailyDeviceModel.php */
/* Location: ./application/models/LogDailyDeviceModel.php */

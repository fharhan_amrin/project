<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ModelLogin extends CI_Model
{

    public function islogin($data)
    {
        $query = $this->db->get_where('user', array('username' => $data['username'], 'password' => $data['password']));
        return $query->row_array();

    }

}

/* End of file ModelLogin.php */

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StatementModel extends CI_Model {

	private $t = 'statement';
	private $t_list = 'statement_list';

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('id')) {
            redirect('/');
        }
	}
	
	// ~Statement

	public function getStatement()
	{
		$this->db->select('s.*,m.mitra');
		$this->db->join('mitra m', 'm.id = s.id_mitra', 'inner');
		$q = $this->db->get($this->t.' s');
		return $q;
	}

	public function getStatementID($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}

		$q = $this->db->get_where($this->t,['id' => $id]);
		return $q;
	}

	public function cekStatus($id='',$idpl='')
	{
		if ($id != '' && $idpl != '') {
			$get = $this->db->get_where('statement_list', ['id_statement' => $id, 'id_project_list' => $idpl]);
			if ($get->num_rows() > 0) {
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function getStatementAll($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}

		$id = explode(',', $id);
		$this->db->where_in('id', $id);
		$q = $this->db->get($this->t);
		return $q;
	}

	public function inStatement($object='')
	{

		if ($object == '') {

			$object = [
				'activity' => $this->input->post('activity'),
				'id_mitra' => $this->input->post('mitra'),
				'start_target' => $this->input->post('start_target'),
				'end_target' => $this->input->post('end_target'),
				'created_date' => date('Y-m-d H:i:s')
			];

		}


		$q = $this->db->insert($this->t, $object);
		
		if ($q) {
			return true;
		}else{
			return false;
		}
	}

	public function upStatement($id='',$object='')
	{
		// Deifinis		
		if ($id == '') {
			$id = $this->input->post('id');
		}

		if ($object == '') {
			$object = array();
		}

		for ($i=0; $i < count($id) ; $i++) { 

			$activity 	= $this->input->post('activity')[$i];
			$mitra 	= $this->input->post('mitra')[$i];
			$start_target 	= $this->input->post('start_target')[$i];
			$end_target 	= $this->input->post('end_target')[$i];

			$arr = array(
				'id' => $id[$i],
				'activity' => $activity,
				'id_mitra' => $mitra,
				'start_target' => $start_target,
				'end_target' => $end_target,

			);

			array_push($object,$arr);

		}

		$q = $this->db->update_batch($this->t, $object,'id');
		
		if ($q) {
			return true;
		}else{
			return false;
		}
	}

	public function deStatement($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}
		
		if ($id != '') {
			$id = explode(',', $id);
			$this->db->where_in('id', $id);
			$q = $this->db->delete($this->t);

			if ($q) {
				return true;
			}else{
				return false;
			}

		}else{
			return false;
		}
	}

	// ~Statement LIST

	public function getStatementList($idPc='')
	{
		if ($idPc == '') {
			$idPc = $this->input->get('idPc');
		}

		$q = $this->db->get_where($this->t_list,['id_project_list' => $idPc]);
		return $q;
	}

	public function getStatementListID($id='',$arr='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}

		if ($arr == '') {
			$arr = ['id' => $id];
		}

		$q = $this->db->get_where($this->t_list,$arr);
		return $q;
	}

	public function inStatementList($object='')
	{
		if ($object == '') {

			$object = [
				'id_statement' => $this->input->post('id_statement'),
				'id_project_list' => $this->input->post('id_project_list'),
				'created_date' => date('Y-m-d H:i:s')
			];

		}

		$q = $this->db->insert($this->t_list, $object);
		
		if ($q) {
			return true;
		}else{
			return false;
		}
	}

	public function upStatementList($id='',$object='')
	{
		// Deifinis		
		if ($id == '') {
			$id = $this->input->get('id');
		}

		if ($obj == '') {
			$object = array();
		}

		for ($i=0; $i < count($id) ; $i++) { 

			$id_statement = $this->input->post('id_statement')[$i];
			$id_project_list = $this->input->post('id_project_list')[$i];

			$arr = array(
				'id' => $id[$i],
				'id_statement' => $id_statement,
				'id_project_list' => $id_project_list,
			);

			array_push($object,$arr);

		}

		$q = $this->db->update_batch($this->t_list, $object,'id');
		
		if ($q) {
			return true;
		}else{
			return false;
		}
	}

	public function deStatementList($id='')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}
		
		if ($id != '') {
			
			$id = explode(',', $id);
			$this->db->where_in('id', $id);
			$q = $this->db->delete($this->t_list);

			if ($q) {
				return true;
			}else{
				return false;
			}

		}else{
			return false;
		}
	}

}

/* End of file M_pc.php */
/* Location: ./application/models/M_pc.php */

/* End of file pc.php */
/* Location: ./application/models/pc.php */
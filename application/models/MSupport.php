<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MSupport extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function upload($name = '', $pathFolder = './uploads/',$type_file='jpg|png')
    {
        // Deklarasi
        $hasil = array();
        $error = 0;
        $data = '';

        $config = array();
        $config['upload_path']          = $pathFolder;
        $config['allowed_types']        = $type_file;
        $config['encrypt_name'] = false;

        $this->load->library('upload', $config, $name);
        $this->$name->initialize($config);

        if (!$this->$name->do_upload($name)) {
                $error = 1;
            } else {
                $data = $this->$name->data();

                $ty = explode('|', $type_file);
                if ($ty[0] == 'jpg' || $ty[0] == 'png') {
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $data['full_path'];
                    $config['create_thumb'] = true;
                    $config['maintain_ratio'] = true;
                    $config['width']         = 100;
                    $config['height']       = 100;

                    $this->load->library('image_lib', $config);

                    $this->image_lib->resize();
                }

            }

        //hasil

        $hasil = array(
            'success' =>  $data,
            'error' => $error
        );

        return json_encode($hasil);
    }
}

/* End of file MSupport.php */



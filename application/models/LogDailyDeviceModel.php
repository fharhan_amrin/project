<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LogDailyDeviceModel extends CI_Model
{

    public $dd = 'daily_device';
    public $ldd = 'ldd';

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('id')) {
            redirect('/');
        }
    }

    public function getDD($id = '', $q = '', $obj = '')
    {

        if ($id != '') {
            $obj = ['id' => $id];
        }

        if ($obj != '') {
            $q = $this->db->get_where($this->dd, $obj);
        } else if ($q != '') {
            $q = $this->db->query($q);
        } else {
            $q = $this->db->get($this->dd);
        }

        return $q;
    }

    public function inDD($obj = '')
    {
        $log = '';

        if ($obj != '') {
            $q = $this->db->insert($this->dd, $obj);
        }

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function upDD($obj = '', $id = '')
    {
        $log = '';
        $based_on = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $q = $this->db->update($this->dd, $obj, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'msg' => 'Sukses ubah Profile',
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function deDD($id = '',$q="",$obj="")
    {
        $log = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $this->db->delete($this->dd, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function upLDDOne($obj = '', $id = '')
    {
        $q = $this->db->update($this->dd, $obj, $id);
        if ($q) {
            return true;
        } else {
            return false;
        }
    }

    # ~logDailyActiviy

    public function getLDD($id = '', $q = '', $obj = '')
    {

        if ($id != '') {
            $obj = ['id' => $id];
        }

        if ($obj != '') {
            $q = $this->db->get_where($this->ldd, $obj);
        } else if ($q != '') {
            $q = $this->db->query($q);
        } else {
            $q = $this->db->get_where($this->ldd);
        }

        return $q;
    }

    public function inLDD($obj = '')
    {
        $log = '';

        if ($obj != '') {
            $q = $this->db->insert($this->ldd, $obj);
        }

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function upLDD($obj = '', $id = '')
    {
        $log = '';
        $based_on = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $q = $this->db->update($this->ldd, $obj, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'msg' => 'Sukses ubah Profile',
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function deLDD($id = '', $based_on = '')
    {
        $log = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $de = $this->db->delete($this->ldd, $based_on);

        if ($de) {
            return true;
        } else {
            return false;
        }
    }

    // OPTIONAL

    public function cekStatus($v = '')
    {
        switch ($v) {
            case '1':
                $val = 'Normal';
                break;
            case '2':
                $val = 'Checking';
                break;
            case '3':
                $val = 'Error';
                break;
            default:
                $val = 'Tidak Diketahui';
                break;
        }

        return $val;
    }

    public function dtDD()
    {
        // Definisi
        $condition = '';
        $data = [];

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->dd;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'device', 'status', 'note', 'created_date');
        // Set searchable column fields
        $CI->dt->column_search = array('device', 'status', 'note', 'created_date');
        // Set select column fields
        $CI->dt->select = $this->dd . '.*';
        // Set default order
        $CI->dt->order = array($this->dd . '.id' => 'desc');

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];

        // DEFINISI
        $device = "'device'";
        $status = "'status'";
        $note = "'note'";
        $created_date = "'created_date'";

        foreach ($dataTabel as $dt) {
            $i++;
            // $dt->status == 0 ? "checked" : ""
            $data[] = array(
                '<div id="txt_des' . $dt->id . '" class="txt">' . $dt->device . '</div> <div><input onchange="ubahINP(' . $device . ',' . $dt->id . ',this.value)" type="text" id="inp_device' . $dt->id . '"class="inp form-control" name="device" value="' . $dt->device . '"></div>',
                '<span id="s' . $dt->id . '"> <div id="txt_pic' . $dt->id . '" class="txt">' . $this->cekStatus($dt->status) . '</div> <div class="inp" > <input type="radio" onchange="ubahINP(' . $status . ',' . $dt->id . ',this.value)" name="status' . $dt->id . '" ' . ($dt->status == 1 ? "checked" : "") . ' id="inp_status' . $dt->id . '" value="1"> Normal <input type="radio" onchange="ubahINP(' . $status . ',' . $dt->id . ',this.value)" name="status' . $dt->id . '" id="inp_status' . $dt->id . '" ' . ($dt->status == 2 ? "checked" : "") . '  value="2"> Checking <input type="radio" onchange="ubahINP(' . $status . ',' . $dt->id . ',this.value)" name="status' . $dt->id . '" id="inp_status' . $dt->id . '" ' . ($dt->status == 3 ? "checked" : "") . '  value="3"> Error </div>',
                '<div id="txt_pic' . $dt->id . '" class="txt">' . $dt->note . ' </div> <input type="text" onchange="ubahINP(' . $note . ',' . $dt->id . ',this.value)" id="inp_note' . $dt->id . '" class="inp form-control"name="note" value="' . $dt->note . '">',
                // ' <div id="txt_pic' . $dt->id . '" class="txt">' . $dt->created_date . ' </div> <input type="text" onchange="ubahINP(' . $created_date . ',' . $dt->id . ',this.value)"  id="inp_created_date' . $dt->id . '" class="inp form-control"name="created" value="' . $dt->created_date . '">',
            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }
}

/* End of file LogDailyDeviceModel.php */
/* Location: ./application/models/LogDailyDeviceModel.php */
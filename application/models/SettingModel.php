<?php 
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class SettingModel extends CI_Model {
        
        private $t = 'setting';

        public function getSetting($id = '', $code='', $q = '', $obj = '')
        {
    
            if ($id != '') {
                $obj = ['id' => $id];
            }
           
            if ($code != '') {
                $obj = ['code' => $code];
            }
    
            if ($obj != '') {
                $q = $this->db->get_where($this->t, $obj);
            } else if ($q != '') {
                $q = $this->db->query($q);
            } else {
                $q = $this->db->get($this->t);
            }
    
            return $q;
        }
    
        public function inSetting($obj = '')
        {
            $log = '';
    
            if ($obj != '') {
                $q = $this->db->insert($this->t, $obj);
            }
    
            $log = [
                'response' => $q,
                'request' => $obj,
                'date' => date('Y-m-d H:i:s'),
            ];
    
            return $log;
        }
    
        public function upSetting($obj = '', $id = '')
        {
            $log = '';
            $based_on = '';
    
            if ($id != '') {
                $based_on = ['id' => $id];
            }
    
            $q = $this->db->update($this->t, $obj, $based_on);
    
            $log = [
                'response' => $q,
                'request' => $obj,
                'msg' => 'Sukses ubah Profile',
                'date' => date('Y-m-d H:i:s'),
            ];
    
            return $log;
        }
    
        public function deSetting($id = '',$q="",$obj="")
        {
            $log = '';
    
            if ($id != '') {
                $based_on = ['id' => $id];
            }
    
            $this->db->delete($this->t, $based_on);
    
            $log = [
                'response' => $q,
                'request' => $obj,
                'date' => date('Y-m-d H:i:s'),
            ];
    
            return $log;
        }
    
    }
    
    /* End of file SettingModel.php */
    
    
?>
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pc extends CI_Model
{


	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('id')) {
			redirect('/');
		}
	}


	private $table = 'pl_release_ducument p';
	private $nama = 'nama_project';

	public function se($status = '4')
	{
		if ($status == '') {
			$status = $this->input->get('status');
		}

		$this->db->select('p.*,s.segmen');
		$this->db->from($this->table);
		$this->db->join('segmen s', 's.id = p.id_segmen', 'join');
		$this->db->order_by($this->nama, 'asc');
		if ($status != '') {
			$this->db->where('p.status', $status);
		}
		$query = $this->db->get();
		return $query;
	}

	public function in()
	{
		$date = date_default_timezone_set("Asia/Jakarta");
		$date = date("Y-m-d H-m-s");

		$name_project = $this->input->post('name_project');
		$nodin = $this->input->post('nodin');
		$tgl_nodin = $this->input->post('tgl_nodin');
		$pic_telkomcel = $this->input->post('pic_telkomcel');
		$pic_sts = $this->input->post('pic_sts');
		$segmen = $this->input->post('segmen');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$tgl_testing = $this->input->post('tgl_testing');
		$tgl_rilis = $this->input->post('tgl_rilis');
		$status = $this->input->post('status');
		$note = $this->input->post('note');
		$confirm = $this->input->post('confirm');
		$via = $this->input->post('via');
		$tgl_update = $date;

		if ($name_project && $nodin && $tgl_nodin && $pic_sts && $pic_telkomcel && $segmen && $start_date && $end_date && $tgl_testing && $tgl_rilis && $status) {
			$data = array(
				'nama_project' => $name_project,
				'no' => $nodin,
				'tanggal_nodin' => $tgl_nodin,
				'pic_telkomcel' => $pic_telkomcel,
				'pic_sts' => $pic_sts,
				'id_segmen' => $segmen,
				'start_date' => $start_date,
				'end_date' => $end_date,
				'note' => $note,
				'confirm' => $confirm,
				'via' => $via,
				'tanggal_testing' => $tgl_testing,
				'tanggal_rilis' => $tgl_rilis,
				'status' => $status
			);

			$in = $this->db->insert('pl', $data);
			$idPc = $this->db->insert_id();

			if ($in) {
				redirect('project_list/mitra?id_pc=' . $idPc);
			} else {
				redirect('project_list/charter');
			}
		} else {
			echo "<script>alert('Anda harus mengisi data dengan lengkap !');window.history.back();</script>";
		}
	}

	public function up($data)
	{
		$this->db->update_batch('pl', $data, 'id');
	}

	public function de($id)
	{
		$id = implode(",", $id);

		$p = $this->db->query("SELECT * FROM pl_release_ducument WHERE id IN ($id)")->result();
		foreach ($p as $v) {
			$task = $v->task;

			//  OPEN :: Notifikasi Telegram
			$TL = &get_instance();
			$TL->load->model('TelegramModel', 'tm');

			//  Get dari tabel User
			$U = &get_instance();
			$U->load->model('UsersModel', 'um');

			$TL->tm->id_pl = $v->id;
			$TL->tm->task = $v->task;
			$TL->tm->nama_user = $U->um->getUser($this->session->userdata('id'), '', '', 'username')->row()->username;

			$msg = $TL->tm->msgToTelegram('d');
			$request_params = [
				'chat_id' => "-1001390339665",
				'text' => $msg,
				'parse_mode' => 'HTML'
			];

			$TL->tm->kirimPesan('', '', $request_params);
			// CLOSE :: Notifikasi Telegram

			$this->db->update('pl', ['aktif' => '0'], ['id' => $v->id]);
		}

		// $this->db->where('id', $id);
		// $this->db->delete('pl');
		return true;
	}


	public function cekStatus($val = '')
	{
		// Definisi 
		$cek = '';

		switch ($val) {
			case 1:
				$cek = "DRAFT";
				break;
			case 2:
				$cek = "ON PROGRESS";
				break;
			case 3:
				$cek = "DONE";
				break;
			case 4:
				$cek = "PANDING";
				break;
			default:
				$cek = "TIDAK DIKETAHUI";
				break;
		}

		return $cek;
	}

	public function get_by_id($id)
	{
		return $this->db->query('SELECT * FROM project_list WHERE id IN (' . $id . ')')->result();
	}

	public function segmen()
	{
		return $this->db->get('segmen');
	}

	public function get_data_project()
	{
		$query = $this->db->query("SELECT status,COUNT(*) AS statusSum FROM project_list GROUP BY status");
		if ($query->num_rows() > 0) {
			$i = 0;
			foreach ($query->result() as $data) {
				$hasil[$i] = array($data->status, $data->statusSum);
				// $hasil[$i] = ;
				$i++;
			}
			return $hasil;
		}
	}

	public function getProjectListID($id = '')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}

		$q = $this->db->get_where('pl_release_ducument', ['id' => $id]);
		return $q;
	}
	public function getProjectListID_($id = '')
	{
		if ($id == '') {
			$id = $this->input->get('id');
		}

		$q = $this->db->get_where('pl', ['id' => $id]);
		return $q;
	}

	# ~SCOPE

	public function upScope($object = '', $id = '')
	{
		if ($id == '') {
			$id = $this->input->get('id_pc');
		}

		if ($object == '') {
			$object = [
				'scope' => $this->input->get('scope')
			];
		}

		$CI = &get_instance();
		$CI->load->model('ProjectListModel', 'plm');

		$this->db->update('pl', $object, ['id' => $id]);

		$pl = $CI->plm->getPL($id)->row();
		$dataLog = array(
			'id_pl' => $id,
			'task' => $pl->task,
			'status' => $pl->status,
			'pic_sts' => $pl->pic_sts,
			'pic_tcel' => $pl->pic_tcel,
			'note' => $pl->note,
			'review' => $pl->review,
			'confirm' => $pl->confirm,
			'via' => $pl->via,
			'tanggal' => $pl->tanggal,
			'scope' => $this->input->get('scope'),
			'tanggal_update' => date('Y-m-d H:i:s')
		);
		$CI->plm->inLogPL($dataLog);

		return true;
	}
}

/* End of file M_pc.php */
/* Location: ./application/models/M_pc.php */

/* End of file pc.php */
/* Location: ./application/models/pc.php */
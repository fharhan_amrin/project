<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class SubunitModel extends CI_Model {
    
    private $t = 'subunit';
    
    public function getSubunit($id = '', $unit='', $q = '', $obj = '')
    {
        
        if ($id != '') {
            $obj = ['id' => $id];
        }
        
        if ($unit != '') {
            $obj = ['unit_id' => $unit];
        }
        
        if ($obj != '') {
            $q = $this->db->get_where($this->t, $obj);
        } else if ($q != '') {
            $q = $this->db->query($q);
        } else {
            $q = $this->db->get($this->t);
        }
        
        return $q;
    }
    
    public function inSubunit($obj = '')
    {
        $log = '';
        
        if ($obj != '') {
            $q = $this->db->insert($this->t, $obj);
        }
        
        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];
        
        return $log;
    }
    
    public function upSubunit($obj = '', $id = '')
    {
        $log = '';
        $based_on = '';
        
        if ($id != '') {
            $based_on = ['id' => $id];
        }
        
        $q = $this->db->update($this->t, $obj, $based_on);
        
        $log = [
            'response' => $q,
            'request' => $obj,
            'msg' => 'Sukses ubah Profile',
            'date' => date('Y-m-d H:i:s'),
        ];
        
        return $log;
    }
    
    public function deSubunit($id = '',$q="",$obj="")
    {
        $log = '';
        
        if ($id != '') {
            $based_on = ['id' => $id];
        }
        
        $this->db->delete($this->t, $based_on);
        
        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];
        
        return $log;
    }
    
}

/* End of file SubunitModel.php */




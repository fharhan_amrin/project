<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LogDailyActivityModel extends CI_Model
{

    public $da = 'daily_activity';
    public $lda = 'lda';

    public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('id')) {
            redirect('/');
        }
	}

    public function dtDA()
    {
        // Definisi
        $condition = '';
        $data = [];

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->da;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'description', 'pic', 'status', 'note', 'review');
        // Set searchable column fields
        $CI->dt->column_search = array('description', 'pic', 'status', 'note', 'review');
        // Set select column fields
        $CI->dt->select = $this->da . '.*';
        // Set default order
        $CI->dt->order = array($this->da . '.id' => 'desc');

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];

        // DEFINISI
        $description = "'description'";
        $pic = "'pic'";
        $status = "'status'";
        $note = "'note'";
        $review = "'review'";

        foreach ($dataTabel as $dt) {
            $i++;
            // $dt->status == 0 ? "checked" : ""
            $data[] = array(
                '<div id="txt_des' . $dt->id . '" class="txt">' . $dt->description . '</div> <div><input onchange="ubahINP(' . $description . ',' . $dt->id . ',this.value)" type="text" id="inp_description' . $dt->id . '"class="inp form-control" name="description" value="' . $dt->description . '"></div>',
                ' <div id="txt_pic' . $dt->id . '" class="txt">' . $dt->pic . ' </div> <input type="text" onchange="ubahINP(' . $pic . ',' . $dt->id . ',this.value)" id="inp_pic' . $dt->id . '" class="inp form-control"name="pic" value="' . $dt->pic . '">',
                '<span id="s' . $dt->id . '"> <div id="txt_pic' . $dt->id . '" class="txt">' . $this->cekStatus($dt->status) . '</div> <div class="inp" > <input type="radio" onchange="ubahINP(' . $status . ',' . $dt->id . ',this.value)" name="status' . $dt->id . '" ' . ($dt->status == 0 ? "checked" : "") . ' id="inp_status' . $dt->id . '" value="0"> No Issue <input type="radio" onchange="ubahINP(' . $status . ',' . $dt->id . ',this.value)" name="status' . $dt->id . '" id="inp_status' . $dt->id . '" ' . ($dt->status == 1 ? "checked" : "") . '  value="1"> Issue </div>',
                '<div id="txt_pic' . $dt->id . '" class="txt">' . $dt->note . ' </div> <input type="text" onchange="ubahINP(' . $note . ',' . $dt->id . ',this.value)" id="inp_note' . $dt->id . '" class="inp form-control"name="note" value="' . $dt->note . '">',
                ' <div id="txt_pic' . $dt->id . '" class="txt">' . $dt->review . ' </div> <input type="text" onchange="ubahINP(' . $review . ',' . $dt->id . ',this.value)"  id="inp_review' . $dt->id . '" class="inp form-control"name="review" value="' . $dt->review . '">',
            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }
    public function getDA($id = '', $q = '')
    {
        $obj = 0;

        if ($id != '') {
            $obj = ['id' => $id];
        }

        if ($obj != 0) {
            $q = $this->db->get_where($this->da, $obj);
        } else if ($q != '') {
            $q = $this->db->query($q);
        } else {
            $q = $this->db->get_where($this->da);
        }

        return $q;
    }

    public function upLDAOne($obj = '', $id = '')
    {
        $q = $this->db->update($this->da, $obj, $id);
        if ($q) {
            return true;
        } else {
            return false;
        }
    }

    public function inDA($obj = '')
    {
        $log = '';

        if ($obj != '') {
            $q = $this->db->insert($this->da, $obj);
        }

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function upDA($obj = '', $id = '')
    {
        $log = '';
        $based_on = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $q = $this->db->update($this->da, $obj, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'msg' => 'Sukses ubah Profile',
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function deDA($id = '')
    {
        $log = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $this->db->delete($this->da, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    # ~logDailyActiviy

    public function getLDA($id = '', $q = '', $obj = '')
    {

        if ($id != '') {
            $obj = ['id' => $id];
        }

        if ($obj != '') {
            $q = $this->db->get_where($this->lda, $obj);
        } else if ($q != '') {
            $q = $this->db->query($q);
        } else {
            $q = $this->db->get_where($this->lda);
        }

        return $q;
    }

    public function inLDA($obj = '')
    {
        $log = '';

        if ($obj != '') {
            $q = $this->db->insert($this->lda, $obj);
        }

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function upLDA($obj = '', $id = '')
    {
        $log = '';
        $based_on = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $q = $this->db->update($this->lda, $obj, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'msg' => 'Sukses ubah Profile',
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function deLDA($id = '', $based_on)
    {
        $log = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $this->db->delete($this->lda, $based_on);

        $log = [
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    // OPTIONAL

    public function cekStatus($v = '')
    {
        switch ($v) {
            case '0':
                $val = 'No Issue';
                break;
            case '1':
                $val = 'Issue';
                break;
            default:
                $val = 'Tidak Diketahui';
                break;
        }

        return $val;

    }

}

/* End of file LogDailyActivityModel.php */
/* Location: ./application/models/LogDailyActivityModel.php */
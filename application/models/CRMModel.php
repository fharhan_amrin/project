<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CRMModel extends CI_Model
{
    // public $dd = 'crm';
    public $tablemy = 'crm';
    // public $log_tcel = 'log_tcel';
    public $kolom = "";
    public  $startdate = '';
    public   $enddate = '';


    function dtCRM()
    {
        // Definisi
        $kondisi = '';
        $data = [];
        $startdate = '';
        $enddate = '';



        if ($this->input->get('startdate') != "" && $this->input->get('enddate') != "") {
            $kondisi =  [
                // ['where', $this->tabel . '.aktif', '1'],
                // ['where', $this->tablemy . '.status', $this->input->get('status')],
                ['where', $this->tablemy . '.request_date >=', $this->input->get('startdate')],
                ['where', $this->tablemy . '.request_date <=', $this->input->get('enddate')]
            ];
        }

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tablemy;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'request_date', 'costumer_request', 'whom', 'status', 'request_done', 'request_by', 'note', 'review');
        // Set searchable column fields
        $CI->dt->column_search = array('request_date', 'costumer_request', 'whom', 'status', 'request_done', 'request_by', 'note', 'review');
        // Set select column fields
        $CI->dt->select = $this->tablemy . '.*';
        // Set default order
        $CI->dt->order = array($this->tablemy . '.id' => 'desc');

        $condition = $kondisi;

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(

                '<a href="#ok" onclick="edit(' . $dt->id . ')" >' . $dt->request_date . '</a>',
                $dt->costumer_request,
                $dt->whom,
                $this->cekStatus($dt->status) . '<br>' . $dt->request_done,
                $dt->request_by,
                $dt->note,
                $dt->review,
            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    // OPTIONAL

    public function cekStatus($status = '')
    {
        if ($status == '') {
            $status = $this->input->get('status');
        }

        switch ($status) {
            case 0:
                $q = 'DRAFT';
                break;
            case 1:
                $q = 'ON PROGRESS';
                break;
            case 3:
                $q = 'DONE';
                break;
            case 4:
                $q = 'PANDING';
                break;
            default:
                $q = 'TIDAK DIKETEHUI';
                break;
        }

        return $q;
    }

    public function inCRM($object = '')
    {

        if ($object == '') {

            $object = [
                'request_date' => $this->input->post('request_date'),
                'costumer_request' => $this->input->post('costumer_request'),
                'whom' => $this->input->post('whom'),
                'status' => $this->input->post('status'),
                'request_done' => $this->input->post('request_done'),
                'request_by' => $this->input->post('request_by'),
                'review' => $this->input->post('review'),
                'note' => $this->input->post('note'),
                'created_date' => date('Y-m-d H:i:s'),
            ];
        }

        $q = $this->db->insert($this->tablemy, $object);

        // Insert Log Actvitiy
        // $msgLog = "User : " . $this->session->userdata('username') . " -> Insert crm";
        // $this->lm->id_user = $this->session->userdata('id');
        // $this->lm->inLogActivity($msgLog, json_encode($object));

        if ($q) {
            return true;
        } else {
            return false;
        }
    }

    public function se($status = '')
    {
        // if ($status == '') {
        //     $status = $this->input->get('status');
        // }

        // if ($status != '') {
        //     $this->db->where('status', $status);
        // }
        $startdate = $this->startdate;
        $enddate = $this->enddate;

        if ($startdate != "" and $enddate != "") {
            $this->db->where('DATE(request_date) >=', date('Y-m-d', strtotime($startdate)));
            $this->db->where('DATE(request_date) <=', date('Y-m-d', strtotime($enddate)));
        }

        $this->db->order_by('id', 'ASC');
        $query = $this->db->get($this->tablemy);

        return $query;
    }


    public function getCRM($id = '')
    {
        if ($id == '') {
            $id = $this->input->get('id');
        }

        $q = $this->db->get_where($this->tablemy, ['id' => $id]);
        return $q;
    }

    public function upCRM($obj = '', $id = '', $based_on = '')
    {
        $log = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $q = $this->db->update($this->tablemy, $obj, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'msg' => 'Sukses ubah CRM ',
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }
}

/* End of file CRMModel.php */

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class UsersModel extends CI_Model {

    private $t = "user";

    public function getUser($id = '', $q = '', $obj = '', $select="*")
    {

        if ($id != '') {
            $obj = ['id' => $id];
        }

        $this->db->select($select);
        if ($obj != '') {
            $q = $this->db->get_where($this->t, $obj);
        } else if ($q != '') {
            $q = $this->db->query($q);
        } else {
            $q = $this->db->get($this->t);
        }

        return $q;
    }

}

/* End of file UsersModel.php */

<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ProjectListModelRelease extends CI_Model
{

    public $tabel = 'pl_release_ducument';
    public $tLog = 'logPL';
    // public $id_tujuan = "";

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('id')) {
            redirect('/');
        }
        // $this->cekIdTelegram();
    }

    public function se($status = '')
    {
        if ($status == '') {
            $status = $this->input->get('status');
        }

        if ($status != '') {
            $this->db->where('status', $status);
        }

        $this->db->order_by('id', 'ASC');
        $query = $this->db->get($this->tabel);
        return $query;
    }

    public function getPL($id = '', $status = '', $q = '', $obj = "")
    {
        if ($id != '') {
            $obj = ['id' => $id];
        }

        if ($status != '') {
            $obj = ['status' => $status];
        }

        if ($obj != "") {
            $q = $this->db->get_where($this->tabel, $obj);
        } else if ($q != '') {
            $q = $this->db->query($q);
        } else {
            $q = $this->db->get($this->tabel);
        }

        return $q;
    }

    public function inPL()
    {
        date_default_timezone_set("Asia/Jakarta");
        $tanggal = date("Y-m-d H-i-s");

        // fungsi uploud
        $config['upload_path']          = './uploads/release/';
        $config['allowed_types']        = '*';
        $config['max_size']             = 0;
        $config['max_width']            = 0;
        $config['max_height']           = 0;

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if (!$this->upload->do_upload('files')) {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);

            // $this->load->view('welcome_message', $error);
        } else {
            $data = array('upload_data' => $this->upload->data());

            // var_dump($data);

            $nilai = $data['upload_data'];

            $filename = $nilai['file_name'];

            // $this->load->view('upload_success', $data);
        }



        $task = $this->input->post('task');
        $detail_task = $this->input->post('detail_task');
        $s_sts = $this->input->post('s_sts');
        $s_tcel = $this->input->post('s_tcel');
        $ket_s_sts = $this->input->post('ket_s_sts');
        $ket_s_tcel = $this->input->post('ket_s_tcel');
        $status = $this->input->post('status');
        $pic_sts = $this->input->post('pic_sts');
        $pic_tcel = $this->input->post('pic_tcel');
        $note = $this->input->post('note');
        $review = $this->input->post('review');
        $confirm = $this->input->post('confirm');
        $via = $this->input->post('via');
        $startdate = $this->input->post('startdate');
        // $startend = $this->input->post('startend');
        $enddate = $this->input->post('enddate');
        $priority = $this->input->post('priority');
        $subunit_id = $this->input->post('subunit_id');
        $scope = $this->input->post('scope');
        $file =  $filename;

        // if ($startdate == '') {
        //     $startdate = '';
        // }else if($enddate){
        //     $enddate = '';
        // }

        $data = array(
            'task' => $task,
            'detail_task' => $detail_task,
            's_sts' => @$s_sts,
            's_tcel' => @$s_tcel,
            'ket_s_sts' => @$ket_s_sts,
            'ket_s_tcel' => @$ket_s_tcel,
            'status' => $status,
            'pic_sts' => $pic_sts,
            'pic_tcel' => $pic_tcel,
            'note' => $note,
            'review' => $review,
            'confirm' => $confirm,
            'via' => $via,
            'startdate' => @$startdate,
            'enddate' => @$enddate,
            'priority' => $priority,
            'subunit_id' => $subunit_id,
            'tanggal' => $tanggal,
            'file' => $file,
            'scope' => $scope
        );

        $in = $this->db->insert($this->tabel, $data);
        $idPc = $this->db->insert_id();

        $dataLog = array(
            'id_pl' => $idPc,
            'task' => $task,
            'detail_task' => $detail_task,
            'status' => $status,
            'pic_sts' => $pic_sts,
            'pic_tcel' => $pic_tcel,
            'note' => $note,
            'review' => $review,
            'confirm' => $confirm,
            'via' => $via,
            'tanggal' => $tanggal,
            'tanggal_update' => date('Y-m-d H:i:s')
        );

        $this->inLogPL($dataLog);

        //  OPEN :: Notifikasi Telegram
        $TL = &get_instance();
        $TL->load->model('TelegramModel', 'tm');

        //  Get dari tabel User
        $U = &get_instance();
        $U->load->model('UsersModel', 'um');

        $TL->tm->id_pl = $idPc;
        $TL->tm->task = $task;
        $TL->tm->detail_task = $detail_task;
        $TL->tm->priority = $this->cekPriorty($priority);
        $TL->tm->pic_sts = $pic_sts;
        $TL->tm->pic_tcel = $pic_tcel;
        $TL->tm->status = $this->cekStatus($status);
        $TL->tm->note = $note;
        $TL->tm->review = $review;

        $TL->tm->nama_user = $U->um->getUser($this->session->userdata('id'), '', '', 'username')->row()->username;

        // $msg = $TL->tm->msgToTelegram('i');
        // $request_params = [
        //     'chat_id' => $this->id_tujuan,
        //     'text' => $msg,
        //     'parse_mode' => 'HTML'
        // ];

        // $TL->tm->kirimPesan('', '', $request_params);
        // CLOSE :: Notifikasi Telegram


        // Insert Log Actvitiy
        $LM = &get_instance();
        $LM->load->model('LogModel', 'lm');

        $msgLog = "User : " . $this->session->userdata('username') . " -> Insert Project List";
        $LM->lm->id_user = $this->session->userdata('id');
        $LM->lm->inLogActivity($msgLog, json_encode($data));

        // if ($in) {
        //     redirect('Project_List/scope?id_pc=' . $idPc);
        // } else {
        //     redirect('Project_List/charter');
        // }

        redirect('Project_List_Release_Document/project_list');
    }

    public function dePL($id = [])
    {
        $id = implode(",", $id);

        $query  = $this->db->query("SELECT * FROM pl_release_ducument where id='$id'")->row();
        unlink('./uploads/release/' . $query->file);

        $p = $this->db->query("SELECT * FROM $this->tabel WHERE id IN ($id)")->result();
        foreach ($p as $v) {
            $task = $v->task;

            //  OPEN :: Notifikasi Telegram
            $TL = &get_instance();
            $TL->load->model('TelegramModel', 'tm');

            //  Get dari tabel User
            $U = &get_instance();
            $U->load->model('UsersModel', 'um');

            $TL->tm->id_pl = $v->id;
            $TL->tm->task = $v->task;
            $TL->tm->nama_user = $U->um->getUser($this->session->userdata('id'), '', '', 'username')->row()->username;

            $msg = $TL->tm->msgToTelegram('d');
            $request_params = [
                'chat_id' => $this->id_tujuan,
                'text' => $msg,
                'parse_mode' => 'HTML'
            ];

            $TL->tm->kirimPesan('', '', $request_params);
            // CLOSE :: Notifikasi Telegram

            $id = $this->db->update('pl_release_ducument', ['aktif' => '0'], ['id' => $v->id]);
            if ($id) {
                // Insert Log Actvitiy
                $LM = &get_instance();
                $LM->load->model('LogModel', 'lm');

                $msgLog = "User : " . $this->session->userdata('username') . " -> Delete Project List";
                $LM->lm->id_user = $this->session->userdata('id');

                $this->db->trans_start();
                $LM->lm->inLogActivity($msgLog, "id => " . $v->id);
                $this->db->trans_complete();
            }
        }

        redirect('Project_List_Release_Document/project_list');
    }

    public function upPL()
    {
        $id = $this->input->post('id');
        $task = $this->input->post('task');
        $detail_task = $this->input->post('detail_task');
        $status = $this->input->post('status');
        $pic_sts = $this->input->post('pic_sts');
        $pic_tcel = $this->input->post('pic_tcel');
        $note = $this->input->post('note');
        $review = $this->input->post('review');
        $confirm = $this->input->post('confirm');
        $via = $this->input->post('via');
        $startdate = $this->input->post('startdate');
        $enddate = $this->input->post('enddate');
        $priority = $this->input->post('priority');
        $subunit_id = $this->input->post('subunit_id');
        $scope = $this->input->post('scope');
        $filelama = $this->input->post('filelama');

        // fungsi uploud
        $config['upload_path']          = './uploads/release/';
        $config['allowed_types']        = '*';
        $config['max_size']             = 0;
        $config['max_width']            = 0;
        $config['max_height']           = 0;

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if (!$this->upload->do_upload('files')) {
            $error = array('error' => $this->upload->display_errors());
            $data = [
                'detail_task' => $detail_task,
                'task' => $task,
                'status' => $status,
                'pic_sts' => $pic_sts,
                'pic_tcel' => $pic_tcel,
                'note' => $note,
                'review' => $review,
                'confirm' => $confirm,
                'via' => $via,
                'startdate' => $startdate,
                'enddate' => $enddate,
                'priority' => $priority,
                'subunit_id' => $subunit_id,
                'scope' => $scope
            ];

            $query = $this->db->update('pl_release_ducument', $data, ['id' => $id]);
            var_dump($data);
            var_dump($error);
        } else {
            $data = array('upload_data' => $this->upload->data());

            var_dump($data);

            unlink('./uploads/release/' . $filelama);
            $nilai = $data['upload_data'];

            $filename = $nilai['file_name'];

            $data = [
                'detail_task' => $detail_task,
                'task' => $task,
                // 's_tcel' => $s_tcel,
                // 'ket_s_sts' => $ket_s_sts,
                'file' => $filename,
                'status' => $status,
                'pic_sts' => $pic_sts,
                'pic_tcel' => $pic_tcel,
                'note' => $note,
                'review' => $review,
                'confirm' => $confirm,
                'via' => $via,
                'startdate' => $startdate,
                'enddate' => $enddate,
                'priority' => $priority,
                'subunit_id' => $subunit_id,
                'scope' => $scope
            ];

            $query = $this->db->update('pl_release_ducument', $data, ['id' => $id]);
            // var_dump($query);
        }
        // akhir fungsi uploud



        $this->load->model('ProjectListModelRelease', 'plm_release');
        $pl = $this->plm_release->getPL($id)->row();
        $dataLog = array(
            'id_pl' => $id,
            'detail_task' => $pl->detail_task,
            'detail_task' => $pl->detail_task,
            'status' => $pl->status,
            'pic_sts' => $pl->pic_sts,
            'pic_tcel' => $pl->pic_tcel,
            'note' => $pl->note,
            'review' => $pl->review,
            'confirm' => $pl->confirm,
            'via' => $pl->via,
            'tanggal' => $pl->tanggal,
            'tanggal_update' => date('Y-m-d H:i:s')
        );
        // $this->plm_release->inLogPL($dataLog);

        //  OPEN :: Notifikasi Telegram
        // $TL = &get_instance();
        // $TL->load->model('TelegramModel', 'tm');

        // //  Get dari tabel User
        // $U = &get_instance();
        // $U->load->model('UsersModel', 'um');

        // $TL->tm->id_pl = $id;
        // $TL->tm->task = $task;
        // $TL->tm->nama_user = $U->um->getUser($this->session->userdata('id'), '', '', 'username')->row()->username;

        // $msg = $TL->tm->msgToTelegram('u');
        // $request_params = [
        //     'chat_id' => $this->id_tujuan,
        //     'text' => $msg,
        //     'parse_mode' => 'HTML'
        // ];

        // $TL->tm->kirimPesan('', '', $request_params);
        // // CLOSE :: Notifikasi Telegram

        // // Insert Log Actvitiy
        // $LM = &get_instance();
        // $LM->load->model('LogModel', 'lm');

        // $msgLog = "User : " . $this->session->userdata('username') . " -> Update Project List";
        // $LM->lm->id_user = $this->session->userdata('id');
        // $LM->lm->inLogActivity($msgLog, json_encode($data));

        redirect('Project_List_Release_Document/project_list');
    }

    # !OPTIONAL

    public function cekPriorty($val = '')
    {
        // Definisi
        $cek = '';

        switch ($val) {
            case 1:
                $cek = "Immediately";
                break;
            case 2:
                $cek = "Medium";
                break;
            case 3:
                $cek = "Normal";
                break;
            default:
                $cek = "";
                break;
        }

        return $cek;
    }

    public function cekStatus($val = '')
    {
        // Definisi
        $cek = '';

        switch ($val) {
            case 0:
                $cek = "OPEN";
                break;
            case 1:
                $cek = "OPEN";
                break;
            case 2:
                $cek = "SUCCESS";
                break;
                // case 4:
                //     $cek = "PENDING";
                //     break;
                // default:
                $cek = "";
                break;
        }

        return $cek;
    }

    public function dtProjectList()
    {
        // Definisi
        $condition = '';
        $data = [];
        $wPriority = '';
        $wStartdate = '';
        $wEnddate = '';
        $wStartEnd = '';

        $status = $this->input->get('status_release');


        if ($this->input->get('startdate') != "" && $this->input->get('enddate') != "" && $this->input->get('status_release') != "") {
            $kondisi =  [
                ['where', $this->tabel . '.aktif', '1'],
                ['where', $this->tabel . '.status', $this->input->get('status_release')],
                ['where', $this->tabel . '.startdate >=', $this->input->get('startdate')],
                ['where', $this->tabel . '.enddate <=', $this->input->get('enddate')]
            ];
        } else if ($this->input->get('startdate') != "" && $this->input->get('enddate') != "") {
            $kondisi =  [
                ['where', $this->tabel . '.aktif', '1'],
                ['where', $this->tabel . '.startdate >=', $this->input->get('startdate')],
                ['where', $this->tabel . '.enddate <=', $this->input->get('enddate')]
            ];
        } else if ($this->input->get('startdate') != "") {
            $startdate = $this->input->get('startdate');
            $kondisi =  [
                ['where', $this->tabel . '.aktif', '1'],
                ['where', $this->tabel . '.startdate', $startdate]
            ];
        } else if ($this->input->get('enddate') != "") {
            $enddate = $this->input->get('enddate');
            $kondisi =  [
                ['where', $this->tabel . '.aktif', '1'],
                ['where', $this->tabel . '.enddate', $enddate]
            ];
        } else if ($this->input->get('status_release') != "") {
            $status = $this->input->get('status_release');
            $kondisi =  [
                ['where', $this->tabel . '.aktif', '1'],
                ['where', $this->tabel . '.status', $status]
            ];
        } else {


            if ($status != "") {
                $kondisi =  [
                    ['where', $this->tabel . '.aktif', '1'],
                    ['where', $this->tabel . '.status', $status]
                ];
            } else {
                $kondisi =  [
                    ['where', $this->tabel . '.aktif', '1']
                ];
            }
        }


        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'task', 'pic_sts', 'pic_tcel', 'status', 'note', 'review', 'file');
        // Set searchable column fields
        $CI->dt->column_search = array('task', 'pic_sts', 'pic_tcel', 'status', 'note', 'review', 'file');
        // Set select column fields
        $CI->dt->select = $this->tabel . '.*';
        // Set default order
        $CI->dt->order = array($this->tabel . '.id' => 'DESC');

        $condition = $kondisi;

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;

            $data[] = array(
                // $i,
                '<input onclick="edit(' . $dt->id . ')" type="checkbox" name="id[]" value="' . $dt->id . '"> ' . '<span>' . $i . '</span>' . $this->priorityHtml($dt->status),
                '<span style="font-size:12px;">' . $dt->tanggal . '</span>',
                '<b><a href="' . base_url("index.php/Project_List_Release_Document/charter?id_pc=" . $dt->id) . '" > ' . $dt->task . '</a></b> <br> ' . $this->cekStartEndDatePL($dt->startdate, $dt->enddate),
                $dt->pic_sts,
                $dt->pic_tcel,
                $this->cekStatus($dt->status),
                nl2br($dt->note),
                nl2br($dt->review),
                '<a href="' . base_url("uploads/release/" . $dt->file) . '" class="btn btn-primary" target="_blank" > Download Document </a>'

            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    private function priorityHtml($p = '')
    {
        $div = '';

        if ($p == 0) {
            $div = '<div style=" width: auto; height: 7px; background: #6f6e6b; margin-top: 5px; "></div>';
        } else if ($p == 1) {
            $div = '<div style=" width: auto; height: 7px; background: #ff9800; margin-top: 5px; "></div>';
        } else if ($p == 2) {
            $div = '<div style=" width: auto; height: 7px; background:  ##2098ff;margin-top: 5px; "></div>';
        }

        return $div;
    }

    private function cekStartEndDatePL($d1 = '', $d2 = '')
    {

        if ($d1 != '0000-00-00' && $d2 != '0000-00-00') {
            if ($d1 != "" && $d2 != '') {
                $d1 = date('Y-m-d', strtotime($d1));
                $d2 = date('Y-m-d', strtotime($d2));

                $start_end = '<div style="font-size:10px;color:#777;">(' . $d1 . ' - ' . $d2 . ')</div>';
            } else {
                $start_end = '';
            }
        } else {
            $start_end = ' ';
        }

        return $start_end;
    }

    public function dashboard($tahun = 'z')
    {
        if ($tahun == '') {
            $tahun = date('Y');
            $qTahunAll = 'where tanggal like "' . $tahun . '-%"';
            $qTahun = 'WHERE tanggal like "' . $tahun . '-%" AND ';
        } else if ($tahun == 'all') {
            $qTahun = 'WHERE';
            $qTahunAll = '';
        } else {
            $qTahunAll = 'WHERE tanggal like "' . $tahun . '-%"';
            $qTahun = 'WHERE tanggal like "' . $tahun . '-%" AND ';
        }

        // Definis
        $log = [];

        // Project List All Years
        $total_pl_year = $this->db->query("SELECT count(*) as jml FROM pl $qTahunAll ")->row()->jml;

        // Draft
        $draft = $this->db->query("SELECT COUNT(*) jml FROM pl $qTahun status = 1")->row()->jml;

        // On Progress
        $onProgress = $this->db->query("SELECT COUNT(*) jml FROM pl $qTahun status = 2")->row()->jml;

        // Done
        $done = $this->db->query("SELECT COUNT(*) jml FROM pl $qTahun status = 3")->row()->jml;

        // Pending
        $pending = $this->db->query("SELECT COUNT(*) jml FROM pl $qTahun status = 4")->row()->jml;

        $log = [
            'total_dp' => $total_pl_year,
            'data' => [
                ['DONE (' . $done . ') ', (int)$done],
                ['PENDING (' . $pending . ') ', (int)$pending],
                ['ON PROGRESS (' . $onProgress . ')', (int)$onProgress],
                ['DRAFT (' . $draft . ')', (int)$draft],
            ],
        ];

        return $log;
    }

    public function vas($tahun = '')
    {
        if ($tahun == '') {
            $tahun = date('Y');
            $qTahunAll = 'where request_date like "' . $tahun . '-%"';
            $qTahun = 'WHERE request_date like "' . $tahun . '-%" AND ';
        } else if ($tahun == 'all') {
            $qTahun = 'WHERE';
            $qTahunAll = '';
        } else {
            $qTahunAll = 'WHERE request_date like "' . $tahun . '-%"';
            $qTahun = 'WHERE request_date like "' . $tahun . '-%" AND ';
        }

        // Definis
        $log = [];

        // Project List All Years
        $total_pl_year = $this->db->query("SELECT count(*) as jml FROM cdr $qTahunAll ")->row()->jml;

        // Draft
        $draft = $this->db->query("SELECT COUNT(*) jml FROM cdr $qTahun status = 1")->row()->jml;

        // On Progress
        $onProgress = $this->db->query("SELECT COUNT(*) jml FROM cdr $qTahun status = 2")->row()->jml;

        // Done
        $done = $this->db->query("SELECT COUNT(*) jml FROM cdr $qTahun status = 3")->row()->jml;

        // Pending
        $pending = $this->db->query("SELECT COUNT(*) jml FROM cdr $qTahun status = 4")->row()->jml;

        $log = [
            'total_vas' => $total_pl_year,
            'data' => [
                ['DONE (' . $done . ') ', (int)$done],
                ['PENDING (' . $pending . ') ', (int)$pending],
                ['ON PROGRESS (' . $onProgress . ')', (int)$onProgress],
                ['DRAFT (' . $draft . ')', (int)$draft],
            ],
        ];

        return $log;
    }

    public function mytelkomcel($tahun = '')
    {
        if ($tahun == '') {
            $tahun = date('Y');
            $qTahunAll = 'where request_date like "' . $tahun . '-%"';
            $qTahun = 'WHERE request_date like "' . $tahun . '-%" AND ';
        } else if ($tahun == 'all') {
            $qTahun = 'WHERE';
            $qTahunAll = '';
        } else {
            $qTahunAll = 'WHERE request_date like "' . $tahun . '-%"';
            $qTahun = 'WHERE request_date like "' . $tahun . '-%" AND ';
        }

        // Definis
        $log = [];

        // Project List All Years
        $total_pl_year = $this->db->query("SELECT count(*) as jml FROM my_telkomcel $qTahunAll ")->row()->jml;

        // Draft
        $draft = $this->db->query("SELECT COUNT(*) jml FROM my_telkomcel $qTahun status = 1")->row()->jml;

        // On Progress
        $onProgress = $this->db->query("SELECT COUNT(*) jml FROM my_telkomcel $qTahun status = 2")->row()->jml;

        // Done
        $done = $this->db->query("SELECT COUNT(*) jml FROM my_telkomcel $qTahun status = 3")->row()->jml;

        // Pending
        $pending = $this->db->query("SELECT COUNT(*) jml FROM my_telkomcel $qTahun status = 4")->row()->jml;

        $log = [
            'total_mytelkomcel' => $total_pl_year,
            'data' => [
                ['DONE (' . $done . ') ', (int)$done],
                ['PENDING (' . $pending . ') ', (int)$pending],
                ['ON PROGRESS (' . $onProgress . ')', (int)$onProgress],
                ['DRAFT (' . $draft . ')', (int)$draft],
            ],
        ];

        return $log;
    }

    public function crm($tahun = '')
    {
        if ($tahun == '') {
            $tahun = date('Y');
            $qTahunAll = 'where request_date like "' . $tahun . '-%"';
            $qTahun = 'WHERE request_date like "' . $tahun . '-%" AND ';
        } else if ($tahun == 'all') {
            $qTahun = 'WHERE';
            $qTahunAll = '';
        } else {
            $qTahunAll = 'WHERE request_date like "' . $tahun . '-%"';
            $qTahun = 'WHERE request_date like "' . $tahun . '-%" AND ';
        }

        // Definis
        $log = [];

        // Project List All Years
        $total_pl_year = $this->db->query("SELECT count(*) as jml FROM crm $qTahunAll ")->row()->jml;

        // Draft
        $draft = $this->db->query("SELECT COUNT(*) jml FROM crm $qTahun status = 1")->row()->jml;

        // On Progress
        $onProgress = $this->db->query("SELECT COUNT(*) jml FROM crm $qTahun status = 2")->row()->jml;

        // Done
        $done = $this->db->query("SELECT COUNT(*) jml FROM crm $qTahun status = 3")->row()->jml;

        // Pending
        $pending = $this->db->query("SELECT COUNT(*) jml FROM crm $qTahun status = 4")->row()->jml;

        $log = [
            'total_crm' => $total_pl_year,
            'data' => [
                ['DONE (' . $done . ') ', (int)$done],
                ['PENDING (' . $pending . ') ', (int)$pending],
                ['ON PROGRESS (' . $onProgress . ')', (int)$onProgress],
                ['DRAFT (' . $draft . ')', (int)$draft],
            ],
        ];

        return $log;
    }



    public function insert_multiple($data = '')
    {
        if ($data != '') {
            $this->db->insert_batch('students_regist_new2', $data);
        }
    }

    # ~LOG PROJECT LIST

    public function getLogPL($id = '', $id_pl = '', $q = '')
    {
        $obj = 0;

        if ($id != '') {
            $obj = ['id' => $id];
        }

        if ($id_pl != '') {
            $obj = ['id_pl' => $id_pl];
        }

        if ($obj != 0) {
            $q = $this->db->get_where($this->tLog, $obj);
        } else if ($q != '') {
            $q = $this->db->query($q);
        } else {
            $q = $this->db->get_where($this->tLog);
        }

        return $q;
    }

    public function inLogPL($obj = '')
    {
        $log = '';

        if ($obj != '') {
            $q = $this->db->insert($this->tLog, $obj);
        }

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function upLogPL($obj = '', $id = '', $id_pl = '')
    {
        $log = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        if ($id_pl != '') {
            $based_on = ['id_pl' => $id_pl];
        }

        $q = $this->db->update($this->tLog, $obj, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function deLogPL($id = '', $id_pl = '', $obj = '', $q = '')
    {
        $log = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        if ($id_pl != '') {
            $based_on = ['id_pl' => $id_pl];
        }

        $this->db->delete($this->tLog, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    // Telegram Integration

    // public function cekIdTelegram()
    // {
    //     $this->load->model('SettingModel', 'sm');

    //     $s = $this->sm->getSetting('', 'telegram')->row();
    //     $t = (array) json_decode($s->data);

    //     foreach ($t['id_telegram'] as $v) {
    //         if ($v->aktif == "1") {
    //             $this->id_tujuan =  $v->id_tujuan;
    //         }
    //     }
    // }
}

/* End of file ProjectListModel.php */
/* Location: ./application/models/ProjectListModel.php */
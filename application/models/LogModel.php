<?php

defined('BASEPATH') or exit('No direct script access allowed');

class LogModel extends CI_Model
{

    private $tLLogin = "log_login";
    private $tLActivity = "log_activity";

    ### Log Login ###
    public function getLLogin($id = '', $q = '', $obj = '')
    {

        if ($id != '') {
            $obj = ['id' => $id];
        }

        if ($obj != '') {
            $q = $this->db->get_where($this->tLLogin, $obj);
        } else if ($q != '') {
            $q = $this->db->query($q);
        } else {
            $q = $this->db->get($this->tLLogin);
        }

        return $q;
    }

    public function inLLogin($obj = '')
    {
        $log = '';

        if ($obj != '') {
            $q = $this->db->insert($this->tLLogin, $obj);
        }

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function upLLogin($obj = '', $id = '', $base_on = "")
    {
        $log = '';
        $based_on = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $q = $this->db->update($this->tLLogin, $obj, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'msg' => 'Sukses ubah Profile',
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function deLLogin($id = '', $q = '', $obj = '', $based_on = "")
    {
        $log = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $this->db->delete($this->tLLogin, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    ### Log Activity ###

    public function inLogActivity($msg = "", $opsi = "")
    {
        $this->load->library('user_agent');

        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
        $platform = $this->agent->platform();

        $detail = "Platform : " . $platform . " | Access : " . $agent . " | Opsi : " . $opsi;

        $data = [
            'id_user' => $this->id_user,
            'msg' => $msg,
            'b_link' => @$_SERVER['HTTP_REFERER'],
            'ip' => $this->input->ip_address(),
            'datetime' => date('Y-m-d H:i:s'),
            'detail' => $detail
        ];

        return $this->inLActivity($data);
    }

    public function getLActivity($id = '', $q = '', $obj = '')
    {

        if ($id != '') {
            $obj = ['id' => $id];
        }

        if ($obj != '') {
            $q = $this->db->get_where($this->tLActivity, $obj);
        } else if ($q != '') {
            $q = $this->db->query($q);
        } else {
            $q = $this->db->get($this->tLActivity);
        }

        return $q;
    }

    public function inLActivity($obj = '')
    {
        $log = '';

        if ($obj != '') {
            $q = $this->db->insert($this->tLActivity, $obj);
        }

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function upLActivity($obj = '', $id = '', $base_on = "")
    {
        $log = '';
        $based_on = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $q = $this->db->update($this->tLActivity, $obj, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'msg' => 'Sukses ubah Profile',
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }

    public function deLActivity($id = '', $q = '', $obj = '', $based_on = "")
    {
        $log = '';

        if ($id != '') {
            $based_on = ['id' => $id];
        }

        $this->db->delete($this->tLActivity, $based_on);

        $log = [
            'response' => $q,
            'request' => $obj,
            'date' => date('Y-m-d H:i:s'),
        ];

        return $log;
    }
}
/* End of file LogModel.php */